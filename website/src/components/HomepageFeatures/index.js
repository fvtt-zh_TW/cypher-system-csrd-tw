import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: '專注於故事',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        本泛用系統主持人並不擲骰，將原本GM所需的擲骰、計算許多數據的心力移轉到了故事身上，並且所有玩家行動都是基於一顆二十面骰（1D20），以 1-10 的評分標準制定難度來進行。
      </>
    ),
  },
  {
    title: '基於 CSRD',
    Svg: require('@site/static/img/Compatible-with-the-Cypher-System-Logo-black-small.svg').default,
    description: (
      <>
          本站內容皆基於 Monte Cook Games 的 <a href='https://csol.montecookgames.com/license/' target='_blank'>Cypher System Open License</a> 以及所釋出的
          Cypher System Reference Document 進行翻譯，並以 <a href='https://www.montecookgames.com/store/product/cypher-system-rulebook-2/' target='_blank'>Cypher System Rulebook</a> 的章節做為參考分類。
      </>
    ),
  },
  {
    title: '問題回報',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        歡迎回報網站的錯誤與臭蟲，<a href='https://www.plurk.com/nihcep9121' target='_blank'>Sass 噗浪</a>
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
