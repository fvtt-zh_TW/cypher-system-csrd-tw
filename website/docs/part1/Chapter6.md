---
sidebar_position: 3
---

# 第6章：風格
風格是一組特殊能力，用於讓 GM 和玩家調整類型，使其更符合喜好或更適合體裁或設置。例如，如果玩家想要創造能使用魔法的竊賊，她能使用行家並搭上潛行風格。在科幻設定中，勇士可能也有機械知識，所以可能會配上科技風格。

相應階層的風格能力可以與類型能力進行一對一的替換。 所以為了讓勇士有潛行風格的險境感知能力，必須犧牲其他東西 —— 可能是猛擊。角色為了選擇險境感知， 必須要犧牲掉猛擊的選項。

GM 應該始終參與類型的風格調整。 例如，GM對於自己的科幻遊戲，可能有一種稱為「王牌飛行員」的類型，這是種帶有特定科技能力的發言人 —— 特別是那些讓角色成為華麗的星際飛船駕駛的能力。 因此，他們替換了第一階層的翻轉身份和侵略啟發，將之替換成了科技風格的數據插孔和技術技能 ，使角色能夠直接處理飛船， 並能夠以飛行器駕駛和電腦作為技能。

最後，風格主要是作為 GM 工具，透過對四種基本類型進行細微修改，能輕鬆地創建專屬於戰役的類型。儘管玩家可能希望用風格來自己塑造角色，但請記住，他們也能用描述符和焦點很好地塑造自己。

可供選擇的有潛行、科技、魔法、戰鬥、技能和知識。 每個列出能力的完整描述能在[第9章](/docs/part1/Chapter9)中找到。

除非特別說明，你不能選擇相同的能力2次，即使你同時從類型和風格中得到它。


## 潛行風格
具有潛行風格的角色擅長於偷偷摸摸，滲透到不該待的地方以及欺騙別人。他們以各種方式使用這些能力。具有潛行風格的探索者可能是竊賊，而具有潛行風格的勇士可能是刺客。 在超級英雄場景中具有潛行風格的探索者可能是夜間潛伏在街道上的犯罪鬥士。

### 第一階層
- [險境感知](/docs/part1/Chapter9#danger-sense-%E9%9A%AA%E5%A2%83%E6%84%9F%E7%9F%A51-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [激怒](/docs/part1/Chapter9#goad-%E6%BF%80%E6%80%921-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [花招](/docs/part1/Chapter9#legerdemain-%E8%8A%B1%E6%8B%9B1-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [機會主義者](/docs/part1/Chapter9#opportunist-%E6%A9%9F%E6%9C%83%E4%B8%BB%E7%BE%A9%E8%80%85)
- [潛行技能](/docs/part1/Chapter9#stealth-skills-%E6%BD%9B%E8%A1%8C%E6%8A%80%E8%83%BD)

### 第二階層
- [柔術](/docs/part1/Chapter9#contortionist-%E6%9F%94%E8%A1%932-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [尋找缺口](/docs/part1/Chapter9#find-an-opening-%E5%B0%8B%E6%89%BE%E7%BC%BA%E5%8F%A31-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [尋求掩護](/docs/part1/Chapter9#get-away-%E5%B0%8B%E6%B1%82%E6%8E%A9%E8%AD%B72-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [感知埋伏](/docs/part1/Chapter9#sense-ambush-%E6%84%9F%E7%9F%A5%E5%9F%8B%E4%BC%8F)
- [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)

### 第三階層
- [消散](/docs/part1/Chapter9#evanesce-%E6%B6%88%E6%95%A33-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [來自陰影](/docs/part1/Chapter9#from-the-shadows-%E4%BE%86%E8%87%AA%E9%99%B0%E5%BD%B1)
- [賭徒](/docs/part1/Chapter9#gambler-%E8%B3%AD%E5%BE%92)
- [內心防衛](/docs/part1/Chapter9#inner-defense-%E5%85%A7%E5%BF%83%E9%98%B2%E8%A1%9B)
- [誤導攻擊](/docs/part1/Chapter9#misdirect-%E8%AA%A4%E5%B0%8E%E6%94%BB%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [打帶跑](/docs/part1/Chapter9#run-and-fight-%E6%89%93%E5%B8%B6%E8%B7%914-%E9%BB%9E%E6%B0%A3%E5%8A%9B)
- [抓住此刻](/docs/part1/Chapter9#seize-the-moment-%E6%8A%93%E4%BD%8F%E6%AD%A4%E5%88%BB4-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第四階層
- [伏擊者](/docs/part1/Chapter9#ambusher-%E4%BC%8F%E6%93%8A%E8%80%85)
- [衰弱打擊](/docs/part1/Chapter9#debilitating-strike-%E8%A1%B0%E5%BC%B1%E6%89%93%E6%93%8A4-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [智勝](/docs/part1/Chapter9#outwit-%E6%99%BA%E5%8B%9D)
- [超自然感覺](/docs/part1/Chapter9#preternatural-senses-%E8%B6%85%E8%87%AA%E7%84%B6%E6%84%9F%E8%A6%BA)
- [翻滾移動](/docs/part1/Chapter9#tumbling-moves-%E7%BF%BB%E6%BB%BE%E7%A7%BB%E5%8B%955-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第五階層
- [刺客打擊](/docs/part1/Chapter9#assassin-strike-%E5%88%BA%E5%AE%A2%E6%89%93%E6%93%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [面具](/docs/part1/Chapter9#mask-%E9%9D%A2%E5%85%B75-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [回敬原主](/docs/part1/Chapter9#return-to-sender-%E5%9B%9E%E6%95%AC%E5%8E%9F%E4%B8%BB3-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [異樣運氣](/docs/part1/Chapter9#uncanny-luck-%E7%95%B0%E6%A8%A3%E9%81%8B%E6%B0%A34-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第六階層
- [利用優勢](/docs/part1/Chapter9#exploit-advantage-%E5%88%A9%E7%94%A8%E5%84%AA%E5%8B%A2)
- [彈躍](/docs/part1/Chapter9#spring-away-%E5%BD%88%E8%BA%8D5-%E9%BB%9E%E9%80%9F%E5%BA%A6) 
- [竊賊運氣](/docs/part1/Chapter9#thief-s-luck-%E7%AB%8A%E8%B3%8A%E9%81%8B%E6%B0%A3)
- [扭曲命運](/docs/part1/Chapter9#twist-of-fate-%E6%89%AD%E6%9B%B2%E5%91%BD%E9%81%8B)

## 科技風格

帶有科技風格的角色通常來自科幻體裁，或者至少是現代背景（儘管一切皆有可能）。 他們擅長使用、處理和製造機器。帶有科技風格的探索者可能是星際飛船的駕駛，而帶有科技風格的發言人可能是科技牧師。
一些不太以電腦為導向的能力可能適合蒸汽龐克角色，而現代角色可以使用一些不涉及星際飛船或超技術的能力。

### 第一階層
- [數據插孔](/docs/part1/Chapter9#datajack-%E6%95%B8%E6%93%9A%E6%8F%92%E5%AD%941-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [駭客](/docs/part1/Chapter9#hacker-%E9%A7%AD%E5%AE%A22-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [機器界面](/docs/part1/Chapter9#machine-interface-%E6%A9%9F%E5%99%A8%E7%95%8C%E9%9D%A22-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [搶掠機器](/docs/part1/Chapter9#scramble-machine-%E6%90%B6%E6%8E%A0%E6%A9%9F%E5%99%A82-%E9%BB%9E%E6%99%BA%E5%8A%9B) 
- [技術技能](/docs/part1/Chapter9#tech-skills-%E6%8A%80%E8%A1%93%E6%8A%80%E8%83%BD)
- [修補匠](/docs/part1/Chapter9#tinker-%E4%BF%AE%E8%A3%9C%E5%8C%A01-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第二階層
- [遠程接口](/docs/part1/Chapter9#distant-interface-%E9%81%A0%E7%A8%8B%E6%8E%A5%E5%8F%A32-%E9%BB%9E%E6%99%BA%E5%8A%9B) 
- [機器效率](/docs/part1/Chapter9#machine-efficiency-%E6%A9%9F%E5%99%A8%E6%95%88%E7%8E%873-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [機器超載](/docs/part1/Chapter9#overload-machine-%E6%A9%9F%E5%99%A8%E8%B6%85%E8%BC%893-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [伺服-0](/docs/part1/Chapter9#serv-0-%E4%BC%BA%E6%9C%8D-0)
- [伺服-0 防禦者](/docs/part1/Chapter9#serv-0-defender-%E4%BC%BA%E6%9C%8D-0-%E9%98%B2%E7%A6%A6%E8%80%85)
- [伺服-0 修復](/docs/part1/Chapter9#serv-0-repair-%E4%BC%BA%E6%9C%8D-0-%E4%BF%AE%E5%BE%A9)
- [掌握工具](/docs/part1/Chapter9#tool-mastery-%E6%8E%8C%E6%8F%A1%E5%B7%A5%E5%85%B7)

### 第三階層
- [機械傳思](/docs/part1/Chapter9#mechanical-telepathy-%E6%A9%9F%E6%A2%B0%E5%82%B3%E6%80%9D3-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [伺服-0 掃描者](/docs/part1/Chapter9#serv-0-scanner-%E4%BC%BA%E6%9C%8D-0-%E6%8E%83%E6%8F%8F%E8%80%852-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [艦船基礎](/docs/part1/Chapter9#ship-footing-%E8%89%A6%E8%88%B9%E5%9F%BA%E7%A4%8E3-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [艦船語言](/docs/part1/Chapter9#shipspeak-%E8%89%A6%E8%88%B9%E8%AA%9E%E8%A8%80)
- [集中掃射](/docs/part1/Chapter9#spray-%E9%9B%86%E4%B8%AD%E6%8E%83%E5%B0%842-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第四階層
- [機器綁定](/docs/part1/Chapter9#machine-bond-%E6%A9%9F%E5%99%A8%E7%B6%81%E5%AE%9A)
- [機器人戰士](/docs/part1/Chapter9#robot-fighter-%E6%A9%9F%E5%99%A8%E4%BA%BA%E6%88%B0%E5%A3%AB)
- [伺服-0 瞄準](/docs/part1/Chapter9#serv-0-aim-%E4%BC%BA%E6%9C%8D-0-%E7%9E%84%E6%BA%96)
- [伺服-0 爭鬥者](/docs/part1/Chapter9#serv-0-brawler-%E4%BC%BA%E6%9C%8D-0-%E7%88%AD%E9%AC%A5%E8%80%85)
- [伺服-0 間諜](/docs/part1/Chapter9#serv-0-spy-%E4%BC%BA%E6%9C%8D-0-%E9%96%93%E8%AB%9C3-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第五階層
- [控制機器](/docs/part1/Chapter9#control-machine-%E6%8E%A7%E5%88%B6%E6%A9%9F%E5%99%A86-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [應急裝備](/docs/part1/Chapter9#jury-rig-%E6%87%89%E6%80%A5%E8%A3%9D%E5%82%995-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [機器夥伴](/docs/part1/Chapter9#machine-companion-%E6%A9%9F%E5%99%A8%E5%A4%A5%E4%BC%B4)

### 第六階層
- [訊息收集](/docs/part1/Chapter9#information-gathering-%E8%A8%8A%E6%81%AF%E6%94%B6%E9%9B%865-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [機器大師](/docs/part1/Chapter9#master-machine-%E6%A9%9F%E5%99%A8%E5%A4%A7%E5%B8%AB8-%E9%BB%9E%E6%99%BA%E5%8A%9B) 


## 魔法風格
你知道一點點魔法。 可能不是法師，但知道基本原理 —— 魔法是如何運作，以及如何完成的。當然，在你的設置中，「魔法」可能其實是指靈能力、突變能力、怪異的外星科技，或者任何能產生有趣和有用效果的東西。

有魔法風格的探索者可能是巫師獵人，而發言人則可能是施法詩人。 儘管行家配上魔法風格仍然是行家，但是你可能會發現，將類型的一些能力與此風格進行替換，能更理想的調整角色。

### 第一階層
- [眾神祝福](/docs/part1/Chapter9#blessing-of-the-gods-%E7%9C%BE%E7%A5%9E%E7%A5%9D%E7%A6%8F)
- [封閉心靈](/docs/part1/Chapter9#closed-mind-%E5%B0%81%E9%96%89%E5%BF%83%E9%9D%88)
- [纏繞力場](/docs/part1/Chapter9#entangling-force-%E7%BA%8F%E7%B9%9E%E5%8A%9B%E5%A0%B41-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [野魔法](/docs/part1/Chapter9#hedge-magic-%E9%87%8E%E9%AD%94%E6%B3%951-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [魔法訓練](/docs/part1/Chapter9#magic-training-%E9%AD%94%E6%B3%95%E8%A8%93%E7%B7%B4)
- [精神連接](/docs/part1/Chapter9#mental-link-%E7%B2%BE%E7%A5%9E%E9%80%A3%E6%8E%A51-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [預感](/docs/part1/Chapter9#premonition-%E9%A0%90%E6%84%9F2-%E9%BB%9E%E6%99%BA%E5%8A%9B)


### 第二階層
- [震盪衝擊波](/docs/part1/Chapter9#concussive-blast-%E9%9C%87%E7%9B%AA%E8%A1%9D%E6%93%8A%E6%B3%A22-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [取物](/docs/part1/Chapter9#fetch-%E5%8F%96%E7%89%A93-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [力場領域](/docs/part1/Chapter9#force-field-%E5%8A%9B%E5%A0%B4%E9%A0%98%E5%9F%9F3-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [上鎖術](/docs/part1/Chapter9#lock-%E4%B8%8A%E9%8E%96%E8%A1%932-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [修復肉體](/docs/part1/Chapter9#repair-flesh-%E4%BF%AE%E5%BE%A9%E8%82%89%E9%AB%943-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第三階層
- [距離觀察](/docs/part1/Chapter9#distance-viewing-%E8%B7%9D%E9%9B%A2%E8%A7%80%E5%AF%9F5-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [火焰綻放](/docs/part1/Chapter9#fire-bloom-%E7%81%AB%E7%84%B0%E7%B6%BB%E6%94%BE4-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [甩投](/docs/part1/Chapter9#fling-%E7%94%A9%E6%8A%954-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [遠程原力](/docs/part1/Chapter9#force-at-distance-%E9%81%A0%E7%A8%8B%E5%8E%9F%E5%8A%9B4-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [召喚巨蜘蛛](/docs/part1/Chapter9#summon-giant-spider-%E5%8F%AC%E5%96%9A%E5%B7%A8%E8%9C%98%E8%9B%9B4-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第四階層
- [元素保護](/docs/part1/Chapter9#elemental-protection-%E5%85%83%E7%B4%A0%E4%BF%9D%E8%AD%B74-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [撬開](/docs/part1/Chapter9#pry-open-%E6%92%AC%E9%96%8B4-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第五階層
- [憑空造作](/docs/part1/Chapter9#create-%E6%86%91%E7%A9%BA%E9%80%A0%E4%BD%9C7-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [神性干預](/docs/part1/Chapter9#divine-intervention-%E7%A5%9E%E6%80%A7%E5%B9%B2%E9%A0%902-%E9%BB%9E%E6%99%BA%E5%8A%9B%E6%88%96-2-%E9%BB%9E%E6%99%BA%E5%8A%9B--4-xp)
- [龍喉](/docs/part1/Chapter9#dragons-maw-%E9%BE%8D%E5%96%896-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [快速旅行](/docs/part1/Chapter9#fast-travel-%E5%BF%AB%E9%80%9F%E6%97%85%E8%A1%8C7-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [真實感覺](/docs/part1/Chapter9#true-senses-%E7%9C%9F%E5%AF%A6%E6%84%9F%E8%A6%BA)

### 第六階層
- [重新定位](/docs/part1/Chapter9#relocate-%E9%87%8D%E6%96%B0%E5%AE%9A%E4%BD%8D7-%E9%BB%9E%E6%99%BA%E5%8A%9B) 
- [召喚惡魔](/docs/part1/Chapter9#summon-demon-%E5%8F%AC%E5%96%9A%E6%83%A1%E9%AD%947-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [穿越世界](/docs/part1/Chapter9#traverse-the-worlds-%E7%A9%BF%E8%B6%8A%E4%B8%96%E7%95%8C8-%E9%BB%9E%E6%99%BA%E5%8A%9B)
- [死亡令詞](/docs/part1/Chapter9#word-of-death-%E6%AD%BB%E4%BA%A1%E4%BB%A4%E8%A9%9E5-%E9%BB%9E%E6%99%BA%E5%8A%9B)


## 戰鬥風格

戰鬥風格讓角色更有戰鬥感。在奇幻場景中具有戰鬥風格的發言人將是戰鬥詩人。在歷史遊戲中帶有戰鬥風格的探索者可能是海盜。科幻體裁中擅長戰鬥的人，可能是千錘百鍊的戰場老兵。

### 第一階層
- [險境感知](/docs/part1/Chapter9#danger-sense-%E9%9A%AA%E5%A2%83%E6%84%9F%E7%9F%A51-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [著甲熟練](/docs/part1/Chapter9#practiced-in-armor-%E8%91%97%E7%94%B2%E7%86%9F%E7%B7%B4)
- [熟練中型武器](/docs/part1/Chapter9#practiced-with-medium-weapons-%E7%86%9F%E7%B7%B4%E4%B8%AD%E5%9E%8B%E6%AD%A6%E5%99%A8)

### 第二階層
- [嗜血](/docs/part1/Chapter9#bloodlust-%E5%97%9C%E8%A1%803-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 
- [戰鬥英勇](/docs/part1/Chapter9#combat-prowess-%E6%88%B0%E9%AC%A5%E8%8B%B1%E5%8B%87)
- [無甲訓練](/docs/part1/Chapter9#trained-without-armor-%E7%84%A1%E7%94%B2%E8%A8%93%E7%B7%B4)

### 第三階層
- [熟練所有武器](/docs/part1/Chapter9#practiced-with-all-weapons-%E7%86%9F%E7%B7%B4%E6%89%80%E6%9C%89%E6%AD%A6%E5%99%A8)
- [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)
- [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)
- [連續攻擊](/docs/part1/Chapter9#successive-attack-%E9%80%A3%E7%BA%8C%E6%94%BB%E6%93%8A2-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第四階層
- [能幹戰士](/docs/part1/Chapter9#capable-warrior-%E8%83%BD%E5%B9%B9%E6%88%B0%E5%A3%AB)
- [致命瞄準](/docs/part1/Chapter9#deadly-aim-%E8%87%B4%E5%91%BD%E7%9E%84%E6%BA%963-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [盛怒](/docs/part1/Chapter9#fury-%E7%9B%9B%E6%80%923-%E9%BB%9E%E6%B0%A3%E5%8A%9B)
- [誤導攻擊](/docs/part1/Chapter9#misdirect-%E8%AA%A4%E5%B0%8E%E6%94%BB%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6)
- [集中掃射](/docs/part1/Chapter9#spray-%E9%9B%86%E4%B8%AD%E6%8E%83%E5%B0%842-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第五階層
- [熟手防禦者](/docs/part1/Chapter9#experienced-defender-%E7%86%9F%E6%89%8B%E9%98%B2%E7%A6%A6%E8%80%85)
- [難以企及的目標](/docs/part1/Chapter9#hard-target-%E9%9B%A3%E4%BB%A5%E4%BC%81%E5%8F%8A%E7%9A%84%E7%9B%AE%E6%A8%99)
- [招架](/docs/part1/Chapter9#parry-%E6%8B%9B%E6%9E%B65-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### 第六階層
- [高級攻擊技能](/docs/part1/Chapter9#greater-skill-with-attacks-%E9%AB%98%E7%B4%9A%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)
- [護甲精通](/docs/part1/Chapter9#mastery-in-armor-%E8%AD%B7%E7%94%B2%E7%B2%BE%E9%80%9A)
- [精通防禦](/docs/part1/Chapter9#mastery-with-defense-%E7%B2%BE%E9%80%9A%E9%98%B2%E7%A6%A6)


## 技能和知識風格
此風格適合於需要更多知識和更多世界真實才能應用的角色。這不像超自然力量或破解多個敵人的能力那樣浮華和戲劇化，但有時專業知識或技術才是解決問題的真正辦法。

擁有技能和知識的勇士可能是軍事工程師。探索者是野外科學家。發言人則可能是教師。

### 第一階層
- [互動技巧](/docs/part1/Chapter9#interaction-skills-%E4%BA%92%E5%8B%95%E6%8A%80%E5%B7%A7)
- [調查技能](/docs/part1/Chapter9#investigative-skills-%E8%AA%BF%E6%9F%A5%E6%8A%80%E8%83%BD)
- [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD)
- [身體技能](/docs/part1/Chapter9#physical-skills-%E8%BA%AB%E9%AB%94%E6%8A%80%E8%83%BD)
- [旅行技能](/docs/part1/Chapter9#travel-skills-%E6%97%85%E8%A1%8C%E6%8A%80%E8%83%BD)

### 第二階層
- [額外技能](/docs/part1/Chapter9#extra-skill-%E9%A1%8D%E5%A4%96%E6%8A%80%E8%83%BD)
- [掌握工具](/docs/part1/Chapter9#tool-mastery-%E6%8E%8C%E6%8F%A1%E5%B7%A5%E5%85%B7)
- [理解力](/docs/part1/Chapter9#understanding-%E7%90%86%E8%A7%A3%E5%8A%9B1-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第三階層
- [靈活技能](/docs/part1/Chapter9#flex-skill-%E9%9D%88%E6%B4%BB%E6%8A%80%E8%83%BD)
- [即興發揮](/docs/part1/Chapter9#improvise-%E5%8D%B3%E8%88%88%E7%99%BC%E6%8F%AE3-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第四階層
- [多重技能](/docs/part1/Chapter9#multiple-skills-%E5%A4%9A%E9%87%8D%E6%8A%80%E8%83%BD)
- [快速機智](/docs/part1/Chapter9#quick-wits-%E5%BF%AB%E9%80%9F%E6%A9%9F%E6%99%BA)
- [任務專精](/docs/part1/Chapter9#task-specialization-%E4%BB%BB%E5%8B%99%E5%B0%88%E7%B2%BE)

### 第五階層
- [熟練中型武器](/docs/part1/Chapter9#practiced-with-medium-weapons-%E7%86%9F%E7%B7%B4%E4%B8%AD%E5%9E%8B%E6%AD%A6%E5%99%A8)
- [閱讀符號](/docs/part1/Chapter9#read-the-signs-%E9%96%B1%E8%AE%80%E7%AC%A6%E8%99%9F4-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### 第六階層
- [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)
- [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)
