---
sidebar_position: 5
---

# 第8章：焦點
焦點是使角色獨一無二的東西。 一個團體的兩個角色不應該有同樣的焦點。當創造了自己的角色，並且每次提升階層時，焦點都會給角色帶來好處。 它是「我是 [動詞] 的 [形容詞] [名詞]」這句話的動詞。

本章包含了近百個焦點範例，如背負火焰光環，寧願讀書，和駕駛星艦。 這些焦點可供玩家選擇和使用，也能由GM挑選作為清單來讓玩家使用。

此外，本章的後半部分為 GM 或有進取心的玩家提供工具來創建自己的自創焦點，以完美地匹配特定遊戲或戰役需求，如創建新焦點所述。

## 選擇焦點
並不是所有焦點都適合每種體裁。第3部分中的體裁章節提供了指導，但本節提供了一些廣泛概括。 誠然，GM 能在自己的設置中許可全部的焦點。 焦點在這種情況下是個重要的區別，例如命令精神力量，清楚地表明心靈能力存在於背景中，就像對月亮嚎叫意味著有獸化人一般的狼人存在著，還有駕駛星艦，顯然，要有星際飛船才會需要駕駛員。

當選擇焦點時，玩家會得到一個或多個與其它 PCs 連接，第一階層的能力，也許還有1-2個跟能力相關的額外啟始裝備，或者能跟焦點有很好的搭配。 例如，一個可以構建東西的角色需要一套工具。 不斷著火的角色需要一套對火焰免疫的衣服。 繪製符文來施法的角色需要書寫工具。 一個用劍殺死怪物的角色需要一把劍。 等等。 也就是說，很多焦點不需要額外裝備。

每個焦點還提供了一個或多個建議 — GM 侵入 — 對於真正好的或真正壞的擲骰可能產生的影響或後果。

本章的幾個焦點有提供「類型替換選項」，允許玩家將原本能從類型獲得的能力替換為指定能力。 有時玩家不需要進行思考；因為非選不可。 例如，熱愛虛空焦點提供的有太空服，就會旅行能力。

當角色升級新階層時，焦點會給予更多能力。每個能力都有自己屬於動作或啟動子的標註。 不同階層的能力獨立於其他階層，並能與其他階層的能力一起累積（除非另有說明）。因此，如果第一階層的能力給予 +1 護甲，而第四階層的能力也給予了 +1 護甲，當角色到達第四階層並選擇時，總共會獲得 +2 護甲。

在第三階層和第六階層，角色被要求從提供的 2 個選項中選擇一項能力。

最後，你能選擇是否擴展焦點背後的故事（儘管這不是強制性的）。

## 焦點連結
選擇一種與焦點相關的關係。 GM 能為玩家選擇（或創造）一個或多個焦點，或從下列選擇至多 4 個連接。

1. 選擇另一個 PC。 由於你不知道的原因，那個角色完全不受你的焦點能力的影響，不管你是為了幫助還是傷害他們。
2. 選擇另一個 PC。 你幾年前就認識這個角色，但你認為他們不認識你。
3. 選擇另一個 PC。 你總是想給他們留下深刻的印象，但是不確定為什麼。
4. 選擇另一個 PC。 這個角色有一個讓你討厭的習慣，但是你對他們的能力印象深刻。
5. 選擇另一個 PC。 這個角色在欣賞你的特定範式、戰鬥風格或其他焦點屬性方面顯示出了潛力。 你想訓練他們，但你不一定有資格教導（這取決於你），他們可能不感興趣（這取決於他們）。
6. 選擇另一個 PC。 當你在戰鬥中，如果他們在你的直接距離內，有時他們會提供一個資產，有時他們會意外地讓你的攻擊骰受阻（50% 的機率，每場戰鬥決定）。
7. 選擇另一個 PC。 你曾經救過他們的命，他們顯然對你感恩戴德。你希望他們沒有這樣做;這只是工作的一部分。
8. 選擇另一個 PC。 那個角色最近用某種方式嘲笑你，這真的傷害了你的感情。你如何處理這件事（如果有的話）取決於你自己。
9. 選擇另一個 PC。 這個角色知道你在過去遭受過機器人實體的傷害。你是否討厭機器人現在取決於你，這可能會影響你和角色的關係，如果他們與機器人友好或有機器人零件。
10. 選擇另一個 PC。 這個角色和你們來自同一個地方，你們小時候就認識。
11. 選擇另一個 PC。 過去，他們教你一些戰鬥技巧。
12. 選擇另一個 PC。 該角色似乎不贊成你的方法。
13. 選擇另一個 PC。 很久以前，你們兩個是對立的一方。你贏了，儘管你在他們眼中“欺騙”了他們（但從你的角度來看，在一場戰鬥中一切都是公平的）。他們可能已經準備好再次比賽，儘管這取決於他們自己。
14. 選擇另一個 PC。 你總是試圖用你的技能、機智、外表或虛張聲勢給那個角色留下深刻印象。也許他們是你的競爭對手，也許你需要他們的尊重，又或者你對他們很感興趣。
15. 選擇另一個 PC。 你擔心角色會嫉妒你的能力，並擔心它會導致問題。
16. 選擇另一個 PC。 你不小心將它們困在了你設置的陷阱中，而且必須放他們自由。
17. 選擇另一個 PC。 你曾經被雇傭去追蹤與那個角色關係密切的人。
18. 選擇兩個 PCs （最好是那些可能阻礙你攻擊的人）。 當你的攻擊失敗，GM 裁定你擊中了別人而不是你的目標，你擊中了這兩個角色中的一個。
19. 選擇另一個 PC。 你不知道怎麼辦到的，也不知道從何而來，但這個角色在一瓶稀有酒精飲料上有管道，可以半價幫你買到。
20. 選擇另一個 PC。 你最近丟失了一件物品，你開始相信是他們拿走了它。 是否是真的取決於他們自己。
21. 選擇另一個 PC。 他們似乎總是知道你在哪裡，或者至少知道你相對於他們在什麼方向。
22. 選擇另一個 PC。 看到你使用你的焦點能力，似乎會觸發那個角色不愉快的記憶。 這部分的記憶由另一個 PC 決定，儘管它們可能無法有意識地回憶起這些記憶。
23. 選擇另一個 PC。 他們的某些東西干擾了你的能力。當他們站在你旁邊時，你的焦點能力額外消耗 1 點。
24. 選擇另一個 PC。 他們的某些特質可以補充你的能力。 當他們站在你旁邊時，你在任何 24 小時內使用的第一個焦點能力會少消耗 2 點。
25. 選擇另一個 PC。 你已經認識這個角色一段時間了，他們幫助你控制你的焦點相關能力。
26. 選擇另一個 PC。 在這個角色過去的某個時候，他們經歷了一次毀滅性的經歷，他們嘗試了一些你的焦點理所當然要做的事情。 他們是否會告訴你，這取決於他們自己。
27. 選擇另一個 PC。 他們偶爾的笨拙和大聲的行為會激怒你。
28. 選擇另一個 PC。 在最近的練習中，你不小心攻擊了他們，把他們弄傷了。他們是怨恨你還是原諒你，這要由他們來決定。
29. 選擇另一個 PC。 他們欠你很多錢。
30. 選擇另一個 PC。 在最近的一段時間裡，你在躲避威脅的時候，不小心放那個角色自生自滅。 他們勉強活了下來。 這是由玩家的角色決定他們是否怨恨你或決定原諒你。
31. 選擇另一個 PC。 最近，他們無意中（或有意地）把你置於危險的境地。你現在很好，但在他們面前你很小心。
32. 選擇另一個 PC。 從你的角度來看，他們似乎對一個特定的想法、人或情況感到緊張。你應該教他們如何更自在地面對他們的恐懼（如果他們願意的話）。
33. 選擇另一個 PC。 他們曾經叫你膽小鬼。
34. 選擇另一個 PC。 這個角色總是能認出你或你的作品，不管你是喬裝打扮還是當他們出現時你早已不在了。
35. 選擇另一個 PC。 你無意中造成了一場事故，使他們陷入沉睡，三天沒有醒來。他們是否原諒你取決於他們自己。
36. 選擇另一個 PC。 你很確定自己以某種方式聯繫在一起。
37. 選擇另一個 PC。 你不小心得知了他們試圖保守秘密的東西。
38. 選擇另一個 PC。 他們對你的閃亮的焦點能力特別敏感，偶爾他們會暈眩幾輪，這會使他們的行動受阻。
39. 選擇另一個 PC。 它們似乎有一件曾經屬於你的珍貴物品，但幾年前你在一次碰機運的遊戲中失去了它。
40. 選擇另一個 PC。 如果不是因為你，這個角色可能會在過去的智力測驗中失敗。
41. 選擇另一個 PC。 基於你聽到的一些評論，你懷疑他們沒有把你的訓練領域或最喜歡的愛好放在最高的位置。
42. 選擇一個與你的焦點交織的 PC。 這種奇怪的聯繫在某種程度上影響了他們。 例如，如果角色使用武器，你的焦點能力有時會以某種方式提高他們的攻擊力。
43. 選擇另一個 PC。 他們非常害怕高地。 你想教他們如何讓腳離開地面時更加舒適。 他們必須決定是否接受你的指導。
44. 選擇另一個 PC。 他們懷疑你對過去發生的重大事件的說法。 他們甚至可能會試圖詆毀你或發現你故事背後的 “秘密” ，儘管這取決於他們。
45. 選擇另一個 PC。 他們的訣竅是能夠識別你的計劃或方案在哪些方面薄弱。
46. 選擇另一個 PC。 這個角色的臉對你有無法解釋的吸引，有時你會發現自己在泥土中勾勒出他們的模樣，或者使用一些你能接觸到的其他媒介。
47. 選擇另一個 PC。 這個角色有一個你給他們的額外常規裝備，或者是你自己做的東西，或者是你想給他們的東西。 （他們選擇物品。）
48. 選擇另一個 PC。 他們委託你為他們做一件事。 你已經拿到錢了，但還沒有完成工作。
49. 選擇另一個 PC。 你們以前一起工作過，結果很糟糕。
50. 選擇另一個 PC。 當他們站在你旁邊，用他們的行動來集中精力幫助你時，你的一種焦點能力的範圍就加倍了。

## 焦點背後的故事
本書的焦點被有意地精簡，使他們能用於各種體裁。每個焦點都有自己的概述。在選擇焦點之後，你能選擇增加更多與世界或角色相關的故事和描述來擴展它的呈現。

例如，如果你選擇秘密行動，概述就是「打著別人的幌子，你要尋找強者不想洩露的答案。」如果你選擇進行怪異科學，概述就是「你超乎尋常的洞察力和能力使你成為一個有能力做出驚人壯舉的科學家。」這些描述提供了應用焦點的簡單靈感。

不過，如果你願意（而且只有在你願意的時候） 你能夠以一種與遊戲相關的方式向那些描述中增加更多內容。 例如，如果你選擇在現代體裁（如恐怖、都市幻想、間諜活動或類似的）中使用秘密行動和進行怪異科學，那麼你能擴展成以下範例所示的描述。

**秘密行動：** 間諜行動並不如大眾所想。至少，你希望人人都以為是那個樣子，因為事實上，你已經被訓練成間諜或秘密特工。你可能為政府工作，也可能為自己工作。你可能是警探或罪犯。你甚至是調查記者。

不管怎樣，你知道了別人試圖保守秘密的訊息。 你收集謠言、私語、故事和來之不易的證據，然後利用這些知識來幫助自己，如果合適的話，向雇主提供他們想要的訊息。或者，把所學的賣給那些願意付錢的傢伙。

你可能會穿深色的衣服 — 黑色、深灰色或深藍色 — 來幫助融入陰影，除非你的選擇需要你看起來像別人。

**進行怪異科學：** 你可能是受人尊敬的科學家，已經在一些同行評審的期刊上發表過論文。或者可能被同齡人視為怪人，在別人認為缺乏證據的情況下追求邊緣學說。事實是，你有特殊的天賦，能夠篩選可能性的邊緣。通過實驗，你發現新的見解和奇怪的現象。當別人只看到瘋狂，你則視為啟示。無論你是政府的承包商、大學研究員、企業科學家，還是作為對自己實驗室的好奇心放縱者，你都在推動可能的事情。

你可能更關心你的工作，而不是外表、禮貌或得體的行為或社會規範等瑣事，但話又說回來，像你這樣的怪人可能也會扭轉這種刻板印象。

如果你想更進一步，你能決定角色的焦點能力從何而來。 根據不同體裁，他們能通過高級而持久的訓練獲得這些能力，通過魔法符文，通過控制論部分，從基因遺傳，或者獲得先進技術。 例如，角色能用閃電攻擊目標，是因為他們被奇怪的輻射擊中，或者因為他們拿起了閃電槍。另一方面，這可能是因為他們的高強度訓練讓他們學會了閃電魔法。這種可能性幾乎是無窮無盡的，取決於你如何展現。不管焦點的能力是如何獲得的，只要有效就足夠了。


## 焦點
本節中列出的每個焦點能力的完整描述在[第9章](/docs/part1/Chapter9)中可以找到，在一個龐大的目錄中有對類型、風格和焦點能力的描述。在能力之後列出的數字是頁碼，以便於參考。

### ABIDES IN STONE 石成肉身
你的肉體由堅硬的礦物組成，使你成為一個笨重的、難以傷害的類人。

階層 1: [魔像軀體](/docs/part1/Chapter9#golem-body-%E9%AD%94%E5%83%8F%E8%BB%80%E9%AB%94)  
階層 1: [魔像療癒](/docs/part1/Chapter9#golem-healing-%E9%AD%94%E5%83%8F%E7%99%82%E7%99%92)  
階層 2: [魔像握力](/docs/part1/Chapter9#golem-grip-%E9%AD%94%E5%83%8F%E6%8F%A1%E5%8A%9B3-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 3: [受訓猛擊者](/docs/part1/Chapter9#trained-basher-%E5%8F%97%E8%A8%93%E7%8C%9B%E6%93%8A%E8%80%85)  
階層 3: [魔像巨跺](/docs/part1/Chapter9#golem-stomp-%E9%AD%94%E5%83%8F%E5%B7%A8%E8%B7%BA4-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [武器化](/docs/part1/Chapter9#weaponization-%E6%AD%A6%E5%99%A8%E5%8C%96)    
階層 4: [深度儲備](/docs/part1/Chapter9#deep-reserves-%E6%B7%B1%E5%BA%A6%E5%84%B2%E5%82%99)  
階層 5: [專精猛擊者](/docs/part1/Chapter9#specialized-basher-%E5%B0%88%E7%B2%BE%E7%8C%9B%E6%93%8A%E8%80%85)  
階層 5: [靜止的像個雕像](/docs/part1/Chapter9#still-as-a-statue-%E9%9D%9C%E6%AD%A2%E7%9A%84%E5%83%8F%E5%80%8B%E9%9B%95%E5%83%8F5-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 6: [超級增強](/docs/part1/Chapter9#ultra-enhancement-%E8%B6%85%E7%B4%9A%E5%A2%9E%E5%BC%B7) 或 [心靈湧動](/docs/part1/Chapter9#mind-surge-%E5%BF%83%E9%9D%88%E6%B9%A7%E5%8B%95)

### ABSORBS ENERGY 吸收能量
你可以利用動能，把它轉換成其他類型的能量。

階層 1: [吸收動能](/docs/part1/Chapter9#absorb-kinetic-energy-%E5%90%B8%E6%94%B6%E5%8B%95%E8%83%BD)  
階層 1: [釋放能量](/docs/part1/Chapter9#release-energy-%E9%87%8B%E6%94%BE%E8%83%BD%E9%87%8F)  
階層 2: [激發物體](/docs/part1/Chapter9#energize-object-%E6%BF%80%E7%99%BC%E7%89%A9%E9%AB%94)  
階層 3: [吸收純能量](/docs/part1/Chapter9#absorb-pure-energy-%E5%90%B8%E6%94%B6%E7%B4%94%E8%83%BD%E9%87%8F) 或 [強化吸收動能](/docs/part1/Chapter9#improved-absorb-kinetic-energy-%E5%BC%B7%E5%8C%96%E5%90%B8%E6%94%B6%E5%8B%95%E8%83%BD)  
階層 4: [能量過充](/docs/part1/Chapter9#overcharge-energy-%E8%83%BD%E9%87%8F%E9%81%8E%E5%85%85)  
階層 5: [激發生物](/docs/part1/Chapter9#energize-creature-%E6%BF%80%E7%99%BC%E7%94%9F%E7%89%A96-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 6: [激勵人群](/docs/part1/Chapter9#energize-crowd-%E6%BF%80%E5%8B%B5%E4%BA%BA%E7%BE%A4-9-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [設備過充](/docs/part1/Chapter9#overcharge-device-%E8%A8%AD%E5%82%99%E9%81%8E%E5%85%85)

### AWAKENS DREAMS 喚醒夢境
你可以從夢中提取圖像，把它們帶到現實生活中。

階層 1: [夢工廠](/docs/part1/Chapter9#dreamcraft-%E5%A4%A2%E5%B7%A5%E5%BB%A01-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [如夢似幻](/docs/part1/Chapter9#oneirochemy-%E5%A6%82%E5%A4%A2%E4%BC%BC%E5%B9%BB)  
階層 2: [盜夢者](/docs/part1/Chapter9#dream-thief-%E7%9B%9C%E5%A4%A2%E8%80%852-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [夢境變成現實](/docs/part1/Chapter9#dream-becomes-reality-%E5%A4%A2%E5%A2%83%E8%AE%8A%E6%88%90%E7%8F%BE%E5%AF%A64-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [強化智力](/docs/part1/Chapter9#enhanced-intellect-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 4: [白日夢](/docs/part1/Chapter9#daydream-%E7%99%BD%E6%97%A5%E5%A4%A24-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [噩夢](/docs/part1/Chapter9#nightmare-%E5%99%A9%E5%A4%A25-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [夢之屋](/docs/part1/Chapter9#chamber-of-dreams-%E5%A4%A2%E4%B9%8B%E5%B1%8B8-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [反應領域](/docs/part1/Chapter9#reactive-field-%E5%8F%8D%E6%87%89%E9%A0%98%E5%9F%9F)

### BATTLES ROBOTS 與機器人作戰
你擅長與機器人、自動人形和機器實體作戰。

階層 1: [機器弱點](/docs/part1/Chapter9#machine-vulnerabilities-%E6%A9%9F%E5%99%A8%E5%BC%B1%E9%BB%9E)  
階層 1: [技術技能](/docs/part1/Chapter9#tech-skills-%E6%8A%80%E8%A1%93%E6%8A%80%E8%83%BD)  
階層 2: [對機器人防禦](/docs/part1/Chapter9#defense-against-robots-%E5%B0%8D%E6%A9%9F%E5%99%A8%E4%BA%BA%E9%98%B2%E7%A6%A6)  
階層 2: [機器狩獵](/docs/part1/Chapter9#machine-hunting-%E6%A9%9F%E5%99%A8%E7%8B%A9%E7%8D%B5)  
階層 3: [禁用機器](/docs/part1/Chapter9#disable-mechanisms-%E7%A6%81%E7%94%A8%E6%A9%9F%E5%99%A83-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 4: [機器人戰士](/docs/part1/Chapter9#robot-fighter-%E6%A9%9F%E5%99%A8%E4%BA%BA%E6%88%B0%E5%A3%AB)  
階層 5: [消耗能量](/docs/part1/Chapter9#drain-power-%E6%B6%88%E8%80%97%E8%83%BD%E9%87%8F5-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 6: [失效機制](/docs/part1/Chapter9#deactivate-mechanisms-%E5%A4%B1%E6%95%88%E6%A9%9F%E5%88%B65-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### BEARS A HALO OF FIRE 背負火焰光環
你可以把你的身體套在火焰中，這樣可以保護你和傷害你的敵人。

階層 1: [火焰包覆](/docs/part1/Chapter9#shroud-of-flame-%E7%81%AB%E7%84%B0%E5%8C%85%E8%A6%861-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 2: [火焰投擲](/docs/part1/Chapter9#hurl-flame-%E7%81%AB%E7%84%B0%E6%8A%95%E6%93%B22-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [火焰之翼](/docs/part1/Chapter9#wings-of-fire-%E7%81%AB%E7%84%B0%E4%B9%8B%E7%BF%BC4-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [末日焰手](/docs/part1/Chapter9#fiery-hand-of-doom-%E6%9C%AB%E6%97%A5%E7%84%B0%E6%89%8B3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [火焰劍](/docs/part1/Chapter9#flameblade-%E7%81%AB%E7%84%B0%E5%8A%8D4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [火焰觸鬚](/docs/part1/Chapter9#fire-tendrils-%E7%81%AB%E7%84%B0%E8%A7%B8%E9%AC%9A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [火焰僕從](/docs/part1/Chapter9#fire-servant-%E7%81%AB%E7%84%B0%E5%83%95%E5%BE%9E6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [煉獄軌跡](/docs/part1/Chapter9#inferno-trail-%E7%85%89%E7%8D%84%E8%BB%8C%E8%B7%A16-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### BLAZES WITH RADIANCE 光輝四射
你可以創造光，雕刻它，彎曲它遠離你，或者收集它作為武器使用。

階層 1: [開悟](/docs/part1/Chapter9#enlightened-%E9%96%8B%E6%82%9F)  
階層 1: [照明觸摸](/docs/part1/Chapter9#illuminating-touch-%E7%85%A7%E6%98%8E%E8%A7%B8%E6%91%B81-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [耀眼爆裂](/docs/part1/Chapter9#dazzling-sunburst-%E8%80%80%E7%9C%BC%E7%88%86%E8%A3%822-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [燃燒之光](/docs/part1/Chapter9#burning-light-%E7%87%83%E7%87%92%E4%B9%8B%E5%85%893-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 4: [明耀光塵](/docs/part1/Chapter9#sunlight-%E6%98%8E%E8%80%80%E5%85%89%E5%A1%B53-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [難以察覺](/docs/part1/Chapter9#disappear-%E9%9B%A3%E4%BB%A5%E5%AF%9F%E8%A6%BA4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [活光分子](/docs/part1/Chapter9#living-light-%E6%B4%BB%E5%85%89%E5%88%86%E5%AD%906-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [防禦領域](/docs/part1/Chapter9#defensive-field-%E9%98%B2%E7%A6%A6%E9%A0%98%E5%9F%9F)

### BRANDISHES AN EXOTIC SHIELD 揮舞奇異盾牌
你部署了一個神奇的純力場盾牌，提供保護和一些進攻選項。

階層 1: [力場盾](/docs/part1/Chapter9#force-field-shield-%E5%8A%9B%E5%A0%B4%E7%9B%BE)  
階層 1: [力場猛擊](/docs/part1/Chapter9#force-bash-%E5%8A%9B%E5%A0%B4%E7%8C%9B%E6%93%8A1-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 2: [防護屏障](/docs/part1/Chapter9#enveloping-shield-%E9%98%B2%E8%AD%B7%E5%B1%8F%E9%9A%9C)  
階層 3: [治療脈衝](/docs/part1/Chapter9#healing-pulse-%E6%B2%BB%E7%99%82%E8%84%88%E8%A1%9D3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [投擲力場盾](/docs/part1/Chapter9#throw-force-shield-%E6%8A%95%E6%93%B2%E5%8A%9B%E5%A0%B4%E7%9B%BE)  
階層 4: [能量盾](/docs/part1/Chapter9#energized-shield-%E8%83%BD%E9%87%8F%E7%9B%BE)  
階層 5: [力場牆](/docs/part1/Chapter9#force-wall-%E5%8A%9B%E5%A0%B4%E7%89%865-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [彈跳盾牌](/docs/part1/Chapter9#bouncing-shield-%E5%BD%88%E8%B7%B3%E7%9B%BE%E7%89%8C) 或 [護盾爆裂](/docs/part1/Chapter9#shield-burst-%E8%AD%B7%E7%9B%BE%E7%88%86%E8%A3%82)

### BUILDS ROBOTS 建造機器人
你的機器人創造物按照命令行事。

階層 1: [機器人助手](/docs/part1/Chapter9#robot-assistant-%E6%A9%9F%E5%99%A8%E4%BA%BA%E5%8A%A9%E6%89%8B)  
階層 1: [機器人建造師](/docs/part1/Chapter9#robot-builder-%E6%A9%9F%E5%99%A8%E4%BA%BA%E5%BB%BA%E9%80%A0%E5%B8%AB)  
階層 2: [機器人控制](/docs/part1/Chapter9#robot-control-%E6%A9%9F%E5%99%A8%E4%BA%BA%E6%8E%A7%E5%88%B62-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [專家追隨者](/docs/part1/Chapter9#expert-follower-%E5%B0%88%E5%AE%B6%E8%BF%BD%E9%9A%A8%E8%80%85) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 4: [機器人升級](/docs/part1/Chapter9#robot-upgrade-%E6%A9%9F%E5%99%A8%E4%BA%BA%E5%8D%87%E7%B4%9A)  
階層 5: [機器人艦隊](/docs/part1/Chapter9#robot-fleet-%E6%A9%9F%E5%99%A8%E4%BA%BA%E8%89%A6%E9%9A%8A)  
階層 6: [機器人進化](/docs/part1/Chapter9#robot-evolution-%E6%A9%9F%E5%99%A8%E4%BA%BA%E9%80%B2%E5%8C%96) 或 [機器人升級](/docs/part1/Chapter9#robot-upgrade-%E6%A9%9F%E5%99%A8%E4%BA%BA%E5%8D%87%E7%B4%9A)  

### CALCULATES THE INCALCULABLE 計算不可估量
令人敬畏的數學能力允許你實時建模世界，讓你超越任何人的界限。

階層 1: [預測方程式](/docs/part1/Chapter9#predictive-equation-%E9%A0%90%E6%B8%AC%E6%96%B9%E7%A8%8B%E5%BC%8F2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [高等數學](/docs/part1/Chapter9#higher-mathematics-%E9%AB%98%E7%AD%89%E6%95%B8%E5%AD%B8)  
階層 2: [預測模型](/docs/part1/Chapter9#predictive-model-%E9%A0%90%E6%B8%AC%E6%A8%A1%E5%9E%8B2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [潛意識防禦](/docs/part1/Chapter9#subconscious-defense-%E6%BD%9B%E6%84%8F%E8%AD%98%E9%98%B2%E7%A6%A6) 或 [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 4: [認知攻擊](/docs/part1/Chapter9#cognizant-offense-%E8%AA%8D%E7%9F%A5%E6%94%BB%E6%93%8A)  
階層 5: [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 5: [未來數學](/docs/part1/Chapter9#further-mathematics-%E6%9C%AA%E4%BE%86%E6%95%B8%E5%AD%B8)  
階層 6: [了解未知](/docs/part1/Chapter9#knowing-the-unknown-%E4%BA%86%E8%A7%A3%E6%9C%AA%E7%9F%A56-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)

### CHANNELS DIVINE BLESSINGS 引導神性祝福
作為一個神性存在的虔誠追隨者，你引導你的神的力量去創造奇蹟。

階層 1: [眾神祝福](/docs/part1/Chapter9#blessing-of-the-gods-%E7%9C%BE%E7%A5%9E%E7%A5%9D%E7%A6%8F)  
階層 2: [強化智力](/docs/part1/Chapter9#enhanced-intellect-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 3: [神聖光輝](/docs/part1/Chapter9#divine-radiance-%E7%A5%9E%E8%81%96%E5%85%89%E8%BC%9D2-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [火焰綻放](/docs/part1/Chapter9#fire-bloom-%E7%81%AB%E7%84%B0%E7%B6%BB%E6%94%BE4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [霸氣](/docs/part1/Chapter9#overawe-%E9%9C%B8%E6%B0%A35-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [神性干預](/docs/part1/Chapter9#divine-intervention-%E7%A5%9E%E6%80%A7%E5%B9%B2%E9%A0%902-%E9%BB%9E%E6%99%BA%E5%8A%9B%E6%88%96-2-%E9%BB%9E%E6%99%BA%E5%8A%9B--4-xp)  
階層 6: [神聖符號](/docs/part1/Chapter9#divine-symbol-%E7%A5%9E%E8%81%96%E7%AC%A6%E8%99%9F5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [召喚惡魔](/docs/part1/Chapter9#summon-demon-%E5%8F%AC%E5%96%9A%E6%83%A1%E9%AD%947-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### COMMANDS MENTAL POWERS 命令精神力量
你可以從夢中提取圖像，把它們帶到現實生活中。

階層 1: [心靈通訊](/docs/part1/Chapter9#telepathic-%E5%BF%83%E9%9D%88%E9%80%9A%E8%A8%8A1-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 2: [讀心](/docs/part1/Chapter9#mind-reading-%E8%AE%80%E5%BF%832-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [心靈爆裂](/docs/part1/Chapter9#psychic-burst-%E5%BF%83%E9%9D%88%E7%88%86%E8%A3%823-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [通靈建議](/docs/part1/Chapter9#psychic-suggestion-%E9%80%9A%E9%9D%88%E5%BB%BA%E8%AD%B04-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [使用他人感官](/docs/part1/Chapter9#use-senses-of-others-%E4%BD%BF%E7%94%A8%E4%BB%96%E4%BA%BA%E6%84%9F%E5%AE%984-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [預知](/docs/part1/Chapter9#precognition-%E9%A0%90%E7%9F%A56-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [精神控制](/docs/part1/Chapter9#mind-control-%E7%B2%BE%E7%A5%9E%E6%8E%A7%E5%88%B66-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [心靈通訊網](/docs/part1/Chapter9#telepathic-network-%E5%BF%83%E9%9D%88%E9%80%9A%E8%A8%8A%E7%B6%B20-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### CONDUCTS WEIRD SCIENCE 進行怪異科學
你超乎尋常的洞察力和能力使你成為一個有能力做出驚人壯舉的科學家。

階層 1: [實驗室分析](/docs/part1/Chapter9#lab-analysis-%E5%AF%A6%E9%A9%97%E5%AE%A4%E5%88%86%E6%9E%903-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD)  
階層 2: [修改設備](/docs/part1/Chapter9#modify-device-%E4%BF%AE%E6%94%B9%E8%A8%AD%E5%82%994-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [通過化學更好地生活](/docs/part1/Chapter9#better-living-through-chemistry-%E9%80%9A%E9%81%8E%E5%8C%96%E5%AD%B8%E6%9B%B4%E5%A5%BD%E5%9C%B0%E7%94%9F%E6%B4%BB4-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [荒誕健康](/docs/part1/Chapter9#incredible-health-%E8%8D%92%E8%AA%95%E5%81%A5%E5%BA%B7)  
階層 4: [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD)  
階層 4: [有點瘋狂](/docs/part1/Chapter9#just-a-bit-mad-%E6%9C%89%E9%BB%9E%E7%98%8B%E7%8B%82)  
階層 5: [怪異科學突破](/docs/part1/Chapter9#weird-science-breakthrough-%E6%80%AA%E7%95%B0%E7%A7%91%E5%AD%B8%E7%AA%81%E7%A0%B45-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [發明家](/docs/part1/Chapter9#inventor-%E7%99%BC%E6%98%8E%E5%AE%B6) 或 [防禦領域](/docs/part1/Chapter9#defensive-field-%E9%98%B2%E7%A6%A6%E9%A0%98%E5%9F%9F)

### CONSORTS WITH THE DEAD 與死者為伍
死者回答你的問題，他們復活的屍體為你服務。

階層 1: [與亡靈交談](/docs/part1/Chapter9#speaker-for-the-dead-%E8%88%87%E4%BA%A1%E9%9D%88%E4%BA%A4%E8%AB%872-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [死靈術](/docs/part1/Chapter9#necromancy-%E6%AD%BB%E9%9D%88%E8%A1%933-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [閱讀房間](/docs/part1/Chapter9#reading-the-room-%E9%96%B1%E8%AE%80%E6%88%BF%E9%96%933-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [修復肉體](/docs/part1/Chapter9#repair-flesh-%E4%BF%AE%E5%BE%A9%E8%82%89%E9%AB%943-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [高級死靈術](/docs/part1/Chapter9#greater-necromancy-%E9%AB%98%E7%B4%9A%E6%AD%BB%E9%9D%88%E8%A1%935-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [可怕凝視](/docs/part1/Chapter9#terrifying-gaze-%E5%8F%AF%E6%80%95%E5%87%9D%E8%A6%966-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [真死靈術](/docs/part1/Chapter9#true-necromancy-%E7%9C%9F%E6%AD%BB%E9%9D%88%E8%A1%938-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [死亡令詞](/docs/part1/Chapter9#word-of-death-%E6%AD%BB%E4%BA%A1%E4%BB%A4%E8%A9%9E5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### CONTROLS BEASTS 控制野獸
你的溝通能力和領導野獸的能力是不可思議的。

階層 1: [野獸夥伴](/docs/part1/Chapter9#beast-companion-%E9%87%8E%E7%8D%B8%E5%A4%A5%E4%BC%B4)  
階層 2: [安撫野蠻](/docs/part1/Chapter9#soothe-the-savage-%E5%AE%89%E6%92%AB%E9%87%8E%E8%A0%BB2-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 2: [傳達概念](/docs/part1/Chapter9#communication-%E5%82%B3%E9%81%94%E6%A6%82%E5%BF%B52-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [坐騎](/docs/part1/Chapter9#mount-%E5%9D%90%E9%A8%8E) 或 [一起變強](/docs/part1/Chapter9#stronger-together-%E4%B8%80%E8%B5%B7%E8%AE%8A%E5%BC%B7)  
階層 4: [野獸之眼](/docs/part1/Chapter9#beast-eyes-%E9%87%8E%E7%8D%B8%E4%B9%8B%E7%9C%BC3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [改進同伴](/docs/part1/Chapter9#improved-companion-%E6%94%B9%E9%80%B2%E5%90%8C%E4%BC%B4)  
階層 5: [野獸召喚](/docs/part1/Chapter9#beast-call-%E9%87%8E%E7%8D%B8%E5%8F%AC%E5%96%9A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [如同一體](/docs/part1/Chapter9#as-if-one-creature-%E5%A6%82%E5%90%8C%E4%B8%80%E9%AB%94) 或 [控制野蠻](/docs/part1/Chapter9#control-the-savage-%E6%8E%A7%E5%88%B6%E9%87%8E%E8%A0%BB6-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### CONTROLS GRAVITY 控制重力
你可以改變重力本身的吸引力。

類型能力交換選項: [變重](/docs/part1/Chapter9#weighty-%E8%AE%8A%E9%87%8D1-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [懸浮](/docs/part1/Chapter9#hover-%E6%87%B8%E6%B5%AE2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [強化速度節省值](/docs/part1/Chapter9#enhanced-speed-edge-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 3: [定義向下](/docs/part1/Chapter9#define-down-%E5%AE%9A%E7%BE%A9%E5%90%91%E4%B8%8B4-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [重力劈砍](/docs/part1/Chapter9#gravity-cleave-%E9%87%8D%E5%8A%9B%E5%8A%88%E7%A0%8D3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [重力場](/docs/part1/Chapter9#field-of-gravity-%E9%87%8D%E5%8A%9B%E5%A0%B44-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [浮空飛行](/docs/part1/Chapter9#flight-%E6%B5%AE%E7%A9%BA%E9%A3%9B%E8%A1%8C4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [改進重力劈砍](/docs/part1/Chapter9#improved-gravity-cleave-%E6%94%B9%E9%80%B2%E9%87%8D%E5%8A%9B%E5%8A%88%E7%A0%8D9-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [世界的重量](/docs/part1/Chapter9#weight-of-the-world-%E4%B8%96%E7%95%8C%E7%9A%84%E9%87%8D%E9%87%8F6-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### CRAFTS ILLUSIONS 工藝幻術
你在燈光下的時尚形像是如此的完美，它們看起來就像真的一樣。

階層 1: [弱效幻象](/docs/part1/Chapter9#minor-illusion-%E5%BC%B1%E6%95%88%E5%B9%BB%E8%B1%A11-%E9%BB%9E%E6%99%BA%E5%8A%9B)
階層 2: [幻象偽裝](/docs/part1/Chapter9#illusory-disguise-%E5%B9%BB%E8%B1%A1%E5%81%BD%E8%A3%9D2%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [施放幻術](/docs/part1/Chapter9#cast-illusion-%E6%96%BD%E6%94%BE%E5%B9%BB%E8%A1%93) 或 [強效幻象](/docs/part1/Chapter9#major-illusion-%E5%BC%B7%E6%95%88%E5%B9%BB%E8%B1%A13-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [虛幻自我](/docs/part1/Chapter9#illusory-selves-%E8%99%9B%E5%B9%BB%E8%87%AA%E6%88%914-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [可怕圖像](/docs/part1/Chapter9#terrifying-image-%E5%8F%AF%E6%80%95%E5%9C%96%E5%83%8F6-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [宏偉幻覺](/docs/part1/Chapter9#grandiose-illusion-%E5%AE%8F%E5%81%89%E5%B9%BB%E8%A6%BA8-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [永久幻象](/docs/part1/Chapter9#permanent-illusion-%E6%B0%B8%E4%B9%85%E5%B9%BB%E8%B1%A19-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### CRAFTS UNIQUE OBJECTS 創造獨特物品
你發明了奇怪而有用的東西。

階層 1: [工匠](/docs/part1/Chapter9#crafter-%E5%B7%A5%E5%8C%A0)  
階層 1: [鑑定大師](/docs/part1/Chapter9#master-identifier-%E9%91%91%E5%AE%9A%E5%A4%A7%E5%B8%AB)  
階層 2: [神器修補者](/docs/part1/Chapter9#artifact-tinkerer-%E7%A5%9E%E5%99%A8%E4%BF%AE%E8%A3%9C%E8%80%85)  
階層 2: [快速工作](/docs/part1/Chapter9#quick-work-%E5%BF%AB%E9%80%9F%E5%B7%A5%E4%BD%9C3-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [匠人大師](/docs/part1/Chapter9#master-crafter-%E5%8C%A0%E4%BA%BA%E5%A4%A7%E5%B8%AB) 或 [內置武裝](/docs/part1/Chapter9#built-in-weaponry-%E5%85%A7%E7%BD%AE%E6%AD%A6%E8%A3%9D)  
階層 4: [秘具匠](/docs/part1/Chapter9#cyphersmith-%E7%A7%98%E5%85%B7%E5%8C%A0)  
階層 5: [創新者](/docs/part1/Chapter9#innovator-%E5%89%B5%E6%96%B0%E8%80%85)  
階層 6: [發明家](/docs/part1/Chapter9#inventor-%E7%99%BC%E6%98%8E%E5%AE%B6) 或 [融合裝甲](/docs/part1/Chapter9#fusion-armor-%E8%9E%8D%E5%90%88%E8%A3%9D%E7%94%B2)

### DANCES WITH DARK MATTER 與暗物質共舞
你可以操縱陰影和 “黑暗” 物質。

階層 1: [暗物質帶](/docs/part1/Chapter9#ribbons-of-dark-matter-%E6%9A%97%E7%89%A9%E8%B3%AA%E5%B8%B62-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [虛空之翼](/docs/part1/Chapter9#void-wings-%E8%99%9B%E7%A9%BA%E4%B9%8B%E7%BF%BC3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [暗物質包覆](/docs/part1/Chapter9#dark-matter-shroud-%E6%9A%97%E7%89%A9%E8%B3%AA%E5%8C%85%E8%A6%864-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [暗物質打擊](/docs/part1/Chapter9#dark-matter-strike-%E6%9A%97%E7%89%A9%E8%B3%AA%E6%89%93%E6%93%8A4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [暗物質外殼](/docs/part1/Chapter9#dark-matter-shell-%E6%9A%97%E7%89%A9%E8%B3%AA%E5%A4%96%E6%AE%BC5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [風之旅人](/docs/part1/Chapter9#windwracked-traveler-%E9%A2%A8%E4%B9%8B%E6%97%85%E4%BA%BA4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [暗物質結構](/docs/part1/Chapter9#dark-matter-structure-%E6%9A%97%E7%89%A9%E8%B3%AA%E7%B5%90%E6%A7%8B5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [擁抱黑夜](/docs/part1/Chapter9#embrace-the-night-%E6%93%81%E6%8A%B1%E9%BB%91%E5%A4%9C7-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### DEFENDS THE GATE 守門
在吵架的時候，每個人都希望你站在他們這一邊，因為沒有什麼能通過你。

階層 1: [防禦位置](/docs/part1/Chapter9#fortified-position-%E9%98%B2%E7%A6%A6%E4%BD%8D%E7%BD%AE2-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 1: [向我集合](/docs/part1/Chapter9#rally-to-me-%E5%90%91%E6%88%91%E9%9B%86%E5%90%882-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [心理遊戲](/docs/part1/Chapter9#mind-games-%E5%BF%83%E7%90%86%E9%81%8A%E6%88%B23-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [防禦建造者](/docs/part1/Chapter9#fortification-builder-%E9%98%B2%E7%A6%A6%E5%BB%BA%E9%80%A0%E8%80%85) 或 [轉移攻擊](/docs/part1/Chapter9#divert-attacks-%E8%BD%89%E7%A7%BB%E6%94%BB%E6%93%8A4-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 4: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 5: [強化力場](/docs/part1/Chapter9#reinforcing-field-%E5%BC%B7%E5%8C%96%E5%8A%9B%E5%A0%B46-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [產生力場](/docs/part1/Chapter9#generate-force-field-%E7%94%A2%E7%94%9F%E5%8A%9B%E5%A0%B49-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [震懾攻擊](/docs/part1/Chapter9#stun-attack-%E9%9C%87%E6%87%BE%E6%94%BB%E6%93%8A6-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### DEFENDS THE WEAK 保護弱者
你為無助、軟弱和無保護的人挺身而出。

階層 1: [勇敢](/docs/part1/Chapter9#courageous-%E5%8B%87%E6%95%A2)  
階層 1: [盾牌防護](/docs/part1/Chapter9#warding-shield-%E7%9B%BE%E7%89%8C%E9%98%B2%E8%AD%B7)  
階層 2: [忠誠防禦者](/docs/part1/Chapter9#devoted-defender-%E5%BF%A0%E8%AA%A0%E9%98%B2%E7%A6%A6%E8%80%852-%E9%BB%9E%E6%B0%A3%E5%8A%9B%E6%88%96%E6%99%BA%E5%8A%9B)  
階層 2: [洞察力](/docs/part1/Chapter9#insight-%E6%B4%9E%E5%AF%9F%E5%8A%9B)  
階層 3: [雙重護衛](/docs/part1/Chapter9#dual-wards-%E9%9B%99%E9%87%8D%E8%AD%B7%E8%A1%9B) 或 [真正守護者](/docs/part1/Chapter9#true-guardian-%E7%9C%9F%E6%AD%A3%E5%AE%88%E8%AD%B7%E8%80%852-%E9%BB%9E%E6%B0%A3%E5%8A%9B%E6%88%96%E6%99%BA%E5%8A%9B)  
階層 4: [戰鬥挑戰](/docs/part1/Chapter9#combat-challenge-%E6%88%B0%E9%AC%A5%E6%8C%91%E6%88%B0)  
階層 5: [自願犧牲](/docs/part1/Chapter9#willing-sacrifice-%E8%87%AA%E9%A1%98%E7%8A%A7%E7%89%B2)  
階層 6: [復甦術](/docs/part1/Chapter9#resuscitate-%E5%BE%A9%E7%94%A6%E8%A1%936-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [真正的防禦者](/docs/part1/Chapter9#true-defender-%E7%9C%9F%E6%AD%A3%E7%9A%84%E9%98%B2%E7%A6%A6%E8%80%856-%E9%BB%9E%E6%B0%A3%E5%8A%9B%E6%88%96%E6%99%BA%E5%8A%9B)

### DESCENDS FROM NOBILITY 源於貴族
作為財富和權力的後代，你擁有高貴的頭銜和特權教養賦予你的能力。

類型能力交換選項: [隨從](/docs/part1/Chapter9#retinue-%E9%9A%A8%E5%BE%9E)  
階層 1: [貴族特權](/docs/part1/Chapter9#privileged-nobility-%E8%B2%B4%E6%97%8F%E7%89%B9%E6%AC%8A)  
階層 2: [受訓談話者](/docs/part1/Chapter9#trained-interlocutor-%E5%8F%97%E8%A8%93%E8%AB%87%E8%A9%B1%E8%80%85)  
階層 3: [高級命令](/docs/part1/Chapter9#advanced-command-%E9%AB%98%E7%B4%9A%E5%91%BD%E4%BB%A47-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [貴族勇氣](/docs/part1/Chapter9#nobles-courage-%E8%B2%B4%E6%97%8F%E5%8B%87%E6%B0%A33-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [專家追隨者](/docs/part1/Chapter9#expert-follower-%E5%B0%88%E5%AE%B6%E8%BF%BD%E9%9A%A8%E8%80%85)  
階層 5: [維護你的特權](/docs/part1/Chapter9#asserting-your-privilege-%E7%B6%AD%E8%AD%B7%E4%BD%A0%E7%9A%84%E7%89%B9%E6%AC%8A3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [能夠協助](/docs/part1/Chapter9#able-assistance-%E8%83%BD%E5%A4%A0%E5%8D%94%E5%8A%A9) 或 [領袖思維](/docs/part1/Chapter9#mind-of-a-leader-%E9%A0%98%E8%A2%96%E6%80%9D%E7%B6%AD6-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### DOESN’T DO MUCH 不多做
你是個懶鬼，但是你對很多事情都知道一點。

階層 1: [生活課](/docs/part1/Chapter9#life-lessons-%E7%94%9F%E6%B4%BB%E8%AA%B2)  
階層 2: [完全冷靜](/docs/part1/Chapter9#totally-chill-%E5%AE%8C%E5%85%A8%E5%86%B7%E9%9D%9C)  
階層 3: [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD) 或 [即興發揮](/docs/part1/Chapter9#improvise-%E5%8D%B3%E8%88%88%E7%99%BC%E6%8F%AE3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [生活課](/docs/part1/Chapter9#life-lessons-%E7%94%9F%E6%B4%BB%E8%AA%B2)  
階層 4: [高級防禦技能](/docs/part1/Chapter9#greater-skill-with-defense-%E9%AB%98%E7%B4%9A%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 5: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 6: [汲取人生經驗](/docs/part1/Chapter9#drawing-on-lifes-experiences-%E6%B1%B2%E5%8F%96%E4%BA%BA%E7%94%9F%E7%B6%93%E9%A9%976-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [快速機智](/docs/part1/Chapter9#quick-wits-%E5%BF%AB%E9%80%9F%E6%A9%9F%E6%99%BA)

### DRIVES LIKE A MANIAC 像瘋子一樣駕駛
無論是在兩輪上保持平衡，跳上另一輛載具，還是迎面駛來的敵人的載具，當你坐在方向盤上時，你都不會考慮到風險。

階層 1: [駕駛陸行載具](/docs/part1/Chapter9#driver-%E9%A7%95%E9%A7%9B%E9%99%B8%E8%A1%8C%E8%BC%89%E5%85%B7)  
階層 1: [在邊緣駕駛](/docs/part1/Chapter9#driving-on-the-edge-%E5%9C%A8%E9%82%8A%E7%B7%A3%E9%A7%95%E9%A7%9B)  
階層 2: [汽車衝浪者](/docs/part1/Chapter9#car-surfer-%E6%B1%BD%E8%BB%8A%E8%A1%9D%E6%B5%AA%E8%80%85)  
階層 2: [盯著他們](/docs/part1/Chapter9#stare-them-down-%E7%9B%AF%E8%91%97%E4%BB%96%E5%80%91)  
階層 3: [專業司機](/docs/part1/Chapter9#expert-driver-%E5%B0%88%E6%A5%AD%E5%8F%B8%E6%A9%9F) 或 [強化速度節省值](/docs/part1/Chapter9#enhanced-speed-edge-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 4: [銳眼](/docs/part1/Chapter9#sharp-eyed-%E9%8A%B3%E7%9C%BC)  
階層 4: [強化速度](/docs/part1/Chapter9#enhanced-speed-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 5: [公路狂飆](/docs/part1/Chapter9#something-in-the-road-%E5%85%AC%E8%B7%AF%E7%8B%82%E9%A3%86)  
階層 6: [詭詐司機](/docs/part1/Chapter9#trick-driver-%E8%A9%AD%E8%A9%90%E5%8F%B8%E6%A9%9F) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)  

### EMERGED FROM THE OBELISK 湧自方尖碑
你的身體，堅硬如水晶，給你一套獨特的能力，與浮動水晶方尖碑互動後獲得的。

階層 1: [水晶軀體](/docs/part1/Chapter9#crystalline-body-%E6%B0%B4%E6%99%B6%E8%BB%80%E9%AB%94)  
階層 2: [懸浮](/docs/part1/Chapter9#hover-%E6%87%B8%E6%B5%AE2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [棲息水晶](/docs/part1/Chapter9#inhabit-crystal-%E6%A3%B2%E6%81%AF%E6%B0%B4%E6%99%B64-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [難以撼動](/docs/part1/Chapter9#immovable-%E9%9B%A3%E4%BB%A5%E6%92%BC%E5%8B%95)  
階層 4: [水晶透鏡](/docs/part1/Chapter9#crystal-lens-%E6%B0%B4%E6%99%B6%E9%80%8F%E9%8F%A1)  
階層 5: [共振頻率](/docs/part1/Chapter9#resonant-frequency-%E5%85%B1%E6%8C%AF%E9%A0%BB%E7%8E%87)  
階層 6: [共鳴震盪](/docs/part1/Chapter9#resonant-quake-%E5%85%B1%E9%B3%B4%E9%9C%87%E7%9B%AA7-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [回到方尖碑](/docs/part1/Chapter9#return-to-the-obelisk-%E5%9B%9E%E5%88%B0%E6%96%B9%E5%B0%96%E7%A2%917-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### EMPLOYS MAGNETISM 磁力掌控
你掌握金屬和磁性的力量。

階層 1: [移動金屬](/docs/part1/Chapter9#move-metal-%E7%A7%BB%E5%8B%95%E9%87%91%E5%B1%AC1-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [擊退金屬](/docs/part1/Chapter9#repel-metal-%E6%93%8A%E9%80%80%E9%87%91%E5%B1%AC)  
階層 3: [摧毀金屬](/docs/part1/Chapter9#destroy-metal-%E6%91%A7%E6%AF%80%E9%87%91%E5%B1%AC3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [引導矢](/docs/part1/Chapter9#guide-bolt-%E5%BC%95%E5%B0%8E%E7%9F%A24-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [磁場](/docs/part1/Chapter9#magnetic-field-%E7%A3%81%E5%A0%B44-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [命令金屬](/docs/part1/Chapter9#command-metal-%E5%91%BD%E4%BB%A4%E9%87%91%E5%B1%AC5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [萬物磁化](/docs/part1/Chapter9#diamagnetism-%E8%90%AC%E7%89%A9%E7%A3%81%E5%8C%96) 或 [鋼鐵痛擊](/docs/part1/Chapter9#iron-punch-%E9%8B%BC%E9%90%B5%E7%97%9B%E6%93%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### ENTERTAINS 娛樂
你的表演，主要是為了他人的利益。

階層 1: [輕浮](/docs/part1/Chapter9#levity-%E8%BC%95%E6%B5%AE)  
階層 2: [鼓舞言詞](/docs/part1/Chapter9#inspiration-%E9%BC%93%E8%88%9E%E8%A8%80%E8%A9%9E6-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD) 或 [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 4: [冷靜](/docs/part1/Chapter9#calm-%E5%86%B7%E9%9D%9C3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [能夠協助](/docs/part1/Chapter9#able-assistance-%E8%83%BD%E5%A4%A0%E5%8D%94%E5%8A%A9)  
階層 6: [大師級藝人](/docs/part1/Chapter9#master-entertainer-%E5%A4%A7%E5%B8%AB%E7%B4%9A%E8%97%9D%E4%BA%BA) 或 [報復性表演](/docs/part1/Chapter9#vindictive-performance-%E5%A0%B1%E5%BE%A9%E6%80%A7%E8%A1%A8%E6%BC%945-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### EXISTS IN TWO PLACES AT ONCE 存在於兩地
你同時存在於兩個地方。

階層 1: [虛幻複製體](/docs/part1/Chapter9#illusory-duplicate-%E8%99%9B%E5%B9%BB%E8%A4%87%E8%A3%BD%E9%AB%942-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [分享感覺](/docs/part1/Chapter9#share-senses-%E5%88%86%E4%BA%AB%E6%84%9F%E8%A6%BA)  
階層 3: [強效複製體](/docs/part1/Chapter9#superior-duplicate-%E5%BC%B7%E6%95%88%E8%A4%87%E8%A3%BD%E9%AB%942-%E6%B0%A3%E5%8A%9B%E9%BB%9E) 或 [強韌複製體](/docs/part1/Chapter9#resilient-duplicate-%E5%BC%B7%E9%9F%8C%E8%A4%87%E8%A3%BD%E9%AB%94)  
階層 4: [傷害轉移](/docs/part1/Chapter9#damage-transference-%E5%82%B7%E5%AE%B3%E8%BD%89%E7%A7%BB)  
階層 5: [協調努力](/docs/part1/Chapter9#coordinated-effort-%E5%8D%94%E8%AA%BF%E5%8A%AA%E5%8A%9B3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [多重性](/docs/part1/Chapter9#multiplicity-%E5%A4%9A%E9%87%8D%E6%80%A76-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [強韌複製體](/docs/part1/Chapter9#resilient-duplicate-%E5%BC%B7%E9%9F%8C%E8%A4%87%E8%A3%BD%E9%AB%94)

### EXISTS PARTIALLY OUT OF PHASE 存在相位離散
你有點半透明，從相位中略為分離，可以通過固體物體。

階層 1: [穿越牆壁](/docs/part1/Chapter9#walk-through-walls-%E7%A9%BF%E8%B6%8A%E7%89%86%E5%A3%812-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [相位防禦](/docs/part1/Chapter9#defensive-phasing-%E7%9B%B8%E4%BD%8D%E9%98%B2%E7%A6%A62-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [相位攻擊](/docs/part1/Chapter9#phased-attack-%E7%9B%B8%E4%BD%8D%E6%94%BB%E6%93%8A3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [相位門](/docs/part1/Chapter9#phase-door-%E7%9B%B8%E4%BD%8D%E9%96%804-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [幽靈](/docs/part1/Chapter9#ghost-%E5%B9%BD%E9%9D%884-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [不可觸碰](/docs/part1/Chapter9#untouchable-%E4%B8%8D%E5%8F%AF%E8%A7%B8%E7%A2%B06-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [強化相位攻擊](/docs/part1/Chapter9#enhanced-phased-attack-%E5%BC%B7%E5%8C%96%E7%9B%B8%E4%BD%8D%E6%94%BB%E6%93%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [相位敵人](/docs/part1/Chapter9#phase-foe-%E7%9B%B8%E4%BD%8D%E6%95%B5%E4%BA%BA6-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### EXPLORES DARK PLACES 探索黑暗之地
你是典型的寶藏獵人、拾荒者和尋寶者。

階層 1: [超級探險家](/docs/part1/Chapter9#superb-explorer-%E8%B6%85%E7%B4%9A%E6%8E%A2%E9%9A%AA%E5%AE%B6)  
階層 2: [超級滲透者](/docs/part1/Chapter9#superb-infiltrator-%E8%B6%85%E7%B4%9A%E6%BB%B2%E9%80%8F%E8%80%85)  
階層 2: [眼睛調整](/docs/part1/Chapter9#eyes-adjusted-%E7%9C%BC%E7%9D%9B%E8%AA%BF%E6%95%B4)  
階層 3: [夜襲](/docs/part1/Chapter9#nightstrike-%E5%A4%9C%E8%A5%B2) 或 [滑溜顧客](/docs/part1/Chapter9#slippery-customer-%E6%BB%91%E6%BA%9C%E9%A1%A7%E5%AE%A2)  
階層 4: [堅忍韌性](/docs/part1/Chapter9#hard-won-resilience-%E5%A0%85%E5%BF%8D%E9%9F%8C%E6%80%A7)  
階層 5: [黑暗探索者](/docs/part1/Chapter9#dark-explorer-%E9%BB%91%E6%9A%97%E6%8E%A2%E7%B4%A2%E8%80%85)  
階層 6: [致盲攻擊](/docs/part1/Chapter9#blinding-attack-%E8%87%B4%E7%9B%B2%E6%94%BB%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [被黑暗所擁抱](/docs/part1/Chapter9#embraced-by-darkness-%E8%A2%AB%E9%BB%91%E6%9A%97%E6%89%80%E6%93%81%E6%8A%B16-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### FIGHTS DIRTY 骯髒戰鬥
為了贏得一場戰鬥，你會做任何事:咬、抓、踢、捉弄，甚至更糟。

階層 1: [追蹤者](/docs/part1/Chapter9#tracker-%E8%BF%BD%E8%B9%A4%E8%80%85)  
階層 1: [潛行者](/docs/part1/Chapter9#stalker-%E6%BD%9B%E8%A1%8C%E8%80%85)  
階層 2: [潛匿](/docs/part1/Chapter9#sneak-%E6%BD%9B%E5%8C%BF)  
階層 2: [獵物](/docs/part1/Chapter9#quarry-%E7%8D%B5%E7%89%A92-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [不講武德](/docs/part1/Chapter9#betrayal-%E4%B8%8D%E8%AC%9B%E6%AD%A6%E5%BE%B7) 或 [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 4: [心理遊戲](/docs/part1/Chapter9#mind-games-%E5%BF%83%E7%90%86%E9%81%8A%E6%88%B23-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [能幹戰士](/docs/part1/Chapter9#capable-warrior-%E8%83%BD%E5%B9%B9%E6%88%B0%E5%A3%AB)  
階層 5: [利用環境](/docs/part1/Chapter9#using-the-environment-%E5%88%A9%E7%94%A8%E7%92%B0%E5%A2%834-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [扭曲刀刃](/docs/part1/Chapter9#twisting-the-knife-%E6%89%AD%E6%9B%B2%E5%88%80%E5%88%834-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [謀殺者](/docs/part1/Chapter9#murderer-%E8%AC%80%E6%AE%BA%E8%80%858-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### FIGHTS WITH PANACHE 華麗戰鬥
你是一個虛張聲勢的膽大妄為者，以華麗的風格打鬥，看起來很有趣。

階層 1: [華麗攻擊](/docs/part1/Chapter9#attack-flourish-%E8%8F%AF%E9%BA%97%E6%94%BB%E6%93%8A)  
階層 2: [快速格擋](/docs/part1/Chapter9#quick-block-%E5%BF%AB%E9%80%9F%E6%A0%BC%E6%93%8B)  
階層 3: [雜技攻擊](/docs/part1/Chapter9#acrobatic-attack-%E9%9B%9C%E6%8A%80%E6%94%BB%E6%93%8A1-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [浮誇吹噓](/docs/part1/Chapter9#flamboyant-boast-%E6%B5%AE%E8%AA%87%E5%90%B9%E5%99%931-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [為他人阻擋](/docs/part1/Chapter9#block-for-another-%E7%82%BA%E4%BB%96%E4%BA%BA%E9%98%BB%E6%93%8B)  
階層 4: [快速殺戮](/docs/part1/Chapter9#fast-kill-%E5%BF%AB%E9%80%9F%E6%AE%BA%E6%88%AE2-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 5: [利用環境](/docs/part1/Chapter9#using-the-environment-%E5%88%A9%E7%94%A8%E7%92%B0%E5%A2%834-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [敏捷智慧](/docs/part1/Chapter9#agile-wit-%E6%95%8F%E6%8D%B7%E6%99%BA%E6%85%A7) 或 [回敬原主](/docs/part1/Chapter9#return-to-sender-%E5%9B%9E%E6%95%AC%E5%8E%9F%E4%B8%BB3-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### FLIES FASTER THAN A BULLET 飛得比子彈快
你能飛，而且你超級強壯，很難受傷，而且速度也很快。有什麼是你做不到的嗎?

階層 1: [懸浮](/docs/part1/Chapter9#hover-%E6%87%B8%E6%B5%AE2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 3: [隱藏儲備](/docs/part1/Chapter9#hidden-reserves-%E9%9A%B1%E8%97%8F%E5%84%B2%E5%82%99) 或 [看透物質](/docs/part1/Chapter9#see-through-matter-%E7%9C%8B%E9%80%8F%E7%89%A9%E8%B3%AA3-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [眨眼瞬間](/docs/part1/Chapter9#blink-of-an-eye-%E7%9C%A8%E7%9C%BC%E7%9E%AC%E9%96%934-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 4: [速度上升](/docs/part1/Chapter9#up-to-speed-%E9%80%9F%E5%BA%A6%E4%B8%8A%E5%8D%87)  
階層 5: [尚未死亡](/docs/part1/Chapter9#not-dead-yet-%E5%B0%9A%E6%9C%AA%E6%AD%BB%E4%BA%A1)  
階層 6: [燃燒之光](/docs/part1/Chapter9#burning-light-%E7%87%83%E7%87%92%E4%B9%8B%E5%85%893-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [無視苦痛](/docs/part1/Chapter9#ignore-affliction-%E7%84%A1%E8%A6%96%E8%8B%A6%E7%97%9B5-%E6%B0%A3%E5%8A%9B%E9%BB%9E)

### FOCUSES MIND OVER MATTER 專注精神而非物質
你可以用心靈感應移動物體，而不用身體接觸它們。

階層 1: [轉移攻擊](/docs/part1/Chapter9#divert-attacks-%E8%BD%89%E7%A7%BB%E6%94%BB%E6%93%8A4-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 2: [心靈傳動](/docs/part1/Chapter9#telekinesis-%E5%BF%83%E9%9D%88%E5%82%B3%E5%8B%952-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [機運斗篷](/docs/part1/Chapter9#cloak-of-opportunity-%E6%A9%9F%E9%81%8B%E6%96%97%E7%AF%B75-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [增強力量](/docs/part1/Chapter9#enhance-strength-%E5%A2%9E%E5%BC%B7%E5%8A%9B%E9%87%8F3-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [實物招來](/docs/part1/Chapter9#apportation-%E5%AF%A6%E7%89%A9%E6%8B%9B%E4%BE%864-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [念力攻擊](/docs/part1/Chapter9#psychokinetic-attack-%E5%BF%B5%E5%8A%9B%E6%94%BB%E6%93%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [改進實物](/docs/part1/Chapter9#improved-apportation-%E6%94%B9%E9%80%B2%E5%AF%A6%E7%89%A96-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [重塑法](/docs/part1/Chapter9#reshape-%E9%87%8D%E5%A1%91%E6%B3%955-%E9%BB%9E%E6%99%BA%E5%8A%9B) 

### FUSES FLESH AND STEEL 鋼鐵肉軀
你的身體有一部分是機器。

階層 1: [增強身體](/docs/part1/Chapter9#enhanced-body-%E5%A2%9E%E5%BC%B7%E8%BA%AB%E9%AB%94)  
階層 2: [界面直連](/docs/part1/Chapter9#interface-%E7%95%8C%E9%9D%A2%E7%9B%B4%E9%80%A3)  
階層 3: [包覆感應](/docs/part1/Chapter9#sensing-package-%E5%8C%85%E8%A6%86%E6%84%9F%E6%87%89) 或 [武器化](/docs/part1/Chapter9#weaponization-%E6%AD%A6%E5%99%A8%E5%8C%96)  
階層 4: [融合昇華](/docs/part1/Chapter9#fusion-%E8%9E%8D%E5%90%88%E6%98%87%E8%8F%AF)  
階層 5: [深度儲備](/docs/part1/Chapter9#deep-reserves-%E6%B7%B1%E5%BA%A6%E5%84%B2%E5%82%99)  
階層 6: [心靈湧動](/docs/part1/Chapter9#mind-surge-%E5%BF%83%E9%9D%88%E6%B9%A7%E5%8B%95) 或 [超級增強](/docs/part1/Chapter9#ultra-enhancement-%E8%B6%85%E7%B4%9A%E5%A2%9E%E5%BC%B7)

### FUSES MIND AND MACHINE 融合思想和機器
植入大腦的電子輔助設備使你有精神上的強大力量。

階層 1: [強化智力](/docs/part1/Chapter9#enhanced-intellect-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 1: [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD)  
階層 2: [網絡竊聽](/docs/part1/Chapter9#network-tap-%E7%B6%B2%E7%B5%A1%E7%AB%8A%E8%81%BD4-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [行動處理器](/docs/part1/Chapter9#action-processor-%E8%A1%8C%E5%8B%95%E8%99%95%E7%90%86%E5%99%A84-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [機器心靈感應](/docs/part1/Chapter9#machine-telepathy-%E6%A9%9F%E5%99%A8%E5%BF%83%E9%9D%88%E6%84%9F%E6%87%893-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 4: [知識技能](/docs/part1/Chapter9#knowledge-skills-%E7%9F%A5%E8%AD%98%E6%8A%80%E8%83%BD)  
階層 5: [看到未來](/docs/part1/Chapter9#see-the-future-%E7%9C%8B%E5%88%B0%E6%9C%AA%E4%BE%866-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [機器強化](/docs/part1/Chapter9#machine-enhancement-%E6%A9%9F%E5%99%A8%E5%BC%B7%E5%8C%96) 或 [心靈湧動](/docs/part1/Chapter9#mind-surge-%E5%BF%83%E9%9D%88%E6%B9%A7%E5%8B%95)

### GROWS TO TOWERING HEIGHTS 成長到高聳的高度
在短暫的時間內，你可以成長壯大，並擁有足夠的經驗，可以達到更高的高度。

階層 1: [變巨](/docs/part1/Chapter9#enlarge-%E8%AE%8A%E5%B7%A81-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 1: [大得離奇](/docs/part1/Chapter9#freakishly-large-%E5%A4%A7%E5%BE%97%E9%9B%A2%E5%A5%87)  
階層 2: [更大的](/docs/part1/Chapter9#bigger-%E6%9B%B4%E5%A4%A7%E7%9A%84)  
階層 2: [大塊頭的好處](/docs/part1/Chapter9#advantages-of-being-big-%E5%A4%A7%E5%A1%8A%E9%A0%AD%E7%9A%84%E5%A5%BD%E8%99%95)  
階層 3: [巨大的](/docs/part1/Chapter9#huge-%E5%B7%A8%E5%A4%A7%E7%9A%84) 或 [投擲手](/docs/part1/Chapter9#throw-%E6%8A%95%E6%93%B2%E6%89%8B2-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 4: [攫抓](/docs/part1/Chapter9#grab-%E6%94%AB%E6%8A%93)  
階層 5: [超巨型](/docs/part1/Chapter9#gargantuan-%E8%B6%85%E5%B7%A8%E5%9E%8B)  
階層 6: [碩大的](/docs/part1/Chapter9#colossal-%E7%A2%A9%E5%A4%A7%E7%9A%84) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### HELPS THEIR FRIENDS 幫助他們的朋友
你愛你的朋友，無論遇到任何困難，都可以幫助他們。

類型能力交換選項: [朋友的建議](/docs/part1/Chapter9#advice-from-a-friend-%E6%9C%8B%E5%8F%8B%E7%9A%84%E5%BB%BA%E8%AD%B01-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [友好幫助](/docs/part1/Chapter9#friendly-help-%E5%8F%8B%E5%A5%BD%E5%B9%AB%E5%8A%A9)  
階層 1: [勇敢](/docs/part1/Chapter9#courageous-%E5%8B%87%E6%95%A2)  
階層 2: [度過難關](/docs/part1/Chapter9#weather-the-vicissitudes-%E5%BA%A6%E9%81%8E%E9%9B%A3%E9%97%9C)  
階層 3: [夥伴系統](/docs/part1/Chapter9#buddy-system-%E5%A4%A5%E4%BC%B4%E7%B3%BB%E7%B5%B13-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [擔傷友人](/docs/part1/Chapter9#in-harms-way-%E6%93%94%E5%82%B7%E5%8F%8B%E4%BA%BA3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [強化體質](/docs/part1/Chapter9#enhanced-physique-%E5%BC%B7%E5%8C%96%E9%AB%94%E8%B3%AA)  
階層 5: [激發行動](/docs/part1/Chapter9#inspire-action-%E6%BF%80%E7%99%BC%E8%A1%8C%E5%8B%954-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [深思熟慮](/docs/part1/Chapter9#deep-consideration-%E6%B7%B1%E6%80%9D%E7%86%9F%E6%85%AE6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)


### HOWLS AT THE MOON 對月亮嚎叫
在短暫的時間裡，你會成為一個可怕而強大的生物，有控制問題。

階層 1: [野獸形態](/docs/part1/Chapter9#beast-form-%E9%87%8E%E7%8D%B8%E5%BD%A2%E6%85%8B)  
階層 2: [可控改變](/docs/part1/Chapter9#controlled-change-%E5%8F%AF%E6%8E%A7%E6%94%B9%E8%AE%8A)  
階層 3: [更大野獸形態](/docs/part1/Chapter9#bigger-beast-form-%E6%9B%B4%E5%A4%A7%E9%87%8E%E7%8D%B8%E5%BD%A2%E6%85%8B) 或 [高級野獸形態](/docs/part1/Chapter9#greater-beast-form-%E9%AB%98%E7%B4%9A%E9%87%8E%E7%8D%B8%E5%BD%A2%E6%85%8B)  
階層 4: [高級可控變化](/docs/part1/Chapter9#greater-controlled-change-%E9%AB%98%E7%B4%9A%E5%8F%AF%E6%8E%A7%E8%AE%8A%E5%8C%96)  
階層 5: [強化野獸形態](/docs/part1/Chapter9#enhanced-beast-form-%E5%BC%B7%E5%8C%96%E9%87%8E%E7%8D%B8%E5%BD%A2%E6%85%8B)  
階層 6: [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3) 或 [完美控制](/docs/part1/Chapter9#perfect-control-%E5%AE%8C%E7%BE%8E%E6%8E%A7%E5%88%B6)

### HUNTS 狩獵
你是個追擊獵手，擅長擊倒所選的目標。

階層 1: [華麗攻擊](/docs/part1/Chapter9#attack-flourish-%E8%8F%AF%E9%BA%97%E6%94%BB%E6%93%8A)  
階層 1: [追蹤者](/docs/part1/Chapter9#tracker-%E8%BF%BD%E8%B9%A4%E8%80%85)  
階層 2: [獵物](/docs/part1/Chapter9#quarry-%E7%8D%B5%E7%89%A92-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [潛匿](/docs/part1/Chapter9#sneak-%E6%BD%9B%E5%8C%BF)  
階層 3: [集團戰](/docs/part1/Chapter9#horde-fighting-%E9%9B%86%E5%9C%98%E6%88%B0) 或 [衝刺與抓取](/docs/part1/Chapter9#sprint-and-grab-%E8%A1%9D%E5%88%BA%E8%88%87%E6%8A%93%E5%8F%962-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 4: [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 5: [獵人驅力](/docs/part1/Chapter9#hunters-drive-%E7%8D%B5%E4%BA%BA%E9%A9%85%E5%8A%9B5-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [高級攻擊技能](/docs/part1/Chapter9#greater-skill-with-attacks-%E9%AB%98%E7%B4%9A%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD) 或 [多重獵物](/docs/part1/Chapter9#multiple-quarry-%E5%A4%9A%E9%87%8D%E7%8D%B5%E7%89%A96-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### INFILTRATES 滲透
機敏，狡猾和隱蔽性使你可以進入別人無法企及的地方。

階層 1: [潛行技能](/docs/part1/Chapter9#stealth-skills-%E6%BD%9B%E8%A1%8C%E6%8A%80%E8%83%BD)  
階層 1: [感知態度](/docs/part1/Chapter9#sense-attitudes-%E6%84%9F%E7%9F%A5%E6%85%8B%E5%BA%A6)  
階層 2: [冒充](/docs/part1/Chapter9#impersonate-%E5%86%92%E5%85%852-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [走為上策](/docs/part1/Chapter9#flight-not-fight-%E8%B5%B0%E7%82%BA%E4%B8%8A%E7%AD%96)  
階層 3: [意識](/docs/part1/Chapter9#awareness-%E6%84%8F%E8%AD%983-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [隱形](/docs/part1/Chapter9#invisibility-%E9%9A%B1%E5%BD%A24-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [逃避](/docs/part1/Chapter9#evasion-%E9%80%83%E9%81%BF)  
階層 6: [洗腦](/docs/part1/Chapter9#brainwashing-%E6%B4%97%E8%85%A66-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [彈躍](/docs/part1/Chapter9#spring-away-%E5%BD%88%E8%BA%8D5-%E9%BB%9E%E9%80%9F%E5%BA%A6)  

### INTERPRETS THE LAW 解釋法律
你擅長贏得他人的意見。

階層 1: [開幕詞](/docs/part1/Chapter9#opening-statement-%E9%96%8B%E5%B9%95%E8%A9%9E)  
階層 1: [法律知識](/docs/part1/Chapter9#knowledge-of-the-law-%E6%B3%95%E5%BE%8B%E7%9F%A5%E8%AD%98)  
階層 2: [辯論會](/docs/part1/Chapter9#debate-%E8%BE%AF%E8%AB%96%E6%9C%833-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [能夠協助](/docs/part1/Chapter9#able-assistance-%E8%83%BD%E5%A4%A0%E5%8D%94%E5%8A%A9) 或 [強化智力節省值](/docs/part1/Chapter9#enhanced-intellect-edge-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 4: [鞭笞](/docs/part1/Chapter9#castigate-%E9%9E%AD%E7%AC%9E4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [沒有人比你更清楚](/docs/part1/Chapter9#no-one-knows-better-%E6%B2%92%E6%9C%89%E4%BA%BA%E6%AF%94%E4%BD%A0%E6%9B%B4%E6%B8%85%E6%A5%9A)  
階層 6: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B) 或 [法律實習生](/docs/part1/Chapter9#legal-intern-%E6%B3%95%E5%BE%8B%E5%AF%A6%E7%BF%92%E7%94%9F)

### IS IDOLIZED BY MILLIONS 被百萬人崇拜
你是一個名人，大多數人崇拜你。

階層 1: [隨行人員](/docs/part1/Chapter9#entourage-%E9%9A%A8%E8%A1%8C%E4%BA%BA%E5%93%A1)  
階層 1: [名人才能](/docs/part1/Chapter9#celebrity-talent-%E5%90%8D%E4%BA%BA%E6%89%8D%E8%83%BD)  
階層 2: [明星特權](/docs/part1/Chapter9#perks-of-stardom-%E6%98%8E%E6%98%9F%E7%89%B9%E6%AC%8A)  
階層 3: [荒誕健康](/docs/part1/Chapter9#incredible-health-%E8%8D%92%E8%AA%95%E5%81%A5%E5%BA%B7) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [星光吸引](/docs/part1/Chapter9#captivate-with-starshine-%E6%98%9F%E5%85%89%E5%90%B8%E5%BC%95)  
階層 4: [專家追隨者](/docs/part1/Chapter9#expert-follower-%E5%B0%88%E5%AE%B6%E8%BF%BD%E9%9A%A8%E8%80%85)  
階層 5: [你知道我是誰嗎？](/docs/part1/Chapter9#do-you-know-who-i-am-%E4%BD%A0%E7%9F%A5%E9%81%93%E6%88%91%E6%98%AF%E8%AA%B0%E5%97%8E3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [超越劇本](/docs/part1/Chapter9#transcend-the-script-%E8%B6%85%E8%B6%8A%E5%8A%87%E6%9C%AC5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [改進同伴](/docs/part1/Chapter9#improved-companion-%E6%94%B9%E9%80%B2%E5%90%8C%E4%BC%B4)

### IS LICENSED TO CARRY 授權攜帶
你攜帶槍支，並且知道如何在戰鬥中使用它。
階層 1: [槍客](/docs/part1/Chapter9#gunner-%E6%A7%8D%E5%AE%A2)  
階層 1: [熟練槍械](/docs/part1/Chapter9#practiced-with-guns-%E7%86%9F%E7%B7%B4%E6%A7%8D%E6%A2%B0)  
階層 2: [謹慎射擊](/docs/part1/Chapter9#careful-shot-%E8%AC%B9%E6%85%8E%E5%B0%84%E6%93%8A)  
階層 3: [訓練有素的槍手](/docs/part1/Chapter9#trained-gunner-%E8%A8%93%E7%B7%B4%E6%9C%89%E7%B4%A0%E7%9A%84%E6%A7%8D%E6%89%8B) 或 [傷害製造者](/docs/part1/Chapter9#damage-dealer-%E5%82%B7%E5%AE%B3%E8%A3%BD%E9%80%A0%E8%80%85)  
階層 4: [亂射](/docs/part1/Chapter9#snap-shot-%E4%BA%82%E5%B0%84)  
階層 5: [弧形掃射](/docs/part1/Chapter9#arc-spray-%E5%BC%A7%E5%BD%A2%E6%8E%83%E5%B0%843-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 6: [特殊射擊](/docs/part1/Chapter9#special-shot-%E7%89%B9%E6%AE%8A%E5%B0%84%E6%93%8A) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### IS WANTED BY THE LAW 被法律通緝
「通緝犯，死活不論」的海報（或類似的海報）上印有你的頭像。 這是一個失控的錯誤，還是僅僅因為別人看著你，你就殺了他們，這取決於你自己。

階層 1: [強化速度](/docs/part1/Chapter9#enhanced-speed-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 1: [險境感知](/docs/part1/Chapter9#danger-sense-%E9%9A%AA%E5%A2%83%E6%84%9F%E7%9F%A51-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 2: [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 3: [法外聲譽](/docs/part1/Chapter9#outlaw-reputation-%E6%B3%95%E5%A4%96%E8%81%B2%E8%AD%BD3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [連續攻擊](/docs/part1/Chapter9#successive-attack-%E9%80%A3%E7%BA%8C%E6%94%BB%E6%93%8A2-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 4: [快速殺戮](/docs/part1/Chapter9#fast-kill-%E5%BF%AB%E9%80%9F%E6%AE%BA%E6%88%AE2-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 5: [亡命戰隊](/docs/part1/Chapter9#band-of-desperados-%E4%BA%A1%E5%91%BD%E6%88%B0%E9%9A%8A)  
階層 6: [尚未死亡](/docs/part1/Chapter9#not-dead-yet-%E5%B0%9A%E6%9C%AA%E6%AD%BB%E4%BA%A1) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)  

### KEEPS A MAGIC ALLY 擁有魔法盟友
綁定到一個物體上的魔法生物盟友（比如一個小精靈在燈裡，或者一個幽靈在煙斗裡）是你的朋友，保護者和武器。

階層 1: [綁定魔法生物](/docs/part1/Chapter9#bound-magic-creature-%E7%B6%81%E5%AE%9A%E9%AD%94%E6%B3%95%E7%94%9F%E7%89%A9)  
階層 2: [物體綁定](/docs/part1/Chapter9#object-bond-%E7%89%A9%E9%AB%94%E7%B6%81%E5%AE%9A3-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 2: [隱藏壁櫥](/docs/part1/Chapter9#hidden-closet-%E9%9A%B1%E8%97%8F%E5%A3%81%E6%AB%A5)  
階層 3: [小願望](/docs/part1/Chapter9#minor-wish-%E5%B0%8F%E9%A1%98%E6%9C%9B) 或 [坐騎](/docs/part1/Chapter9#mount-%E5%9D%90%E9%A8%8E)  
階層 4: [改進物體綁定](/docs/part1/Chapter9#improved-object-bond-%E6%94%B9%E9%80%B2%E7%89%A9%E9%AB%94%E7%B6%81%E5%AE%9A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [中等願望](/docs/part1/Chapter9#moderate-wish-%E4%B8%AD%E7%AD%89%E9%A1%98%E6%9C%9B)  
階層 6: [物體綁定大師](/docs/part1/Chapter9#object-bond-mastery-%E7%89%A9%E9%AB%94%E7%B6%81%E5%AE%9A%E5%A4%A7%E5%B8%AB7-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [相信運氣](/docs/part1/Chapter9#trust-to-luck-%E7%9B%B8%E4%BF%A1%E9%81%8B%E6%B0%A33-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### LEADS 領導
天生的領導能力使你可以指揮其他人，包括忠實的追隨者。

階層 1: [天生魅力](/docs/part1/Chapter9#natural-charisma-%E5%A4%A9%E7%94%9F%E9%AD%85%E5%8A%9B)  
階層 1: [好建議](/docs/part1/Chapter9#good-advice-%E5%A5%BD%E5%BB%BA%E8%AD%B0)  
階層 2: [強化潛力](/docs/part1/Chapter9#enhanced-potential-%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 2: [基礎追隨者](/docs/part1/Chapter9#basic-follower-%E5%9F%BA%E7%A4%8E%E8%BF%BD%E9%9A%A8%E8%80%85)  
階層 3: [高級命令](/docs/part1/Chapter9#advanced-command-%E9%AB%98%E7%B4%9A%E5%91%BD%E4%BB%A47-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [專家追隨者](/docs/part1/Chapter9#expert-follower-%E5%B0%88%E5%AE%B6%E8%BF%BD%E9%9A%A8%E8%80%85)  
階層 4: [吸引或激勵](/docs/part1/Chapter9#captivate-or-inspire-%E5%90%B8%E5%BC%95%E6%88%96%E6%BF%80%E5%8B%B5)  
階層 5: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 6: [追隨者戰隊](/docs/part1/Chapter9#band-of-followers-%E8%BF%BD%E9%9A%A8%E8%80%85%E6%88%B0%E9%9A%8A) 或 [領袖思維](/docs/part1/Chapter9#mind-of-a-leader-%E9%A0%98%E8%A2%96%E6%80%9D%E7%B6%AD6-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### LEARNS QUICKLY 快速學習
你可以應對情況的惡化，每次學習新的經驗教訓。

階層 1: [強化智力](/docs/part1/Chapter9#enhanced-intellect-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 1: [你有手段](/docs/part1/Chapter9#theres-your-problem-%E4%BD%A0%E6%9C%89%E6%89%8B%E6%AE%B5)  
階層 2: [快速學習](/docs/part1/Chapter9#quick-study-%E5%BF%AB%E9%80%9F%E5%AD%B8%E7%BF%92)  
階層 3: [難以轉移注意力](/docs/part1/Chapter9#hard-to-distract-%E9%9B%A3%E4%BB%A5%E8%BD%89%E7%A7%BB%E6%B3%A8%E6%84%8F%E5%8A%9B)  
階層 3: [強化智力節省值](/docs/part1/Chapter9#enhanced-intellect-edge-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B%E7%AF%80%E7%9C%81%E5%80%BC) 或 [靈活技能](/docs/part1/Chapter9#flex-skill-%E9%9D%88%E6%B4%BB%E6%8A%80%E8%83%BD)  
階層 4: [輔佐前行](/docs/part1/Chapter9#pay-it-forward-%E8%BC%94%E4%BD%90%E5%89%8D%E8%A1%8C3-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [強化智力節省值](/docs/part1/Chapter9#enhanced-intellect-edge-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 5: [學到一些東西](/docs/part1/Chapter9#learned-a-few-things-%E5%AD%B8%E5%88%B0%E4%B8%80%E4%BA%9B%E6%9D%B1%E8%A5%BF)  
階層 6: [同時做兩件事](/docs/part1/Chapter9#two-things-at-once-%E5%90%8C%E6%99%82%E5%81%9A%E5%85%A9%E4%BB%B6%E4%BA%8B6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)

### LIVES IN THE WILDERNESS 生活在荒野中
你可以在其他人滅亡的荒地中生存。

階層 1: [荒野生活](/docs/part1/Chapter9#wilderness-life-%E8%8D%92%E9%87%8E%E7%94%9F%E6%B4%BB)  
階層 1: [強化氣力](/docs/part1/Chapter9#enhanced-might-%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 2: [靠土地生活](/docs/part1/Chapter9#living-off-the-land-%E9%9D%A0%E5%9C%9F%E5%9C%B0%E7%94%9F%E6%B4%BB)  
階層 2: [荒野探險者](/docs/part1/Chapter9#wilderness-explorer-%E8%8D%92%E9%87%8E%E6%8E%A2%E9%9A%AA%E8%80%85)  
階層 3: [動物直覺](/docs/part1/Chapter9#animal-senses-and-sensibilities-%E5%8B%95%E7%89%A9%E7%9B%B4%E8%A6%BA) 或 [荒野鼓勵](/docs/part1/Chapter9#wilderness-encouragement-%E8%8D%92%E9%87%8E%E9%BC%93%E5%8B%B53-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [荒野意識](/docs/part1/Chapter9#wilderness-awareness-%E8%8D%92%E9%87%8E%E6%84%8F%E8%AD%984-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [荒野伴身邊](/docs/part1/Chapter9#the-wild-is-on-your-side-%E8%8D%92%E9%87%8E%E4%BC%B4%E8%BA%AB%E9%82%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [與荒野一體](/docs/part1/Chapter9#one-with-the-wild-%E8%88%87%E8%8D%92%E9%87%8E%E4%B8%80%E9%AB%946-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [野外迷彩](/docs/part1/Chapter9#wild-camouflage-%E9%87%8E%E5%A4%96%E8%BF%B7%E5%BD%A94-%E9%BB%9E%E6%99%BA%E5%8A%9B) 

### LOOKS FOR TROUBLE 尋找麻煩
你是一名愛打架的人，熱愛尋找戰鬥。

階層 1: [暴怒之拳](/docs/part1/Chapter9#fists-of-fury-%E6%9A%B4%E6%80%92%E4%B9%8B%E6%8B%B3)  
階層 1: [傷口護理師](/docs/part1/Chapter9#wound-tender-%E5%82%B7%E5%8F%A3%E8%AD%B7%E7%90%86%E5%B8%AB)  
階層 2: [保護者](/docs/part1/Chapter9#protector-%E4%BF%9D%E8%AD%B7%E8%80%85)  
階層 2: [直截了當](/docs/part1/Chapter9#straightforward-%E7%9B%B4%E6%88%AA%E4%BA%86%E7%95%B6)  
階層 3: [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD) 或 [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 4: [磕打](/docs/part1/Chapter9#knock-out-%E7%A3%95%E6%89%935-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 5: [精通攻擊](/docs/part1/Chapter9#mastery-with-attacks-%E7%B2%BE%E9%80%9A%E6%94%BB%E6%93%8A)  
階層 6: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### LOVES THE VOID 熱愛虛空
當只有你，你的宇航服，和永遠永遠輪出的星空全景時，你是平靜的。

類型能力交換選項: [有太空服，就會旅行](/docs/part1/Chapter9#have-spacesuit-will-travel-%E6%9C%89%E5%A4%AA%E7%A9%BA%E6%9C%8D%E5%B0%B1%E6%9C%83%E6%97%85%E8%A1%8C)  
階層 1: [真空技能](/docs/part1/Chapter9#vacuum-skilled-%E7%9C%9F%E7%A9%BA%E6%8A%80%E8%83%BD)  
階層 1: [微重力精通](/docs/part1/Chapter9#microgravity-adept-%E5%BE%AE%E9%87%8D%E5%8A%9B%E7%B2%BE%E9%80%9A)  
階層 2: [強化速度節省值](/docs/part1/Chapter9#enhanced-speed-edge-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 2: [強化體質](/docs/part1/Chapter9#enhanced-physique-%E5%BC%B7%E5%8C%96%E9%AB%94%E8%B3%AA)  
階層 3: [太空戰鬥](/docs/part1/Chapter9#space-fighting-%E5%A4%AA%E7%A9%BA%E6%88%B0%E9%AC%A5) 或 [融合裝甲](/docs/part1/Chapter9#fusion-armor-%E8%9E%8D%E5%90%88%E8%A3%9D%E7%94%B2)  
階層 4: [寂靜如太空](/docs/part1/Chapter9#silent-as-space-%E5%AF%82%E9%9D%9C%E5%A6%82%E5%A4%AA%E7%A9%BA)  
階層 4: [推離和投擲](/docs/part1/Chapter9#push-off-and-throw-%E6%8E%A8%E9%9B%A2%E5%92%8C%E6%8A%95%E6%93%B23-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 5: [微重力規避](/docs/part1/Chapter9#microgravity-avoidance-%E5%BE%AE%E9%87%8D%E5%8A%9B%E8%A6%8F%E9%81%BF)  
階層 6: [失重射擊](/docs/part1/Chapter9#weightless-shot-%E5%A4%B1%E9%87%8D%E5%B0%84%E6%93%8A) 或 [反應領域](/docs/part1/Chapter9#reactive-field-%E5%8F%8D%E6%87%89%E9%A0%98%E5%9F%9F)

### MASTERS DEFENSE 防禦大師
你使用防護裝備和實踐技巧來避免在戰鬥中受傷。

階層 1: [盾牌大師](/docs/part1/Chapter9#shield-master-%E7%9B%BE%E7%89%8C%E5%A4%A7%E5%B8%AB)  
階層 2: [身強體壯](/docs/part1/Chapter9#sturdy-%E8%BA%AB%E5%BC%B7%E9%AB%94%E5%A3%AF)  
階層 2: [著甲熟練](/docs/part1/Chapter9#practiced-in-armor-%E8%91%97%E7%94%B2%E7%86%9F%E7%B7%B4)  
階層 3: [閃避並抵抗](/docs/part1/Chapter9#dodge-and-resist-%E9%96%83%E9%81%BF%E4%B8%A6%E6%8A%B5%E6%8A%973-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [閃避與回擊](/docs/part1/Chapter9#dodge-and-respond-%E9%96%83%E9%81%BF%E8%88%87%E5%9B%9E%E6%93%8A3-%E6%B0%A3%E5%8A%9B%E9%BB%9E)  
階層 4: [意志之塔](/docs/part1/Chapter9#tower-of-will-%E6%84%8F%E5%BF%97%E4%B9%8B%E5%A1%94)  
階層 4: [著甲熟手](/docs/part1/Chapter9#experienced-in-armor-%E8%91%97%E7%94%B2%E7%86%9F%E6%89%8B)  
階層 5: [只顧防禦](/docs/part1/Chapter9#nothing-but-defend-%E5%8F%AA%E9%A1%A7%E9%98%B2%E7%A6%A6)  
階層 6: [防禦大師](/docs/part1/Chapter9#defense-master-%E9%98%B2%E7%A6%A6%E5%A4%A7%E5%B8%AB) 或 [穿好它](/docs/part1/Chapter9#wear-it-well-%E7%A9%BF%E5%A5%BD%E5%AE%83)

### MASTERS SPELLS 法術大師
通過專精法術施放和保存法術書，你可以快速施放電弧閃電，滾滾火焰，召喚陰影和召喚法術。

階層 1: [奧術耀斑](/docs/part1/Chapter9#arcane-flare-%E5%A5%A7%E8%A1%93%E8%80%80%E6%96%911-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [混亂射線](/docs/part1/Chapter9#ray-of-confusion-%E6%B7%B7%E4%BA%82%E5%B0%84%E7%B7%9A2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [火焰綻放](/docs/part1/Chapter9#fire-bloom-%E7%81%AB%E7%84%B0%E7%B6%BB%E6%94%BE4-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [召喚巨蜘蛛](/docs/part1/Chapter9#summon-giant-spider-%E5%8F%AC%E5%96%9A%E5%B7%A8%E8%9C%98%E8%9B%9B4-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [靈魂審訊](/docs/part1/Chapter9#soul-interrogation-%E9%9D%88%E9%AD%82%E5%AF%A9%E8%A8%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [花崗岩牆](/docs/part1/Chapter9#granite-wall-%E8%8A%B1%E5%B4%97%E5%B2%A9%E7%89%867-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [召喚惡魔](/docs/part1/Chapter9#summon-demon-%E5%8F%AC%E5%96%9A%E6%83%A1%E9%AD%947-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [死亡令詞](/docs/part1/Chapter9#word-of-death-%E6%AD%BB%E4%BA%A1%E4%BB%A4%E8%A9%9E5-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### MASTERS THE SWARM 掌握群體
昆蟲。 老鼠 蝙蝠。 甚至是鳥。 你掌握了一種服從你的小型生物。

階層 1: [影響集群](/docs/part1/Chapter9#influence-swarm-%E5%BD%B1%E9%9F%BF%E9%9B%86%E7%BE%A41-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [控制集群](/docs/part1/Chapter9#control-swarm-%E6%8E%A7%E5%88%B6%E9%9B%86%E7%BE%A42-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [活化盔甲](/docs/part1/Chapter9#living-armor-%E6%B4%BB%E5%8C%96%E7%9B%94%E7%94%B24-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [召喚集群](/docs/part1/Chapter9#call-swarm-%E5%8F%AC%E5%96%9A%E9%9B%86%E7%BE%A44-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [獲得不尋常的同伴](/docs/part1/Chapter9#gain-unusual-companion-%E7%8D%B2%E5%BE%97%E4%B8%8D%E5%B0%8B%E5%B8%B8%E7%9A%84%E5%90%8C%E4%BC%B4)  
階層 6: [致命蟲群](/docs/part1/Chapter9#deadly-swarm-%E8%87%B4%E5%91%BD%E8%9F%B2%E7%BE%A46-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)

### MASTERS WEAPONRY 武器大師
你是特定類型武器的專精使用者，無論是劍，鞭子，匕首，槍還是其他武器。

階層 1: [武器大師](/docs/part1/Chapter9#weapon-master-%E6%AD%A6%E5%99%A8%E5%A4%A7%E5%B8%AB)  
階層 1: [武器工匠](/docs/part1/Chapter9#weapon-crafter-%E6%AD%A6%E5%99%A8%E5%B7%A5%E5%8C%A0)  
階層 2: [武器防禦](/docs/part1/Chapter9#weapon-defense-%E6%AD%A6%E5%99%A8%E9%98%B2%E7%A6%A6)  
階層 3: [迅速攻擊](/docs/part1/Chapter9#rapid-attack-%E8%BF%85%E9%80%9F%E6%94%BB%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [繳械打擊](/docs/part1/Chapter9#disarming-strike-%E7%B9%B3%E6%A2%B0%E6%89%93%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 4: [永不出糗](/docs/part1/Chapter9#never-fumble-%E6%B0%B8%E4%B8%8D%E5%87%BA%E7%B3%97)  
階層 5: [極限掌握](/docs/part1/Chapter9#extreme-mastery-%E6%A5%B5%E9%99%90%E6%8E%8C%E6%8F%A16-%E9%BB%9E%E6%B0%A3%E5%8A%9B%E6%88%96%E9%80%9F%E5%BA%A6)  
階層 6: [謀殺者](/docs/part1/Chapter9#murderer-%E8%AC%80%E6%AE%BA%E8%80%858-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [致命打擊](/docs/part1/Chapter9#deadly-strike-%E8%87%B4%E5%91%BD%E6%89%93%E6%93%8A5-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### METES OUT JUSTICE 伸張正義
你糾正過錯，保護無辜者，並懲罰有罪行為。

階層 1: [做出判斷](/docs/part1/Chapter9#make-judgment-%E5%81%9A%E5%87%BA%E5%88%A4%E6%96%B7)  
階層 1: [定審](/docs/part1/Chapter9#designation-%E5%AE%9A%E5%AF%A9)  
階層 2: [保護無辜者](/docs/part1/Chapter9#defend-the-innocent-%E4%BF%9D%E8%AD%B7%E7%84%A1%E8%BE%9C%E8%80%852-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 2: [強化定審](/docs/part1/Chapter9#improved-designation-%E5%BC%B7%E5%8C%96%E5%AE%9A%E5%AF%A9)   
階層 3: [保護所有無辜者](/docs/part1/Chapter9#defend-all-the-innocent-%E4%BF%9D%E8%AD%B7%E6%89%80%E6%9C%89%E7%84%A1%E8%BE%9C%E8%80%85) 或 [懲罰罪人](/docs/part1/Chapter9#punish-the-guilty-%E6%87%B2%E7%BD%B0%E7%BD%AA%E4%BA%BA2-%E6%B0%A3%E5%8A%9B%E9%BB%9E)   
階層 4: [找到罪人](/docs/part1/Chapter9#find-the-guilty-%E6%89%BE%E5%88%B0%E7%BD%AA%E4%BA%BA)  
階層 4: [高級定審](/docs/part1/Chapter9#greater-designation-%E9%AB%98%E7%B4%9A%E5%AE%9A%E5%AF%A9)  
階層 5: [懲罰所有罪人](/docs/part1/Chapter9#punish-all-the-guilty-%E6%87%B2%E7%BD%B0%E6%89%80%E6%9C%89%E7%BD%AA%E4%BA%BA3-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 6: [詛咒罪人](/docs/part1/Chapter9#damn-the-guilty-%E8%A9%9B%E5%92%92%E7%BD%AA%E4%BA%BA3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [激勵無辜者](/docs/part1/Chapter9#inspire-the-innocent-%E6%BF%80%E5%8B%B5%E7%84%A1%E8%BE%9C%E8%80%853-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### MOVES LIKE A CAT 輕盈如貓
輕柔，靈活而優美的動作使你能夠快速，順暢地移動，似乎從來沒有出現危險。

階層 1: [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 1: [平衡感](/docs/part1/Chapter9#balance-%E5%B9%B3%E8%A1%A1%E6%84%9F)  
階層 2: [運動技能](/docs/part1/Chapter9#movement-skills-%E9%81%8B%E5%8B%95%E6%8A%80%E8%83%BD)  
階層 2: [安全掉落](/docs/part1/Chapter9#safe-fall-%E5%AE%89%E5%85%A8%E6%8E%89%E8%90%BD)  
階層 3: [難以擊中](/docs/part1/Chapter9#hard-to-hit-%E9%9B%A3%E4%BB%A5%E6%93%8A%E4%B8%AD)  
階層 3: [強化速度節省值](/docs/part1/Chapter9#enhanced-speed-edge-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6%E7%AF%80%E7%9C%81%E5%80%BC) 或 [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 4: [快速打擊](/docs/part1/Chapter9#quick-strike-%E5%BF%AB%E9%80%9F%E6%89%93%E6%93%8A4-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 5: [滑溜身手](/docs/part1/Chapter9#slippery-%E6%BB%91%E6%BA%9C%E8%BA%AB%E6%89%8B)  
階層 6: [完美速度爆發](/docs/part1/Chapter9#perfect-speed-burst-%E5%AE%8C%E7%BE%8E%E9%80%9F%E5%BA%A6%E7%88%86%E7%99%BC6-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)

### MOVES LIKE THE WIND 像風一樣
你可以移動得如此之快，以至於變得模糊。

階層 1: [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 1: [飄忽步伐](/docs/part1/Chapter9#fleet-of-foot-%E9%A3%84%E5%BF%BD%E6%AD%A5%E4%BC%901-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 2: [難以擊中](/docs/part1/Chapter9#hard-to-hit-%E9%9B%A3%E4%BB%A5%E6%93%8A%E4%B8%AD)  
階層 3: [速度爆發](/docs/part1/Chapter9#speed-burst-%E9%80%9F%E5%BA%A6%E7%88%86%E7%99%BC4-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 4: [眨眼瞬間](/docs/part1/Chapter9#blink-of-an-eye-%E7%9C%A8%E7%9C%BC%E7%9E%AC%E9%96%934-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 5: [難以看見](/docs/part1/Chapter9#hard-to-see-%E9%9B%A3%E4%BB%A5%E7%9C%8B%E8%A6%8B)  
階層 6: [完美速度爆發](/docs/part1/Chapter9#perfect-speed-burst-%E5%AE%8C%E7%BE%8E%E9%80%9F%E5%BA%A6%E7%88%86%E7%99%BC6-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [不可思議的奔跑速度](/docs/part1/Chapter9#incredible-running-speed-%E4%B8%8D%E5%8F%AF%E6%80%9D%E8%AD%B0%E7%9A%84%E5%A5%94%E8%B7%91%E9%80%9F%E5%BA%A6)

### MURDERS 謀殺犯
你是一個刺客，無論是通過貿易，出於偏見，還是因為那件事或自己殺的。

階層 1: [突襲攻擊](/docs/part1/Chapter9#surprise-attack-%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 1: [刺客技能](/docs/part1/Chapter9#assassin-skills-%E5%88%BA%E5%AE%A2%E6%8A%80%E8%83%BD)  
階層 2: [快速死亡](/docs/part1/Chapter9#quick-death-%E5%BF%AB%E9%80%9F%E6%AD%BB%E4%BA%A12-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 2: [滲透者](/docs/part1/Chapter9#infiltrator-%E6%BB%B2%E9%80%8F%E8%80%85)  
階層 3: [意識](/docs/part1/Chapter9#awareness-%E6%84%8F%E8%AD%983-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [毒藥製造者](/docs/part1/Chapter9#poison-crafter-%E6%AF%92%E8%97%A5%E8%A3%BD%E9%80%A0%E8%80%85)  
階層 4: [進階突襲攻擊](/docs/part1/Chapter9#better-surprise-attack-%E9%80%B2%E9%9A%8E%E7%AA%81%E8%A5%B2%E6%94%BB%E6%93%8A)  
階層 5: [傷害製造者](/docs/part1/Chapter9#damage-dealer-%E5%82%B7%E5%AE%B3%E8%A3%BD%E9%80%A0%E8%80%85)  
階層 6: [逃跑計劃](/docs/part1/Chapter9#escape-plan-%E9%80%83%E8%B7%91%E8%A8%88%E5%8A%83) 或 [謀殺者](/docs/part1/Chapter9#murderer-%E8%AC%80%E6%AE%BA%E8%80%858-%E9%BB%9E%E9%80%9F%E5%BA%A6)

### NEEDS NO WEAPON 無需武器
強大的拳打，腳踢，肘部，膝蓋和全身運動都是你所需要的武器。

階層 1: [暴怒之拳](/docs/part1/Chapter9#fists-of-fury-%E6%9A%B4%E6%80%92%E4%B9%8B%E6%8B%B3)  
階層 1: [堅石血肉](/docs/part1/Chapter9#flesh-of-stone-%E5%A0%85%E7%9F%B3%E8%A1%80%E8%82%89)  
階層 2: [優勢對劣勢](/docs/part1/Chapter9#advantage-to-disadvantage-%E5%84%AA%E5%8B%A2%E5%B0%8D%E5%8A%A3%E5%8B%A23-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 2: [徒手格鬥風格大師](/docs/part1/Chapter9#master-of-unarmed-fighting-style-%E5%BE%92%E6%89%8B%E6%A0%BC%E9%AC%A5%E9%A2%A8%E6%A0%BC%E5%A4%A7%E5%B8%AB)  
階層 3: [動如流水](/docs/part1/Chapter9#moving-like-water-%E5%8B%95%E5%A6%82%E6%B5%81%E6%B0%B43-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 4: [轉移攻擊](/docs/part1/Chapter9#divert-attacks-%E8%BD%89%E7%A7%BB%E6%94%BB%E6%93%8A4-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 5: [震懾攻擊](/docs/part1/Chapter9#stun-attack-%E9%9C%87%E6%87%BE%E6%94%BB%E6%93%8A6-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 6: [徒手格鬥風格大師](/docs/part1/Chapter9#master-of-unarmed-fighting-style-%E5%BE%92%E6%89%8B%E6%A0%BC%E9%AC%A5%E9%A2%A8%E6%A0%BC%E5%A4%A7%E5%B8%AB) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### NEVER SAYS DIE 從不言敗
你從不放棄，可以擺脫毆打，並且總是回來獲得更多。

階層 1: [改善恢復](/docs/part1/Chapter9#improved-recovery-%E6%94%B9%E5%96%84%E6%81%A2%E5%BE%A9)  
階層 1: [向前推進](/docs/part1/Chapter9#push-on-through-%E5%90%91%E5%89%8D%E6%8E%A8%E9%80%B22-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 2: [忽略痛苦](/docs/part1/Chapter9#ignore-the-pain-%E5%BF%BD%E7%95%A5%E7%97%9B%E8%8B%A6)  
階層 3: [血液熱潮](/docs/part1/Chapter9#blood-fever-%E8%A1%80%E6%B6%B2%E7%86%B1%E6%BD%AE) 或 [隱藏儲備](/docs/part1/Chapter9#hidden-reserves-%E9%9A%B1%E8%97%8F%E5%84%B2%E5%82%99)  
階層 4: [增加決心](/docs/part1/Chapter9#increasing-determination-%E5%A2%9E%E5%8A%A0%E6%B1%BA%E5%BF%83) 或 [超越敵人](/docs/part1/Chapter9#outlast-the-foe-%E8%B6%85%E8%B6%8A%E6%95%B5%E4%BA%BA)  
階層 5: [尚未死亡](/docs/part1/Chapter9#not-dead-yet-%E5%B0%9A%E6%9C%AA%E6%AD%BB%E4%BA%A1)  
階層 6: [最後反抗](/docs/part1/Chapter9#final-defiance-%E6%9C%80%E5%BE%8C%E5%8F%8D%E6%8A%97) 或 [無視苦痛](/docs/part1/Chapter9#ignore-affliction-%E7%84%A1%E8%A6%96%E8%8B%A6%E7%97%9B5-%E6%B0%A3%E5%8A%9B%E9%BB%9E)  

### OPERATES UNDERCOVER 秘密行動
打著別人的幌子，你要尋找強者不想洩露的答案。

階層 1: [調查學](/docs/part1/Chapter9#investigate-%E8%AA%BF%E6%9F%A5%E5%AD%B8)  
階層 2: [易容](/docs/part1/Chapter9#disguise-%E6%98%93%E5%AE%B9)  
階層 3: [挑釁者特工](/docs/part1/Chapter9#agent-provocateur-%E6%8C%91%E9%87%81%E8%80%85%E7%89%B9%E5%B7%A5) 或 [打帶跑](/docs/part1/Chapter9#run-and-fight-%E6%89%93%E5%B8%B6%E8%B7%914-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 4: [欺騙一波](/docs/part1/Chapter9#pull-a-fast-one-%E6%AC%BA%E9%A8%99%E4%B8%80%E6%B3%A23-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [利用現有的東西](/docs/part1/Chapter9#using-whats-available-%E5%88%A9%E7%94%A8%E7%8F%BE%E6%9C%89%E7%9A%84%E6%9D%B1%E8%A5%BF4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [相信運氣](/docs/part1/Chapter9#trust-to-luck-%E7%9B%B8%E4%BF%A1%E9%81%8B%E6%B0%A33-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [致命打擊](/docs/part1/Chapter9#deadly-strike-%E8%87%B4%E5%91%BD%E6%89%93%E6%93%8A5-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### PERFORMS FEATS OF STRENGTH 展現力量壯舉
肌肉發達的神童，你可以舉起不可思議的體重，向空中投擲自己的身體，並衝過門。

階層 1: [運動員](/docs/part1/Chapter9#athlete-%E9%81%8B%E5%8B%95%E5%93%A1)  
階層 1: [強化氣力節省值](/docs/part1/Chapter9#enhanced-might-edge-%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 2: [力量壯舉](/docs/part1/Chapter9#feat-of-strength-%E5%8A%9B%E9%87%8F%E5%A3%AF%E8%88%891-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 3: [鐵拳](/docs/part1/Chapter9#iron-fist-%E9%90%B5%E6%8B%B3) 或 [投擲手](/docs/part1/Chapter9#throw-%E6%8A%95%E6%93%B2%E6%89%8B2-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 4: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 5: [蠻力打擊](/docs/part1/Chapter9#brute-strike-%E8%A0%BB%E5%8A%9B%E6%89%93%E6%93%8A4-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 6: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B) 或 [跳躍攻擊](/docs/part1/Chapter9#jump-attack-%E8%B7%B3%E8%BA%8D%E6%94%BB%E6%93%8A5-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### PILOTS STARCRAFT 駕駛星艦
你是個優秀的星際飛船駕駛員。

階層 1: [駕駛星艦](/docs/part1/Chapter9#pilot-%E9%A7%95%E9%A7%9B%E6%98%9F%E8%89%A6)  
階層 1: [靈活知識](/docs/part1/Chapter9#flex-lore-%E9%9D%88%E6%B4%BB%E7%9F%A5%E8%AD%98)  
階層 2: [打撈和舒適](/docs/part1/Chapter9#salvage-and-comfort-%E6%89%93%E6%92%88%E5%92%8C%E8%88%92%E9%81%A92-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [強韌的精神](/docs/part1/Chapter9#mentally-tough-%E5%BC%B7%E9%9F%8C%E7%9A%84%E7%B2%BE%E7%A5%9E)  
階層 3: [駕駛專家](/docs/part1/Chapter9#expert-pilot-%E9%A7%95%E9%A7%9B%E5%B0%88%E5%AE%B6)  
階層 3: [艦船基礎](/docs/part1/Chapter9#ship-footing-%E8%89%A6%E8%88%B9%E5%9F%BA%E7%A4%8E3-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [機器夥伴](/docs/part1/Chapter9#machine-companion-%E6%A9%9F%E5%99%A8%E5%A4%A5%E4%BC%B4)  
階層 4: [感應陣列](/docs/part1/Chapter9#sensor-array-%E6%84%9F%E6%87%89%E9%99%A3%E5%88%973-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [強化速度](/docs/part1/Chapter9#enhanced-speed-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 5: [易如反掌](/docs/part1/Chapter9#like-the-back-of-your-hand-%E6%98%93%E5%A6%82%E5%8F%8D%E6%8E%8C)  
階層 6: [無與倫比的領航員](/docs/part1/Chapter9#incomparable-pilot-%E7%84%A1%E8%88%87%E5%80%AB%E6%AF%94%E7%9A%84%E9%A0%98%E8%88%AA%E5%93%A1)  
階層 6: [遠控攻擊](/docs/part1/Chapter9#remote-control-%E9%81%A0%E6%8E%A7%E6%94%BB%E6%93%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)

### PLAYS TOO MANY GAMES 玩太多遊戲
你通過玩太多遊戲學到的經驗、反應和策略在現實世界中也有應用，在現實世界中，人們玩得不夠辛苦，過著枯燥的生活。

階層 1: [遊戲課](/docs/part1/Chapter9#game-lessons-%E9%81%8A%E6%88%B2%E8%AA%B2)  
階層 1: [遊戲玩家](/docs/part1/Chapter9#gamer-%E9%81%8A%E6%88%B2%E7%8E%A9%E5%AE%B6)  
階層 2: [視暗眼眸](/docs/part1/Chapter9#zero-dark-eyes-%E8%A6%96%E6%9A%97%E7%9C%BC%E7%9C%B8)  
階層 2: [抵抗技巧](/docs/part1/Chapter9#resist-tricks-%E6%8A%B5%E6%8A%97%E6%8A%80%E5%B7%A7)  
階層 3: [狙擊瞄準](/docs/part1/Chapter9#snipers-aim-%E7%8B%99%E6%93%8A%E7%9E%84%E6%BA%96) 或 [強化速度節省值](/docs/part1/Chapter9#enhanced-speed-edge-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6%E7%AF%80%E7%9C%81%E5%80%BC)  
階層 4: [心理遊戲](/docs/part1/Chapter9#mind-games-%E5%BF%83%E7%90%86%E9%81%8A%E6%88%B23-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [強化智力](/docs/part1/Chapter9#enhanced-intellect-%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 5: [遊戲玩家的毅力](/docs/part1/Chapter9#gamers-fortitude-%E9%81%8A%E6%88%B2%E7%8E%A9%E5%AE%B6%E7%9A%84%E6%AF%85%E5%8A%9B)  
階層 6: [心靈湧動](/docs/part1/Chapter9#mind-surge-%E5%BF%83%E9%9D%88%E6%B9%A7%E5%8B%95) 或 [遊戲之神](/docs/part1/Chapter9#gaming-god-%E9%81%8A%E6%88%B2%E4%B9%8B%E7%A5%9E)

### RAGES 陷入狂怒
當你發狂的時候，每個人都害怕你。

階層 1: [狂亂](/docs/part1/Chapter9#frenzy-%E7%8B%82%E4%BA%821-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 2: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 2: [運動技能](/docs/part1/Chapter9#movement-skills-%E9%81%8B%E5%8B%95%E6%8A%80%E8%83%BD)  
階層 3: [力量打擊](/docs/part1/Chapter9#power-strike-%E5%8A%9B%E9%87%8F%E6%89%93%E6%93%8A3-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [無甲戰士](/docs/part1/Chapter9#unarmored-fighter-%E7%84%A1%E7%94%B2%E6%88%B0%E5%A3%AB)  
階層 4: [高級狂亂](/docs/part1/Chapter9#greater-frenzy-%E9%AB%98%E7%B4%9A%E7%8B%82%E4%BA%824-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [接續攻擊](/docs/part1/Chapter9#attack-and-attack-again-%E6%8E%A5%E7%BA%8C%E6%94%BB%E6%93%8A)  
階層 6: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B) 或 [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3)

### RIDES THE LIGHTNING 乘御閃電
你創造和釋放電能。

階層 1: [電擊](/docs/part1/Chapter9#shock-%E9%9B%BB%E6%93%8A1-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [注能法](/docs/part1/Chapter9#charge-%E6%B3%A8%E8%83%BD%E6%B3%951-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [騎乘閃矢](/docs/part1/Chapter9#bolt-rider-%E9%A8%8E%E4%B9%98%E9%96%83%E7%9F%A24-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [電能護甲](/docs/part1/Chapter9#electric-armor-%E9%9B%BB%E8%83%BD%E8%AD%B7%E7%94%B24-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [抽取充能](/docs/part1/Chapter9#drain-charge-%E6%8A%BD%E5%8F%96%E5%85%85%E8%83%BD)  
階層 4: [放射散矢](/docs/part1/Chapter9#bolts-of-power-%E6%94%BE%E5%B0%84%E6%95%A3%E7%9F%A25-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [電氣飛行](/docs/part1/Chapter9#electrical-flight-%E9%9B%BB%E6%B0%A3%E9%A3%9B%E8%A1%8C5-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [閃光萬里](/docs/part1/Chapter9#flash-across-the-miles-%E9%96%83%E5%85%89%E8%90%AC%E9%87%8C6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [閃電之牆](/docs/part1/Chapter9#wall-of-lightning-%E9%96%83%E9%9B%BB%E4%B9%8B%E7%89%866-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### RUNS AWAY 逃走
你的第一反應是逃離危險，而且你已經很擅長這一點了。

階層 1: [進入守勢](/docs/part1/Chapter9#go-defensive-%E9%80%B2%E5%85%A5%E5%AE%88%E5%8B%A21-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [強化速度](/docs/part1/Chapter9#enhanced-speed-%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 2: [迅速逃離](/docs/part1/Chapter9#quick-to-flee-%E8%BF%85%E9%80%9F%E9%80%83%E9%9B%A2)  
階層 3: [不可思議的奔跑速度](/docs/part1/Chapter9#incredible-running-speed-%E4%B8%8D%E5%8F%AF%E6%80%9D%E8%AD%B0%E7%9A%84%E5%A5%94%E8%B7%91%E9%80%9F%E5%BA%A6) 或 [高級強化速度](/docs/part1/Chapter9#greater-enhanced-speed-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E9%80%9F%E5%BA%A6)  
階層 4: [增加決心](/docs/part1/Chapter9#increasing-determination-%E5%A2%9E%E5%8A%A0%E6%B1%BA%E5%BF%83)  
階層 4: [快速機智](/docs/part1/Chapter9#quick-wits-%E5%BF%AB%E9%80%9F%E6%A9%9F%E6%99%BA)  
階層 5: [退隱](/docs/part1/Chapter9#go-to-ground-%E9%80%80%E9%9A%B14-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 6: [爆發逃跑](/docs/part1/Chapter9#burst-of-escape-%E7%88%86%E7%99%BC%E9%80%83%E8%B7%915-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)

### SAILED BENEATH THE JOLLY ROGER 在骷髏旗下航行
你和一群可怕的海盜一起航行，但是你決定結束你的海盜生涯，加入一些其他的事業。問題是，你的過去會這麼輕易地讓你離開嗎?

階層 1: [堅韌如釘](/docs/part1/Chapter9#tough-as-nails-%E5%A0%85%E9%9F%8C%E5%A6%82%E9%87%98)  
階層 1: [水手](/docs/part1/Chapter9#sailor-%E6%B0%B4%E6%89%8B)  
階層 2: [落井下石](/docs/part1/Chapter9#taking-advantage-%E8%90%BD%E4%BA%95%E4%B8%8B%E7%9F%B3)  
階層 2: [可怕名聲](/docs/part1/Chapter9#fearsome-reputation-%E5%8F%AF%E6%80%95%E5%90%8D%E8%81%B23-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 4: [水手腿](/docs/part1/Chapter9#sea-legs-%E6%B0%B4%E6%89%8B%E8%85%BF)  
階層 4: [運動技能](/docs/part1/Chapter9#movement-skills-%E9%81%8B%E5%8B%95%E6%8A%80%E8%83%BD)  
階層 5: [迷失混亂中](/docs/part1/Chapter9#lost-in-the-chaos-%E8%BF%B7%E5%A4%B1%E6%B7%B7%E4%BA%82%E4%B8%AD)  
階層 6: [決鬥至死](/docs/part1/Chapter9#duel-to-the-death-%E6%B1%BA%E9%AC%A5%E8%87%B3%E6%AD%BB5-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [連續攻擊](/docs/part1/Chapter9#successive-attack-%E9%80%A3%E7%BA%8C%E6%94%BB%E6%93%8A2-%E9%BB%9E%E9%80%9F%E5%BA%A6)  

### SCAVENGES 打撈
當你不需要逃跑和躲藏時，你在文明的廢墟中篩選有用的殘餘，以確保生存。

階層 1: [末世生存者](/docs/part1/Chapter9#post-apocalyptic-survivor-%E6%9C%AB%E4%B8%96%E7%94%9F%E5%AD%98%E8%80%85)  
階層 1: [廢墟知識](/docs/part1/Chapter9#ruin-lore-%E5%BB%A2%E5%A2%9F%E7%9F%A5%E8%AD%98)  
階層 2: [垃圾工法](/docs/part1/Chapter9#junkmonger-%E5%9E%83%E5%9C%BE%E5%B7%A5%E6%B3%952-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [落井下石](/docs/part1/Chapter9#taking-advantage-%E8%90%BD%E4%BA%95%E4%B8%8B%E7%9F%B3) 或 [荒誕健康](/docs/part1/Chapter9#incredible-health-%E8%8D%92%E8%AA%95%E5%81%A5%E5%BA%B7)  
階層 4: [知道去哪裡找](/docs/part1/Chapter9#know-where-to-look-%E7%9F%A5%E9%81%93%E5%8E%BB%E5%93%AA%E8%A3%A1%E6%89%BE)  
階層 5: [回收秘具](/docs/part1/Chapter9#recycled-cyphers-%E5%9B%9E%E6%94%B6%E7%A7%98%E5%85%B7)  
階層 6: [神器打撈者](/docs/part1/Chapter9#artifact-scavenger-%E7%A5%9E%E5%99%A8%E6%89%93%E6%92%88%E8%80%856-%E9%BB%9E%E6%99%BA%E5%8A%9B--2-xp) 或 [反應領域](/docs/part1/Chapter9#reactive-field-%E5%8F%8D%E6%87%89%E9%A0%98%E5%9F%9F)  

### SEES BEYOND 天眼通
你有一種靈感，能讓你看到別人看不見的東西。

階層 1: [看不見的東西](/docs/part1/Chapter9#see-the-unseen-%E7%9C%8B%E4%B8%8D%E8%A6%8B%E7%9A%84%E6%9D%B1%E8%A5%BF)  
階層 2: [看透物質](/docs/part1/Chapter9#see-through-matter-%E7%9C%8B%E9%80%8F%E7%89%A9%E8%B3%AA3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [尋找隱藏](/docs/part1/Chapter9#find-the-hidden-%E5%B0%8B%E6%89%BE%E9%9A%B1%E8%97%8F4-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [感測器](/docs/part1/Chapter9#sensor-%E6%84%9F%E6%B8%AC%E5%99%A84-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [遠景視界](/docs/part1/Chapter9#remote-viewing-%E9%81%A0%E6%99%AF%E8%A6%96%E7%95%8C6-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [看穿時間](/docs/part1/Chapter9#see-through-time-%E7%9C%8B%E7%A9%BF%E6%99%82%E9%96%937-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [精神投射](/docs/part1/Chapter9#mental-projection-%E7%B2%BE%E7%A5%9E%E6%8A%95%E5%B0%846-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [完全意識](/docs/part1/Chapter9#total-awareness-%E5%AE%8C%E5%85%A8%E6%84%8F%E8%AD%98)

### SEPARATES MIND FROM BODY 使身心分離
你可以將你的思想投射出你的身體，去看遠處的地方，並學習原本會隱藏的秘密。

階層 1: [第三隻眼](/docs/part1/Chapter9#third-eye-%E7%AC%AC%E4%B8%89%E9%9A%BB%E7%9C%BC1-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [打開思想](/docs/part1/Chapter9#open-mind-%E6%89%93%E9%96%8B%E6%80%9D%E6%83%B33-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [敏銳感官](/docs/part1/Chapter9#sharp-senses-%E6%95%8F%E9%8A%B3%E6%84%9F%E5%AE%98)  
階層 3: [第三眼漫遊](/docs/part1/Chapter9#roaming-third-eye-%E7%AC%AC%E4%B8%89%E7%9C%BC%E6%BC%AB%E9%81%8A3-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [尋找隱藏](/docs/part1/Chapter9#find-the-hidden-%E5%B0%8B%E6%89%BE%E9%9A%B1%E8%97%8F4-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [感測器](/docs/part1/Chapter9#sensor-%E6%84%9F%E6%B8%AC%E5%99%A84-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [通靈乘客](/docs/part1/Chapter9#psychic-passenger-%E9%80%9A%E9%9D%88%E4%B9%98%E5%AE%A26-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [精神投射](/docs/part1/Chapter9#mental-projection-%E7%B2%BE%E7%A5%9E%E6%8A%95%E5%B0%846-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [改進感測器](/docs/part1/Chapter9#improved-sensor-%E6%94%B9%E9%80%B2%E6%84%9F%E6%B8%AC%E5%99%A82-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### SHEPHERDS THE COMMUNITY 社區牧羊人
你要保護你居住的地方不受任何危險。

階層 1: [社區知識](/docs/part1/Chapter9#community-knowledge-%E7%A4%BE%E5%8D%80%E7%9F%A5%E8%AD%982-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [社區積極分子](/docs/part1/Chapter9#community-activist-%E7%A4%BE%E5%8D%80%E7%A9%8D%E6%A5%B5%E5%88%86%E5%AD%90)  
階層 2: [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 3: [牧羊人之怒](/docs/part1/Chapter9#shepherds-fury-%E7%89%A7%E7%BE%8A%E4%BA%BA%E4%B9%8B%E6%80%92) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 4: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)  
階層 5: [逃避](/docs/part1/Chapter9#evasion-%E9%80%83%E9%81%BF)  
階層 6: [高級攻擊技能](/docs/part1/Chapter9#greater-skill-with-attacks-%E9%AB%98%E7%B4%9A%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD) 或 [保護牆](/docs/part1/Chapter9#protective-wall-%E4%BF%9D%E8%AD%B7%E7%89%866-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### SHEPHERDS SPIRITS 靈魂牧者
流浪的靈魂，大自然的精神和元素的存在可以幫助和支持你。

階層 1: [詢問靈魂](/docs/part1/Chapter9#question-the-spirits-%E8%A9%A2%E5%95%8F%E9%9D%88%E9%AD%822-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [靈魂同謀](/docs/part1/Chapter9#spirit-accomplice-%E9%9D%88%E9%AD%82%E5%90%8C%E8%AC%80)  
階層 3: [改進命令精魂](/docs/part1/Chapter9#improved-command-spirit-%E6%94%B9%E9%80%B2%E5%91%BD%E4%BB%A4%E7%B2%BE%E9%AD%82) 或 [超自然感覺](/docs/part1/Chapter9#preternatural-senses-%E8%B6%85%E8%87%AA%E7%84%B6%E6%84%9F%E8%A6%BA)  
階層 4: [怨靈斗篷](/docs/part1/Chapter9#wraith-cloak-%E6%80%A8%E9%9D%88%E6%96%97%E7%AF%B7)  
階層 5: [召喚死靈](/docs/part1/Chapter9#call-dead-spirit-%E5%8F%AC%E5%96%9A%E6%AD%BB%E9%9D%886-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [召喚異界之靈](/docs/part1/Chapter9#call-otherworldly-spirit-%E5%8F%AC%E5%96%9A%E7%95%B0%E7%95%8C%E4%B9%8B%E9%9D%886-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [灌注靈魂](/docs/part1/Chapter9#infuse-spirit-%E7%81%8C%E6%B3%A8%E9%9D%88%E9%AD%82)

### SHREDS THE WALLS OF THE WORLD 撕碎世界之壁
速度加上相位使你具有獨特的躲避危險並同時造成損害的能力。

階層 1: [相位衝刺](/docs/part1/Chapter9#phase-sprint-%E7%9B%B8%E4%BD%8D%E8%A1%9D%E5%88%BA1-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 1: [破壞觸碰](/docs/part1/Chapter9#disrupting-touch-%E7%A0%B4%E5%A3%9E%E8%A7%B8%E7%A2%B01-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 2: [划痕存在](/docs/part1/Chapter9#scratch-existence-%E5%88%92%E7%97%95%E5%AD%98%E5%9C%A81-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 3: [隱形相位](/docs/part1/Chapter9#invisible-phasing-%E9%9A%B1%E5%BD%A2%E7%9B%B8%E4%BD%8D4-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [穿越牆壁](/docs/part1/Chapter9#walk-through-walls-%E7%A9%BF%E8%B6%8A%E7%89%86%E5%A3%812-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [相位引爆](/docs/part1/Chapter9#phase-detonation-%E7%9B%B8%E4%BD%8D%E5%BC%95%E7%88%862-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 5: [極遠距衝刺](/docs/part1/Chapter9#very-long-sprinting-%E6%A5%B5%E9%81%A0%E8%B7%9D%E8%A1%9D%E5%88%BA)  
階層 6: [碎裂存在](/docs/part1/Chapter9#shred-existence-%E7%A2%8E%E8%A3%82%E5%AD%98%E5%9C%A8) 或 [移動時無法觸及](/docs/part1/Chapter9#untouchable-while-moving-%E7%A7%BB%E5%8B%95%E6%99%82%E7%84%A1%E6%B3%95%E8%A7%B8%E5%8F%8A4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  

### SIPHONS POWER 虹吸力量
你從機器和生物中汲取能量以增強自己的能力。

階層 1: [吸取機器](/docs/part1/Chapter9#drain-machine-%E5%90%B8%E5%8F%96%E6%A9%9F%E5%99%A83-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [吸取生物](/docs/part1/Chapter9#drain-creature-%E5%90%B8%E5%8F%96%E7%94%9F%E7%89%A93-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [短距離吸取](/docs/part1/Chapter9#drain-at-a-distance-%E7%9F%AD%E8%B7%9D%E9%9B%A2%E5%90%B8%E5%8F%96) 或 [分解消耗](/docs/part1/Chapter9#unraveling-consumption-%E5%88%86%E8%A7%A3%E6%B6%88%E8%80%97)  
階層 4: [儲存能量](/docs/part1/Chapter9#store-energy-%E5%84%B2%E5%AD%98%E8%83%BD%E9%87%8F)  
階層 5: [分享力量](/docs/part1/Chapter9#share-the-power-%E5%88%86%E4%BA%AB%E5%8A%9B%E9%87%8F)  
階層 6: [爆炸性釋放](/docs/part1/Chapter9#explosive-release-%E7%88%86%E7%82%B8%E6%80%A7%E9%87%8B%E6%94%BE6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [太陽虹吸](/docs/part1/Chapter9#sun-siphon-%E5%A4%AA%E9%99%BD%E8%99%B9%E5%90%B8)  

### SLAYS MONSTERS 殺戮怪物
你殺怪物。

階層 1: [熟練劍類](/docs/part1/Chapter9#practiced-with-swords-%E7%86%9F%E7%B7%B4%E5%8A%8D%E9%A1%9E)  
階層 1: [怪物災禍](/docs/part1/Chapter9#monster-bane-%E6%80%AA%E7%89%A9%E7%81%BD%E7%A6%8D)  
階層 1: [怪物知識](/docs/part1/Chapter9#monster-lore-%E6%80%AA%E7%89%A9%E7%9F%A5%E8%AD%98)  
階層 2: [傳奇意志](/docs/part1/Chapter9#will-of-legend-%E5%82%B3%E5%A5%87%E6%84%8F%E5%BF%97)  
階層 3: [受訓殺手](/docs/part1/Chapter9#trained-slayer-%E5%8F%97%E8%A8%93%E6%AE%BA%E6%89%8B)  
階層 3: [改進怪物禍害](/docs/part1/Chapter9#improved-monster-bane-%E6%94%B9%E9%80%B2%E6%80%AA%E7%89%A9%E7%A6%8D%E5%AE%B3) 或 [誤導攻擊](/docs/part1/Chapter9#misdirect-%E8%AA%A4%E5%B0%8E%E6%94%BB%E6%93%8A3-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 4: [繼續戰鬥](/docs/part1/Chapter9#fight-on-%E7%B9%BC%E7%BA%8C%E6%88%B0%E9%AC%A5)  
階層 5: [高級攻擊技能](/docs/part1/Chapter9#greater-skill-with-attacks-%E9%AB%98%E7%B4%9A%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 6: [謀殺者](/docs/part1/Chapter9#murderer-%E8%AC%80%E6%AE%BA%E8%80%858-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [英雄級怪物禍害](/docs/part1/Chapter9#heroic-monster-bane-%E8%8B%B1%E9%9B%84%E7%B4%9A%E6%80%AA%E7%89%A9%E7%A6%8D%E5%AE%B3)

### SOLVES MYSTERIES 解決謎團
你是推論大師，使用證據來尋找答案。

階層 1: [調查員](/docs/part1/Chapter9#investigator-%E8%AA%BF%E6%9F%A5%E5%93%A1)  
階層 1: [神探](/docs/part1/Chapter9#sleuth-%E7%A5%9E%E6%8E%A2)  
階層 2: [遠離傷害](/docs/part1/Chapter9#out-of-harms-way-%E9%81%A0%E9%9B%A2%E5%82%B7%E5%AE%B3)  
階層 3: [你學習了](/docs/part1/Chapter9#you-studied-%E4%BD%A0%E5%AD%B8%E7%BF%92%E4%BA%86) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [得出結論](/docs/part1/Chapter9#draw-conclusion-%E5%BE%97%E5%87%BA%E7%B5%90%E8%AB%963-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [化解局面](/docs/part1/Chapter9#defuse-situation-%E5%8C%96%E8%A7%A3%E5%B1%80%E9%9D%A2)  
階層 6: [奪取主動權](/docs/part1/Chapter9#seize-the-initiative-%E5%A5%AA%E5%8F%96%E4%B8%BB%E5%8B%95%E6%AC%8A5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [高級防禦技能](/docs/part1/Chapter9#greater-skill-with-defense-%E9%AB%98%E7%B4%9A%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)

### SPEAKS FOR THE LAND 大地言談
你與自然和環境的精神聯繫賦予你神秘的能力。

階層 1: [憤怒種子](/docs/part1/Chapter9#seeds-of-fury-%E6%86%A4%E6%80%92%E7%A8%AE%E5%AD%901-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [荒野知識](/docs/part1/Chapter9#wilderness-lore-%E8%8D%92%E9%87%8E%E7%9F%A5%E8%AD%98)  
階層 2: [攫物樹葉](/docs/part1/Chapter9#grasping-foliage-%E6%94%AB%E7%89%A9%E6%A8%B9%E8%91%893-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [安撫野蠻](/docs/part1/Chapter9#soothe-the-savage-%E5%AE%89%E6%92%AB%E9%87%8E%E8%A0%BB2-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [傳達概念](/docs/part1/Chapter9#communication-%E5%82%B3%E9%81%94%E6%A6%82%E5%BF%B52-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 4: [月亮變形](/docs/part1/Chapter9#moon-shape-%E6%9C%88%E4%BA%AE%E8%AE%8A%E5%BD%A24-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [昆蟲爆發](/docs/part1/Chapter9#insect-eruption-%E6%98%86%E8%9F%B2%E7%88%86%E7%99%BC6-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [召喚風暴](/docs/part1/Chapter9#call-the-storm-%E5%8F%AC%E5%96%9A%E9%A2%A8%E6%9A%B47-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [地震](/docs/part1/Chapter9#earthquake-%E5%9C%B0%E9%9C%877-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### STANDS LIKE A BASTION 像堡壘般佇立
你的盔甲，再加上體型，力量，難以置信的訓練或機器增強，使你難以移動或受傷。

階層 1: [著甲熟練](/docs/part1/Chapter9#practiced-in-armor-%E8%91%97%E7%94%B2%E7%86%9F%E7%B7%B4)  
階層 1: [熟手防禦者](/docs/part1/Chapter9#experienced-defender-%E7%86%9F%E6%89%8B%E9%98%B2%E7%A6%A6%E8%80%85)  
階層 2: [抵抗自然因素](/docs/part1/Chapter9#resist-the-elements-%E6%8A%B5%E6%8A%97%E8%87%AA%E7%84%B6%E5%9B%A0%E7%B4%A0)  
階層 3: [不可移動](/docs/part1/Chapter9#unmovable-%E4%B8%8D%E5%8F%AF%E7%A7%BB%E5%8B%953-%E6%B0%A3%E5%8A%9B%E9%BB%9E)  
階層 3: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B) 或 [熟練所有武器](/docs/part1/Chapter9#practiced-with-all-weapons-%E7%86%9F%E7%B7%B4%E6%89%80%E6%9C%89%E6%AD%A6%E5%99%A8)  
階層 4: [牙齒之牆](/docs/part1/Chapter9#wall-with-teeth-%E7%89%99%E9%BD%92%E4%B9%8B%E7%89%86)  
階層 5: [堅忍不拔](/docs/part1/Chapter9#hardiness-%E5%A0%85%E5%BF%8D%E4%B8%8D%E6%8B%94)  
階層 5: [護甲精通](/docs/part1/Chapter9#mastery-in-armor-%E8%AD%B7%E7%94%B2%E7%B2%BE%E9%80%9A)  
階層 6: [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3) 或 [盾牌訓練](/docs/part1/Chapter9#shield-training-%E7%9B%BE%E7%89%8C%E8%A8%93%E7%B7%B4)

### TALKS TO MACHINES 與機器對話
你像電腦一樣使用有機大腦，可以與任何電子設備“無線”連接。你可以以其他人無法控制的方式控制和影響它們。

階層 1: [機器親和](/docs/part1/Chapter9#machine-affinity-%E6%A9%9F%E5%99%A8%E8%A6%AA%E5%92%8C)  
階層 1: [遠程接口](/docs/part1/Chapter9#distant-interface-%E9%81%A0%E7%A8%8B%E6%8E%A5%E5%8F%A32-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [哄騙能量](/docs/part1/Chapter9#coaxing-power-%E5%93%84%E9%A8%99%E8%83%BD%E9%87%8F-2-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [魅惑機器](/docs/part1/Chapter9#charm-machine-%E9%AD%85%E6%83%91%E6%A9%9F%E5%99%A82-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [智能界面](/docs/part1/Chapter9#intelligent-interface-%E6%99%BA%E8%83%BD%E7%95%8C%E9%9D%A23-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [命令機器](/docs/part1/Chapter9#command-machine-%E5%91%BD%E4%BB%A4%E6%A9%9F%E5%99%A84-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [機器夥伴](/docs/part1/Chapter9#machine-companion-%E6%A9%9F%E5%99%A8%E5%A4%A5%E4%BC%B4)  
階層 4: [機器人戰士](/docs/part1/Chapter9#robot-fighter-%E6%A9%9F%E5%99%A8%E4%BA%BA%E6%88%B0%E5%A3%AB)  
階層 5: [訊息收集](/docs/part1/Chapter9#information-gathering-%E8%A8%8A%E6%81%AF%E6%94%B6%E9%9B%865-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [控制機器](/docs/part1/Chapter9#control-machine-%E6%8E%A7%E5%88%B6%E6%A9%9F%E5%99%A86-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [改進機器夥伴](/docs/part1/Chapter9#improved-machine-companion-%E6%94%B9%E9%80%B2%E6%A9%9F%E5%99%A8%E5%A4%A5%E4%BC%B4)  

### THROWS WITH DEADLY ACCURACY 致命精準投擲
離開你手的一切都可以精確地移動到你想讓它去的地方，並以能產生完美範圍和速度的衝擊。

階層 1: [精準](/docs/part1/Chapter9#precision-%E7%B2%BE%E6%BA%96)  
階層 2: [仔細瞄準](/docs/part1/Chapter9#careful-aim-%E4%BB%94%E7%B4%B0%E7%9E%84%E6%BA%96)  
階層 3: [快速投擲](/docs/part1/Chapter9#quick-throw-%E5%BF%AB%E9%80%9F%E6%8A%95%E6%93%B22-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD)  
階層 4: [萬物皆武](/docs/part1/Chapter9#everything-is-a-weapon-%E8%90%AC%E7%89%A9%E7%9A%86%E6%AD%A6)  
階層 4: [專精投擲](/docs/part1/Chapter9#specialized-throwing-%E5%B0%88%E7%B2%BE%E6%8A%95%E6%93%B2)  
階層 5: [投擲旋風](/docs/part1/Chapter9#whirlwind-of-throws-%E6%8A%95%E6%93%B2%E6%97%8B%E9%A2%A85-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 6: [致命傷害](/docs/part1/Chapter9#lethal-damage-%E8%87%B4%E5%91%BD%E5%82%B7%E5%AE%B3) 或 [精通防禦](/docs/part1/Chapter9#mastery-with-defense-%E7%B2%BE%E9%80%9A%E9%98%B2%E7%A6%A6)

### THUNDERS 轟鳴
你發出破壞性的聲音並操縱聲音空間。

階層 1: [雷鳴射束](/docs/part1/Chapter9#thunder-beam-%E9%9B%B7%E9%B3%B4%E5%B0%84%E6%9D%9F2-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 2: [聲音轉換屏障](/docs/part1/Chapter9#sound-conversion-barrier-%E8%81%B2%E9%9F%B3%E8%BD%89%E6%8F%9B%E5%B1%8F%E9%9A%9C)  
階層 3: [消滅聲音](/docs/part1/Chapter9#nullify-sound-%E6%B6%88%E6%BB%85%E8%81%B2%E9%9F%B33-%E6%B0%A3%E5%8A%9B%E9%BB%9E) 或 [迴聲定位](/docs/part1/Chapter9#echolocation-%E8%BF%B4%E8%81%B2%E5%AE%9A%E4%BD%8D)  
階層 4: [碎裂呼喊](/docs/part1/Chapter9#shattering-shout-%E7%A2%8E%E8%A3%82%E5%91%BC%E5%96%8A5-%E9%BB%9E%E6%B0%A3%E5%8A%9B)   
階層 5: [亞音速隆隆聲](/docs/part1/Chapter9#subsonic-rumble-%E4%BA%9E%E9%9F%B3%E9%80%9F%E9%9A%86%E9%9A%86%E8%81%B22-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [放大聲音](/docs/part1/Chapter9#amplify-sounds-%E6%94%BE%E5%A4%A7%E8%81%B2%E9%9F%B32-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 6: [地震](/docs/part1/Chapter9#earthquake-%E5%9C%B0%E9%9C%877-%E9%BB%9E%E6%B0%A3%E5%8A%9B) 或 [致命振動](/docs/part1/Chapter9#lethal-vibration-%E8%87%B4%E5%91%BD%E6%8C%AF%E5%8B%957-%E9%BB%9E%E6%B0%A3%E5%8A%9B)

### TRAVELS THROUGH TIME 穿越時間
你可以看透時間，試圖觸及通過它，最終甚至可以穿越它。

階層 1: [讓子彈飛](/docs/part1/Chapter9#anticipation-%E8%AE%93%E5%AD%90%E5%BD%88%E9%A3%9B1-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [查看歷史](/docs/part1/Chapter9#see-history-%E6%9F%A5%E7%9C%8B%E6%AD%B7%E5%8F%B24-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [時空加速](/docs/part1/Chapter9#temporal-acceleration-%E6%99%82%E7%A9%BA%E5%8A%A0%E9%80%9F5-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [時間循環](/docs/part1/Chapter9#time-loop-%E6%99%82%E9%96%93%E5%BE%AA%E7%92%B04-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [時空錯位](/docs/part1/Chapter9#temporal-dislocation-%E6%99%82%E7%A9%BA%E9%8C%AF%E4%BD%8D7-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [時空二重身](/docs/part1/Chapter9#time-doppelganger-%E6%99%82%E7%A9%BA%E4%BA%8C%E9%87%8D%E8%BA%AB6-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [穿越時間的召喚](/docs/part1/Chapter9#call-through-time-%E7%A9%BF%E8%B6%8A%E6%99%82%E9%96%93%E7%9A%84%E5%8F%AC%E5%96%9A6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [時間旅行](/docs/part1/Chapter9#time-travel-%E6%99%82%E9%96%93%E6%97%85%E8%A1%8C10-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### WAS FORETOLD 被預言
你是「被選中的人」 ，預言、預測、預卜，或者其他決定命運的方法，總有一天會對你有所期待。

階層 1: [互動技巧](/docs/part1/Chapter9#interaction-skills-%E4%BA%92%E5%8B%95%E6%8A%80%E5%B7%A7)  
階層 1: [懂得](/docs/part1/Chapter9#knowing-%E6%87%82%E5%BE%97)  
階層 2: [命定不凡](/docs/part1/Chapter9#destined-for-greatness-%E5%91%BD%E5%AE%9A%E4%B8%8D%E5%87%A1)  
階層 3: [克服一切障礙](/docs/part1/Chapter9#overcome-all-obstacles-%E5%85%8B%E6%9C%8D%E4%B8%80%E5%88%87%E9%9A%9C%E7%A4%993-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [堅忍韌性](/docs/part1/Chapter9#hard-won-resilience-%E5%A0%85%E5%BF%8D%E9%9F%8C%E6%80%A7)  
階層 4: [聚光中心](/docs/part1/Chapter9#center-of-attention-%E8%81%9A%E5%85%89%E4%B8%AD%E5%BF%835-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [給他們指路](/docs/part1/Chapter9#show-them-the-way-%E7%B5%A6%E4%BB%96%E5%80%91%E6%8C%87%E8%B7%AF6-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [正如預言](/docs/part1/Chapter9#as-foretold-in-prophecy-%E6%AD%A3%E5%A6%82%E9%A0%90%E8%A8%80) 或 [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)

### WEARS A SHEEN OF ICE 帶著冰的光澤
你掌控著寒冷和冰的冬天力量。

階層 1: [冰凍護甲](/docs/part1/Chapter9#ice-armor-%E5%86%B0%E5%87%8D%E8%AD%B7%E7%94%B21-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [冰霜之觸](/docs/part1/Chapter9#frost-touch-%E5%86%B0%E9%9C%9C%E4%B9%8B%E8%A7%B81-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [凍結之觸](/docs/part1/Chapter9#freezing-touch-%E5%87%8D%E7%B5%90%E4%B9%8B%E8%A7%B84-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [冰物創造](/docs/part1/Chapter9#ice-creation-%E5%86%B0%E7%89%A9%E5%89%B5%E9%80%A04-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [韌性冰甲](/docs/part1/Chapter9#resilient-ice-armor-%E9%9F%8C%E6%80%A7%E5%86%B0%E7%94%B2)  
階層 5: [寒冷爆裂](/docs/part1/Chapter9#cold-burst-%E5%AF%92%E5%86%B7%E7%88%86%E8%A3%825-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 6: [冰風暴](/docs/part1/Chapter9#ice-storm-%E5%86%B0%E9%A2%A8%E6%9A%B4) 或 [冬霜護手](/docs/part1/Chapter9#winter-gauntlets-%E5%86%AC%E9%9C%9C%E8%AD%B7%E6%89%8B)  

### WEARS POWER ARMOR 穿著動力裝甲
階層 1: [動力裝甲](/docs/part1/Chapter9#powered-armor-%E5%8B%95%E5%8A%9B%E8%A3%9D%E7%94%B2)  
階層 1: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 2: [抬頭顯示器](/docs/part1/Chapter9#heads-up-display-%E6%8A%AC%E9%A0%AD%E9%A1%AF%E7%A4%BA%E5%99%A82-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 3: [融合裝甲](/docs/part1/Chapter9#fusion-armor-%E8%9E%8D%E5%90%88%E8%A3%9D%E7%94%B2) 或 [荒誕健康](/docs/part1/Chapter9#incredible-health-%E8%8D%92%E8%AA%95%E5%81%A5%E5%BA%B7)  
階層 4: [動能爆破](/docs/part1/Chapter9#force-blast-%E5%8B%95%E8%83%BD%E7%88%86%E7%A0%B4)  
階層 5: [實地強化裝甲](/docs/part1/Chapter9#field-reinforced-armor-%E5%AF%A6%E5%9C%B0%E5%BC%B7%E5%8C%96%E8%A3%9D%E7%94%B2)  
階層 6: [大師級盔甲改造 (噴氣輔助飛行)](/docs/part1/Chapter9#masterful-armor-modification-%E5%A4%A7%E5%B8%AB%E7%B4%9A%E7%9B%94%E7%94%B2%E6%94%B9%E9%80%A0) 或 [大師級盔甲改造 (秘具莢)](/docs/part1/Chapter9#masterful-armor-modification-%E5%A4%A7%E5%B8%AB%E7%B4%9A%E7%9B%94%E7%94%B2%E6%94%B9%E9%80%A0) 

### WIELDS TWO WEAPONS AT ONCE 同時揮舞兩件武器
你雙手帶著鋼鐵，隨時準備迎戰任何敵人。

階層 1: [雙持輕型揮舞](/docs/part1/Chapter9#dual-light-wield-%E9%9B%99%E6%8C%81%E8%BC%95%E5%9E%8B%E6%8F%AE%E8%88%9E)  
階層 2: [雙重打擊](/docs/part1/Chapter9#double-strike-%E9%9B%99%E9%87%8D%E6%89%93%E6%93%8A3-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 2: [超級滲透者](/docs/part1/Chapter9#superb-infiltrator-%E8%B6%85%E7%B4%9A%E6%BB%B2%E9%80%8F%E8%80%85)  
階層 3: [雙持中型揮舞](/docs/part1/Chapter9#dual-medium-wield-%E9%9B%99%E6%8C%81%E4%B8%AD%E5%9E%8B%E6%8F%AE%E8%88%9E) 或 [精確切割](/docs/part1/Chapter9#precise-cut-%E7%B2%BE%E7%A2%BA%E5%88%87%E5%89%B2)  
階層 4: [雙持防禦](/docs/part1/Chapter9#dual-defense-%E9%9B%99%E6%8C%81%E9%98%B2%E7%A6%A6)  
階層 5: [雙重分心](/docs/part1/Chapter9#dual-distraction-%E9%9B%99%E9%87%8D%E5%88%86%E5%BF%831-%E9%BB%9E%E9%80%9F%E5%BA%A6)  
階層 6: [繳械攻擊](/docs/part1/Chapter9#disarming-attack-%E7%B9%B3%E6%A2%B0%E6%94%BB%E6%93%8A5-%E9%BB%9E%E9%80%9F%E5%BA%A6) 或 [旋轉攻擊](/docs/part1/Chapter9#spin-attack-%E6%97%8B%E8%BD%89%E6%94%BB%E6%93%8A5-%E9%BB%9E%E9%80%9F%E5%BA%A6)  

### WORKS FOR A LIVING 為生活而工作
你會對一份出色的工作感到非常滿意，無論是編程、建造房屋還是開採小行星。

階層 1: [手巧](/docs/part1/Chapter9#handy-%E6%89%8B%E5%B7%A7)  
階層 2: [鋼鐵肌肉](/docs/part1/Chapter9#muscles-of-iron-%E9%8B%BC%E9%90%B5%E8%82%8C%E8%82%892-%E9%BB%9E%E6%B0%A3%E5%8A%9B)  
階層 3: [細節之眼](/docs/part1/Chapter9#eye-for-detail-%E7%B4%B0%E7%AF%80%E4%B9%8B%E7%9C%BC2-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [即興發揮](/docs/part1/Chapter9#improvise-%E5%8D%B3%E8%88%88%E7%99%BC%E6%8F%AE3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 4: [高級強化氣力](/docs/part1/Chapter9#greater-enhanced-might-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%B0%A3%E5%8A%9B)  
階層 4: [堅強起來](/docs/part1/Chapter9#tough-it-out-%E5%A0%85%E5%BC%B7%E8%B5%B7%E4%BE%86)  
階層 5: [專業技能](/docs/part1/Chapter9#expert-skill-%E5%B0%88%E6%A5%AD%E6%8A%80%E8%83%BD)  
階層 6: [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B) 或 [堅忍韌性](/docs/part1/Chapter9#hard-won-resilience-%E5%A0%85%E5%BF%8D%E9%9F%8C%E6%80%A7)


### WOULD RATHER BE READING 寧願讀書
書是你的朋友。什麼比知識更重要? 沒有。

階層 1: [知識就是力量](/docs/part1/Chapter9#knowledge-is-power-%E7%9F%A5%E8%AD%98%E5%B0%B1%E6%98%AF%E5%8A%9B%E9%87%8F)  
階層 2: [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 3: [應用你的知識](/docs/part1/Chapter9#applying-your-knowledge-%E6%87%89%E7%94%A8%E4%BD%A0%E7%9A%84%E7%9F%A5%E8%AD%98) 或 [靈活技能](/docs/part1/Chapter9#flex-skill-%E9%9D%88%E6%B4%BB%E6%8A%80%E8%83%BD)  
階層 4: [知識就是力量](/docs/part1/Chapter9#knowledge-is-power-%E7%9F%A5%E8%AD%98%E5%B0%B1%E6%98%AF%E5%8A%9B%E9%87%8F)  
階層 4: [了解未知](/docs/part1/Chapter9#knowing-the-unknown-%E4%BA%86%E8%A7%A3%E6%9C%AA%E7%9F%A56-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [高級強化智力](/docs/part1/Chapter9#greater-enhanced-intellect-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%99%BA%E5%8A%9B)  
階層 6: [知識就是力量](/docs/part1/Chapter9#knowledge-is-power-%E7%9F%A5%E8%AD%98%E5%B0%B1%E6%98%AF%E5%8A%9B%E9%87%8F)  
階層 6: [智力之塔](/docs/part1/Chapter9#tower-of-intellect-%E6%99%BA%E5%8A%9B%E4%B9%8B%E5%A1%94) 或 [閱讀符號](/docs/part1/Chapter9#read-the-signs-%E9%96%B1%E8%AE%80%E7%AC%A6%E8%99%9F4-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### WORKS MIRACLES 運用神跡
你可以用觸碰來治療他人，改變時間來幫助他人，並且通常受到大家的喜愛。

階層 1: [治療之觸](/docs/part1/Chapter9#healing-touch-%E6%B2%BB%E7%99%82%E4%B9%8B%E8%A7%B81-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 2: [緩解](/docs/part1/Chapter9#alleviate-%E7%B7%A9%E8%A7%A33-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 3: [治療泉源](/docs/part1/Chapter9#font-of-healing-%E6%B2%BB%E7%99%82%E6%B3%89%E6%BA%90) 或 [奇蹟健康](/docs/part1/Chapter9#miraculous-health-%E5%A5%87%E8%B9%9F%E5%81%A5%E5%BA%B7)  
階層 4: [激發行動](/docs/part1/Chapter9#inspire-action-%E6%BF%80%E7%99%BC%E8%A1%8C%E5%8B%954-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 5: [撤銷](/docs/part1/Chapter9#undo-%E6%92%A4%E9%8A%B75-%E9%BB%9E%E6%99%BA%E5%8A%9B)   
階層 6: [高級治療之觸](/docs/part1/Chapter9#greater-healing-touch-%E9%AB%98%E7%B4%9A%E6%B2%BB%E7%99%82%E4%B9%8B%E8%A7%B84-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [死而復生](/docs/part1/Chapter9#restore-life-%E6%AD%BB%E8%80%8C%E5%BE%A9%E7%94%9F9-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### WORKS THE BACK ALLEYS 暗地做事
你讓別人看不見你，偷富人的東西來達到你的目的。

階層 1: [潛行技能](/docs/part1/Chapter9#stealth-skills-%E6%BD%9B%E8%A1%8C%E6%8A%80%E8%83%BD)  
階層 2: [地下聯絡人](/docs/part1/Chapter9#underworld-contacts-%E5%9C%B0%E4%B8%8B%E8%81%AF%E7%B5%A1%E4%BA%BA)  
階層 3: [欺騙一波](/docs/part1/Chapter9#pull-a-fast-one-%E6%AC%BA%E9%A8%99%E4%B8%80%E6%B3%A23-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [公會訓練](/docs/part1/Chapter9#guild-training-%E5%85%AC%E6%9C%83%E8%A8%93%E7%B7%B4)  
階層 4: [盜賊大師](/docs/part1/Chapter9#master-thief-%E7%9B%9C%E8%B3%8A%E5%A4%A7%E5%B8%AB)  
階層 5: [卑劣戰法](/docs/part1/Chapter9#dirty-fighter-%E5%8D%91%E5%8A%A3%E6%88%B0%E6%B3%952-%E9%BB%9E%E9%80%9F%E5%BA%A6)   
階層 6: [街頭鼠目](/docs/part1/Chapter9#alley-rat-%E8%A1%97%E9%A0%AD%E9%BC%A0%E7%9B%AE6-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [全力以赴](/docs/part1/Chapter9#all-out-con-%E5%85%A8%E5%8A%9B%E4%BB%A5%E8%B5%B47-%E9%BB%9E%E6%99%BA%E5%8A%9B)

### WORKS THE SYSTEM 利用系統
你以利用人工系統中的缺陷，包括但不限於電腦代碼。

階層 1: [破解不可能](/docs/part1/Chapter9#hack-the-impossible-%E7%A0%B4%E8%A7%A3%E4%B8%8D%E5%8F%AF%E8%83%BD3-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 1: [電腦程式設計](/docs/part1/Chapter9#computer-programming-%E9%9B%BB%E8%85%A6%E7%A8%8B%E5%BC%8F%E8%A8%AD%E8%A8%88)  
階層 2: [有聯繫](/docs/part1/Chapter9#connected-%E6%9C%89%E8%81%AF%E7%B9%AB)  
階層 3: [詐騙高手](/docs/part1/Chapter9#confidence-artist-%E8%A9%90%E9%A8%99%E9%AB%98%E6%89%8B) 或 [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)  
階層 4: [迷惑敵人](/docs/part1/Chapter9#confuse-enemy-%E8%BF%B7%E6%83%91%E6%95%B5%E4%BA%BA4-%E9%BB%9E%E6%99%BA%E5%8A%9B)  
階層 5: [鍛煉友情](/docs/part1/Chapter9#work-the-friendship-%E9%8D%9B%E7%85%89%E5%8F%8B%E6%83%854-%E9%BB%9E%E6%99%BA%E5%8A%9B) 
階層 6: [運用人情](/docs/part1/Chapter9#call-in-favor-%E9%81%8B%E7%94%A8%E4%BA%BA%E6%83%854-%E9%BB%9E%E6%99%BA%E5%8A%9B) 或 [高級強化潛力](/docs/part1/Chapter9#greater-enhanced-potential-%E9%AB%98%E7%B4%9A%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)


## 自創焦點
本節提供了自創焦點所需的一切。

每個焦點都有總體風格，無論是探索、能量操縱，還是簡單地在戰鬥中造成大量傷害。這些廣泛的分類被稱為焦點類別。

焦點類別也屬於一個總體主題，然後是選擇指南，描述如何從[第9章](/docs/part1/Chapter9)的每個階層（從第一層到第六層）選擇能力。

自創焦點應該以動詞的形式命名，如「控制野獸」或「在石頭中生存」。例如，按照能量操縱焦點類別的指南創建的用火焦點可以稱為[背負火焰光環](/docs/part1/Chapter8#bears-a-halo-of-fire-%E8%83%8C%E8%B2%A0%E7%81%AB%E7%84%B0%E5%85%89%E7%92%B0)。或者，一個新的用火焦點應該有個全新的名字，如「激起末日之火」或「以思想助燃火焰」。

### 通過相對力量選擇能力
能力選擇指南指導你從三種範圍中選擇能力：低階、中階和高階。這些範圍與每個能力的力量程度對應。並且根據其作用被進一步分類為不同的能力類別，提高物理攻擊的能力屬於「攻擊技能」類別，協助盟友的能力屬於「支援」類別，等等。在[第9章](/docs/part1/Chapter9)的能力類別和相對力量部分，能找到階層和類別。

低階適合 1-2 層。中階能力則適合 3-4 層。高階能力則該給予 5-6 層的焦點能力。

話說回來，有時你會發現給 3-4 層分配低階能力，或者在 1-2 層分配中階能力很合適。盡量別這樣做，但也不是不行。有時這是你自創焦點的好方法之一。較高階的能力通常需要花費更多的屬性池點數。因此，如果一個中階能力在 1-2 層可用，或一個高階能力在 3-4 層可用，較高的成本將是一個平衡因素。

### 平衡能力
每個類別中的準則在很大程度上能確保你所建立的焦點是平衡的。有時，根據焦點需要，給一個低強度能力和一個符合階層的一般能力可能是合適的。「低強度能力」是特意留給GM解釋的，但一般來說，它不應該比一個低階能力（也就是通常在 1-2 層可用的能力）更強大。

例如，一個使用寒冰的人除了發射寒冰射線外，可能還能製造小雪雕。用電的人可能會給耗竭的法器充能，或者擁有處理電力系統的資產。諸如此類。

通常情況下，焦點指南會指出這種可能性。然而，你自己能夠決定焦點是否需要額外能力。如果你增加了一個能力，或者給了一個超出階層強度的能力，這可能意味著在下個階層或前個階層的選擇並不那麼好。平衡焦點是門藝術。讓你的焦點平衡，不要太過，也不要太弱。

### 能力指南不是規定性的
每個焦點類別都提供了準則，說明你應該在每個階層選擇什麼樣的能力。但不要把這些準則當作是無法改變的東西。這不是硬性規定；只是起個頭而已。你可能想在某特定階層改變準則中沒有指明的能力種類。只要選擇的能力屬於該階層的預期強度，那就可以了。準則並不是限制。

例如，如果你正在為奇幻類型的遊戲建立使用寒冷的焦點，你可能會決定，在某特定階層，召喚惡魔比對某區域造成傷害的能力更好，這正是能量操縱的第五階層準則所要求的。如果你把新焦點稱為「引導第九環」，那麼做出這種改變特別有用。

### 能力替換
如果你正在創建焦點，而且你認為應該在第一層時要有一組能力，但這組能力在機制上會過強，你能選擇增加「替換」能力。這允許角色用類型的能力來替換低階焦點的能力，而不是直接獲得。

### 概念和類別
選擇創造使用特定概念的焦點，例如「創造幻覺」，並不會把你困在特定類別中。焦點能通過各種方式使用特定的能量、工具或概念來構建，每種最終都會導致焦點有不同的結果。這取決於目的。在這種情況下，創造幻覺可能被用來動搖他人，這就需要使用影響類的準則來構建焦點。

同樣，如果焦點賦予角色召喚某種力量或能量的能力，這並不意味著這個焦點應該自動使用能量操縱類別準則來建立（當然，如果用這種能量攻擊和保護自己是重點的話，它也可以）。但你也能以同樣性質的方式將焦點，聚焦於耐用性上，這可能就更偏向坦克戰鬥焦點（能在戰鬥中承受大量懲罰的人）；或者以最大化傷害為主要關注點進行爆破，這樣就會是個前鋒戰鬥焦點；或者創造一個由該能量或力量組成的追隨者，則能透過盟友使用焦點（也就是說，一個使用幫助生物、NPC、甚至自己的複製版本來給他們一個優勢）。

這是另個例子：控制重力的焦點能想像成環境操縱的焦點或能量操縱的焦點。這取決於這個焦點是更關注粉碎和固定事物（環境操縱），還是更關注爆破事物和用重力保護自己（能量操縱）。

概念的可塑性在其他領域也是如此。例如，如果某人能夠召喚和塑造生土，則可能會用來把自己變成一個石頭人（坦克戰鬥），來打擊敵人（前鋒戰鬥），或者創造牆壁、路障和盾牌來保護他們的盟友（支援）。

### 有關聯能力的能力
[第9章](/docs/part1/Chapter9)中的一些能力有關聯能力。 如果你為焦點或類型選擇了有關聯能力的能力，那麼你得在類型或焦點中包含較低階的能力，作為 PC 的選擇。

### 修改能力以適應概念
如果你正在尋找一種能力，但在[第9章](/docs/part1/Chapter9)的龐大目錄中找不到合適的，則能考慮自己重新貼皮，使其看起來很新（並達到你所需要的目的）。重新貼皮的意思是，你以某個能力作為基本機制，但用某種方式改變其風味。例如，也許你要創造一個新的移動土地的焦點，但找不到足夠的與土有關的能力來滿足你的需要。改變其他能力的描述風味，使其使用土而不是火、冷或磁很容易。例如，火之翼可以變成土之翼，冰之盔甲可以變成土之盔甲，等等。這些改變除了傷害類型和任何連帶效應（例如，土之翼可能會在其身後產生塵埃雲）外，沒有任何變化。

### 創造全新的能力
你能做到比重新貼皮更進一步，創造一個或多個全新能力。在這樣做的時候，盡量找到與你想要的效果相近的東西，然後將其作為模板。在任何情況下，當涉及到角色的屬性池時，決定能力應該花費多少是很重要的。

你可能會注意到，更高階的能力更昂貴。因為作用更大，但也是因為高階角色比低階角色有更多的節省值，這意味著他們從相關屬性池中花費的點數更少。一個在相關屬性池中有 3 節省值的第三階層角色，對於花費 3 或更少點數的能力無需支付費用。這對低階能力來說很好，但你通常希望角色在使用他們最強大的能力時要考慮一下。這意味著能力應該比該層級的角色可能擁有的節省值至少多花 1 點。（通常情況下，角色會在相關屬性池裡有與他們階層相等的節省值。）

作為好的經驗法則，典型能力應該花費與階層相等的點數。

### 選擇 GM 入侵
想想有哪些事情可能讓人感到驚訝、驚恐，或者對某人來說出現災難性的錯誤，並把它指定為該焦點的 GM 入侵。當然，這往往是在遊戲過程中即興完成的。但是，在構建焦點的過程中對這個題目進行思考，並作為焦點的主體GM入侵，能在發生狀況時帶來一些靈感。


## 焦點類別

### 運用盟友
優先為角色提供 NPC 追隨者的焦點是運用盟友的焦點。這些追隨者以各種方式給 PC 提供幫助，但從根本上說，通常是為角色的行動提供資產。

在運用盟友類別中存在多種潛在主題，從允許角色召喚或製作盟友的能力，到允許通過名聲、魔法或基本權威或魅力吸引盟友的能力。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 角色維持盟友所需的任何物品。例如，使用超科技創造機器人盟友的人，需要工具來製作和修理這些盟友。這個類別中的一些焦點不需要任何東西來獲得或維持其利益。

**建議弱效影響：** NPC 盟友使下回合的任務簡化。

**建議強效影響：** NPC 盟友立即獲得一個額外動作。

> 焦點範例
> - [建造機器人](/docs/part1/Chapter8#builds-robots-%E5%BB%BA%E9%80%A0%E6%A9%9F%E5%99%A8%E4%BA%BA) 
> - [與死者為伍](/docs/part1/Chapter8#consorts-with-the-dead-%E8%88%87%E6%AD%BB%E8%80%85%E7%82%BA%E4%BC%8D)
> - [控制野獸](/docs/part1/Chapter8#controls-beasts-%E6%8E%A7%E5%88%B6%E9%87%8E%E7%8D%B8) 
> - [存在於兩地](/docs/part1/Chapter8#exists-in-two-places-at-once-%E5%AD%98%E5%9C%A8%E6%96%BC%E5%85%A9%E5%9C%B0)
> - [領導](/docs/part1/Chapter8#leads-%E9%A0%98%E5%B0%8E) 
> - [掌握群體](/docs/part1/Chapter8#masters-the-swarm-%E6%8E%8C%E6%8F%A1%E7%BE%A4%E9%AB%94) 
> - [靈魂牧者](/docs/part1/Chapter8#shepherds-spirits-%E9%9D%88%E9%AD%82%E7%89%A7%E8%80%85)

#### 能力選擇指南
**第一階層：** 選擇給予角色 2 級 NPC 追隨者的低階能力，或者給予 NPC 提供的類似好處。或者，通過選擇能使角色對他人產生影響的能力，為在更高階層上獲得 NPC 盟友奠定基礎。

有時，根據焦點，額外的低強度能力是合適的。通常情況下，是指對相關知識領域或技能方面給予受訓的能力。例如，與角色獲得 NPC 追隨者種類相關的技能受訓將是適合的。

**第二階層：** 選擇一個低階能力，該能力對前一層獲得的類似追隨者的 NPC 產生影響。如果在前一層沒有獲得追隨者，那麼此處應該選擇能獲得追隨者的能力。

有時，除了上述能力外，提供一個低強度能力可能是適合的，也許是能增加屬性池 2-3 點的能力。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該強化先前的 NPC 追隨者（通常從 2 級提高到 3 級）或給予額外追隨者的中階能力。

另個選項應該是有利於角色的東西，也許是一種進攻或防禦能力，或者是能擴大對追隨者（或潛在追隨者）影響的東西。

**第四階層：** 選擇1個中階能力，如果之前沒有獲得攻擊或防禦性能力的話，最好是跟焦點主題有關。例如，如果角色因魅力而獲得追隨者，此能力可能是讓他們能短時間內指揮敵人。如果角色通過建立或召喚追隨者而獲得追隨者，此能力也能讓他們影響那些還沒有成為追隨者的同類型實體。

另外，此能力可能會進一步提高之前獲得的追隨者等級，從 3 級提高到 4 級，或者給予一個額外追隨者。

**第五階層：** 選擇一種能力，通過提供防禦、改善屬性池或另種保護來改善角色。

另外，這個能力能為影響和召喚與焦點主題相關的 NPC 盟友方面開闢一條新道路。例如，飼養野獸盟友的人能獲得召喚小型野獸群的能力。製造機器人的人可能獲得一種能力，能夠製作數個小機器人幫手。以此類推。

最後，此能力可能會將之前追隨者提高到5級。

**第六階層：** 選擇2個高階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個能力應該將以前獲得的追隨者提高到 5 級，如果這不是在 5 階層就提供的話。或是為角色提供少量的 3 級追隨者。

另個高階能力能為影響和召喚與焦點主題有關的 NPC 盟友方面開闢一條新的道路。例如，通過高魅力和訓練獲得追隨者的人可能會獲得一種能力，以了解本來不可能收集到的訊息。

### 基礎
主要依靠提供技能訓練、任務資產、提升屬性池和節省值以改善角色的焦點屬於基礎類別。與其他大多數類別一樣，也包括總體主題，使所提供的各種基本能力具有意義。

此外，由於這種焦點提供的好處大多是直接的（通常有幾個例外），大多數而且也適合於非奇幻戰役，在這些戰役中，魔法、超級科學或精神能力通常不會發揮作用。也就是說，基礎焦點所賦予的能力是直接的，但這並不意味著在與類型、描述符、秘具和其他角色方面所賦予的能力相結合時效果就不如人。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何完成該焦點的首要主題所需的物品。例如，名為[寧願讀書](/docs/part1/Chapter8#would-rather-be-reading-%E5%AF%A7%E9%A1%98%E8%AE%80%E6%9B%B8)的焦點應該給角色少量的書。名為[為生活而工作](/docs/part1/Chapter8#works-for-a-living-%E7%82%BA%E7%94%9F%E6%B4%BB%E8%80%8C%E5%B7%A5%E4%BD%9C)的焦點應該提供一套工具。

**建議弱效影響：** 下個動作簡化。

**建議強效影響：** 進行一次免費的無動作恢復骰，該恢復骰不計入每日數量。


> 焦點範例
> - [不多做](/docs/part1/Chapter8#doesnt-do-much-%E4%B8%8D%E5%A4%9A%E5%81%9A) 
> - [解釋法律](/docs/part1/Chapter8#interprets-the-law-%E8%A7%A3%E9%87%8B%E6%B3%95%E5%BE%8B) 
> - [快速學習](/docs/part1/Chapter8#learns-quickly-%E5%BF%AB%E9%80%9F%E5%AD%B8%E7%BF%92)
> - [為生活而工作](/docs/part1/Chapter8#works-for-a-living-%E7%82%BA%E7%94%9F%E6%B4%BB%E8%80%8C%E5%B7%A5%E4%BD%9C)
> - [寧願讀書](/docs/part1/Chapter8#would-rather-be-reading-%E5%AF%A7%E9%A1%98%E8%AE%80%E6%9B%B8)

#### 能力選擇指南
**第一階層：** 選擇一項能力，賦予與焦點主題相關的技能受訓或資產，或給某個屬性池 5-6 點。

或者，選擇一項只給某個屬性池提供 2-3 點的能力，以及一個只給一個任務提供受訓或資產的能力。

**第二階層：** 選擇第一階層未選擇的任何一種能力。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該是一個實務能力，並能提高角色在焦點主題中的能力。例如，如果主題涉及以某種方式關注，那麼收集訊息的能力可能是合適的。

另一個選項應該能夠提高角色的節省值，或者為角色提供某種防禦。

**第四階層：** 選擇另一項能力，給予與焦點主題相關的技能額外的受訓或資產，或給最適合焦點主題的屬性池 5-6 點。或者選擇兩個只提供 2-3 點的能力，加上另一個改善單一任務或技能的中階能力。

或者，提供一個第五階層的能力。

最後，如果焦點還沒有給予某種防禦，這裡能提供一種防禦能力。

**第五階層：** 選擇一項能力，使角色能夠擁有像[專家技能]那樣的能力，能夠自動成功完成有受訓的任務。

或者，如果在第四階層提供了一個非標準的好處，在這裡提供第四階層建議的好處。

**第六階層：** 選擇 2 個高階能力。把這兩項能力作為焦點選項；PC 選擇其中一種。

其中一個應該是為某個最適合該焦點的特定屬性池提供另外 5-6 點的能力，或者角色能按照自己的意願進行分配。另外，進攻或防禦方面的受訓也是合適的。

另一個第六階層選項應該在主題範圍內給角色全新的能力，但不是非實務能力。例如，允許角色進行 2 次動作的能力是合理的。給予額外的受訓、資產或節省值也是可以的。

### 能量操縱
能量操縱焦點提供的能力是引導火焰、閃電、力場、磁力，或非標準形式的能量，如寒冷、土石、或像「虛空」或「陰影」這樣的奇怪東西。這些能力通常給角色提供方法，在攻擊敵人和給予自己或盟友額外保護之間實現某種平衡。焦點通常還提供了其他能力，提供了使用特定能量的方法，如運輸、創造能夠影響多個目標的大量能量，或創造臨時物體或能量屏障。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 一件或多件對所操縱的能量免疫的設備，可能是一套衣服。或者，與正在產生的能量有關的東西。這類焦點不需要額外設備。

**能量異能：** 如果角色類型賦予了通常使用其他種類能量的特殊能力，現在則產生此焦點所使用的種類。例如，如果角色用此焦點來操縱閃電，原本的力場爆破就會變成閃電爆炸。這些改變除了傷害類型和任何連鎖反應（例如，閃電可能會暫時使電子系統短路）外，沒有任何變化。 

**建議弱效影響：** 目標或附近的東西因為殘餘能量而受阻。

**建議強效影響：** 目標身上的一件重要物品被摧毀。

> 焦點範例
> - [吸收能量](/docs/part1/Chapter8#absorbs-energy-%E5%90%B8%E6%94%B6%E8%83%BD%E9%87%8F)
> - [背負火焰光環](/docs/part1/Chapter8#bears-a-halo-of-fire-%E8%83%8C%E8%B2%A0%E7%81%AB%E7%84%B0%E5%85%89%E7%92%B0)
> - [與暗物質共舞](/docs/part1/Chapter8#dances-with-dark-matter-%E8%88%87%E6%9A%97%E7%89%A9%E8%B3%AA%E5%85%B1%E8%88%9E)
> - [乘御閃電](/docs/part1/Chapter8#rides-the-lightning-%E4%B9%98%E5%BE%A1%E9%96%83%E9%9B%BB)
> - [轟鳴](/docs/part1/Chapter8#thunders-%E8%BD%9F%E9%B3%B4)
> - [帶著冰的光澤](/docs/part1/Chapter8#wears-a-sheen-of-ice-%E5%B8%B6%E8%91%97%E5%86%B0%E7%9A%84%E5%85%89%E6%BE%A4)

#### 能力選擇指南
**第一階層：** 選擇一項低階能力，以某種方式使用適當的能量類型造成傷害或提供保護。

有時，根據能量類型的不同，一個額外的低階能力也是合適的。例如，操縱寒冷的焦點可能會賦予創造雪雕的能力。操縱電力的焦點可能會賦予給耗盡的神器充能的能力，或者有處理電力系統的資產。吸收能量的焦點可能會賦予釋放能量作為基本攻擊的能力。以此類推。

**第二階層：** 選擇第一階層未選擇的任何一種能力。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該是使用適當的能量類型造成傷害的能力（可能還有相關效果）。

另一個選項應該是通過使用適當的能量類型來增強移動能力，給予能量相關的額外保護，或者以一種全新的方式使用能量，例如從機器中耗盡能量（如果使用電），將受害者困在一層冰中（如果使用寒冷），創造完美的沉默（如果使用聲音），創造耀眼的照明（如果使用光），等等。

**第四階層：** 選擇第三階層未選擇的任何一種能力。

**第五階層：** 選擇一個能造成傷害（可能還有相關效果）的高階能力，該能力可以使用適當的能量類型影響一個以上的目標，或者選擇一個以某種未曾使用過的方式使用能量的能力，如第三和第六階層所述。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個高階能力應該能用能量來對一個或幾個目標造成大量傷害。

另一個選項應該使用適當的能量類型來完成以前低階能力所不能提供的任務，例如塑造一個火焰追隨者（如果使用火），作為一陣閃電傳送到很遠的地方（如果使用電），從能量中創造固體物體，等等。

### 環境操縱
允許角色移動物體、影響重力、創造物體（或物體幻覺）等等的焦點是環境操縱焦點。鑑於在許多情況下，能量被用作此過程的一部分，這類別和能量操縱在某種程度上是重疊的。環境操縱著重於優先考慮通過物體、力量和周圍環境的改變間接影響敵人和盟友的能力；而能量操縱著重於考慮用所選擇的能量或力量直接傷害目標。

例如，使用基於重力的環境操縱焦點的角色更有可能擁有將目標固定在原地的能力，重力投擲重物作為攻擊手段，或者降低某特定區域甚至某特定物體的重力。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何操縱周圍環境所需的物體。例如，一個擁有賦予製作物品能力的焦點的人需要基本的工具。這個類別中的一些焦點不需要任何東西來獲得或維持好處。

**環境操縱能力：** 涉及圖像或可見能量的焦點主題能影響你的類型能力的外觀。不過只會改變外觀，此外沒有特別效果。如果操縱重力，也許會有一種明顯的藍色光芒滲透到所有能力的使用中，包括類型能力。如果能產生幻象，也許會有華麗的視覺和聽覺特質伴隨著類型能力，比如在使用[停滯]時，有觸角的野獸會把目標固定在原地。以此類推。

**建議弱效影響：** 目標轉身，下一次攻擊受阻。

**建議強效影響：** 角色刷新並恢復 4 點到一個屬性池。

> 焦點範例
> - [喚醒夢境](/docs/part1/Chapter8#awakens-dreams-%E5%96%9A%E9%86%92%E5%A4%A2%E5%A2%83)
> - [光輝四射](/docs/part1/Chapter8#blazes-with-radiance-%E5%85%89%E8%BC%9D%E5%9B%9B%E5%B0%84)
> - [計算不可估量](/docs/part1/Chapter8#calculates-the-incalculable-%E8%A8%88%E7%AE%97%E4%B8%8D%E5%8F%AF%E4%BC%B0%E9%87%8F)
> - [控制重力](/docs/part1/Chapter8#controls-gravity-%E6%8E%A7%E5%88%B6%E9%87%8D%E5%8A%9B)
> - [工藝幻術](/docs/part1/Chapter8#crafts-illusions-%E5%B7%A5%E8%97%9D%E5%B9%BB%E8%A1%93)
> - [創造獨特物品](/docs/part1/Chapter8#crafts-unique-objects-%E5%89%B5%E9%80%A0%E7%8D%A8%E7%89%B9%E7%89%A9%E5%93%81)
> - [磁力掌控](/docs/part1/Chapter8#employs-magnetism-%E7%A3%81%E5%8A%9B%E6%8E%8C%E6%8E%A7)
> - [專注精神而非物質](/docs/part1/Chapter8#focuses-mind-over-matter-%E5%B0%88%E6%B3%A8%E7%B2%BE%E7%A5%9E%E8%80%8C%E9%9D%9E%E7%89%A9%E8%B3%AA)

#### 能力選擇指南
**第一階層：** 選擇一個低階能力，授予使用焦點主題改變環境（或預測環境）的能力的基本用法。例如，一個影響重力的焦點會提供一種使目標變輕或變重的能力。一個製造幻覺的焦點可能會賦予一種能力，允許創造圖像。製造物體的焦點可能會賦予創造特定種類物體的基本能力。預測焦點可能會計算出結果，並為角色提供這種預知的好處。以此類推。

有時，一個額外的低強度能力是合適的，這取決於焦點。通常情況下，這是個在相關知識領域提供技能受訓的能力。

**第二階層：** 選擇一個低階能力，提供一個與焦點主題相關的新的防禦或進攻能力。

或者，這個能力能提供一種額外或全新的能力來操縱與重點主題有關的環境。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

其中一個選項應該是與焦點主題相關的中階能力，提供了額外的環境操縱能力，或進一步改善了先前授予的基本環境操縱能力。這種能力不是直接的進攻或防禦性，而是提供與基本能力相關的全新能力，或者增加先前解鎖的基本能力的強度、範圍或其他一些擴展。

如果可能的話，另一個中階選項應該提供一個與焦點提供的特定移動形式有關的攻擊或防禦性能力。

**第四階層：** 選擇一個中階能力，該能力上一階層沒被選擇的進攻或防禦性能力。

**第五階層：** 選擇一個低階能力的高階版本。例如，如果焦點賦予的操縱是虛幻的，此能力可能會用可怕的圖像來困擾目標。如果焦點是基於重力的，則可能會解鎖飛行。如果是磁性，它可能允許使用者重塑金屬。如果焦點賦予心靈能力，這種能力可以讓一個角色向敵人投擲巨大的物體。以此類推。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個能力應該提供攻擊或防禦性能力，與第四階層提供的能力相反（此處選擇高階能力）。

另一個選擇應該是進一步探索使用基本環境操縱能力的東西。如果第五階層的能力想要能夠擴展的話，這可能是一種與所提供的操縱能力有關的更好的終極能力，或者是一種使用該能力的不同方式，以解鎖該能力的一個尚未探索的方面。


### 探索
允許角色收集訊息、在陌生環境中生存、找到新地點或追蹤特定生物和敵人的焦點是探索焦點。在不熟悉的環境中生存需要合理地選擇防禦選項；然而，允許角色尋找和學習的能力更為優先。

探索焦點依賴於各種方法，儘管訓練和專業知識是主要的。有些方法需要特定工具（如載具）來提供好處，而其他方法可能依靠超自然或超級科學來學習新事物，並從遠處探索陌生的地方。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何探索所需的物品。例如，開始地圖和/或指南針是基本裝備，而使用精神能力的人可能需要一面鏡子或水晶球來凝視。如前所述，裝備還可能包括獲得探索所需的載具。

**建議弱效影響：** 你在任何涉及使用感官的行動上有資產，例如感知或攻擊，直到下一回合結束。

**建議強效影響：** 你的智力節省值 +1，直到下一輪結束。

> 焦點範例
> - [探索黑暗之地](/docs/part1/Chapter8#explores-dark-places-%E6%8E%A2%E7%B4%A2%E9%BB%91%E6%9A%97%E4%B9%8B%E5%9C%B0) 
> - [滲透](/docs/part1/Chapter8#infiltrates-%E6%BB%B2%E9%80%8F) 
> - [秘密行動](/docs/part1/Chapter8#operates-undercover-%E7%A7%98%E5%AF%86%E8%A1%8C%E5%8B%95)
> - [駕駛星艦](/docs/part1/Chapter8#pilots-starcraft-%E9%A7%95%E9%A7%9B%E6%98%9F%E8%89%A6)
> - [天眼通](/docs/part1/Chapter8#sees-beyond-%E5%A4%A9%E7%9C%BC%E9%80%9A)
> - [使身心分離](/docs/part1/Chapter8#separates-mind-from-body-%E4%BD%BF%E8%BA%AB%E5%BF%83%E5%88%86%E9%9B%A2)

#### 能力選擇指南
**第一階層：** 選擇一個低階能力，在焦點的主題內，賦予角色基本的探索、生存或訊息收集能力。

有時，一個額外的低階能力是合適的，這取決於焦點。通常，這是一個在相關知識領域或相關技能中給予技能受訓的能力（儘管這可能已經包括在主要能力中）。另外，它能為氣力池提供 2-3 點的簡單獎勵。

**第二階層：** 選擇另一項低階能力，賦予與探索、生存或訊息收集有關的額外能力。

例如，一個致力於在野蠻條件下生存的焦點可能提供一種（或兩種）能力，使其更容易避開自然災害、毒藥、困難的地形等等。專注於探索某一特定區域的人可能會提供進入該區域的能力，或其他人通常缺乏的能力（如在黑暗中看到的能力）。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

其中一個選項應該進一步提高所授予的基本探索能力，或者給予一個新的探索、生存或訊息收集能力。

另一個選項應該是對角色有利的東西，可能是進攻或防禦能力（特別是如果這個焦點還沒有授予這種能力的話），或是是進一步擴大角色在焦點所選領域的探索能力的東西。

**第四階層：** 選擇一項有利於該角色的中階攻擊或防禦性能力（在第三階層沒提供的）。或者，如果攻擊和防禦性能力已經得到了很好的體現，那麼選擇一個不同的中階能力，以擴大角色的探索、生存或收集訊息的能力。

**第五階層：** 選擇一種高階能力，以減輕在通常荒涼的地方進行探索、生存或收集訊息的一些懲罰。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個選項應該進一步改善之前授予的基本探索主題能力，或者給予一個全新的探索、生存或訊息收集能力。

另一個選項應該是對角色有利的東西，可能是進攻或防禦能力，或是另一種進一步擴大他們在焦點所選領域中的探索能力的能力。


### 影響力
優先考慮權威和影響力的焦點，無論是讓人或機器按照命令行事，幫助他人，還是提升到更有聲望的重要位置，都是都屬於影響類別。

此焦點通過訓練和說服，直接的精神操縱，利用名聲引起人們的注意並影響行動，或者僅僅通過了解和學習影響後來決定的事情來給予影響力。在此意義上，影響力的概念是廣泛的。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何實現所建議的影響力所必需的物品都應該作為額外的裝備來授予。有些影響力的焦點不需要任何東西來獲得或維持其利益。

**建議弱效影響：** 影響能力的範圍或持續時間加倍。

**建議強效影響：** 盟友或指定的目標可以採取額外動作。

> 焦點範例
> - [命令精神力量](/docs/part1/Chapter8#commands-mental-powers-%E5%91%BD%E4%BB%A4%E7%B2%BE%E7%A5%9E%E5%8A%9B%E9%87%8F) 
> - [進行怪異科學](/docs/part1/Chapter8#conducts-weird-science-%E9%80%B2%E8%A1%8C%E6%80%AA%E7%95%B0%E7%A7%91%E5%AD%B8)
> - [融合思想和機器](/docs/part1/Chapter8#fuses-mind-and-machine-%E8%9E%8D%E5%90%88%E6%80%9D%E6%83%B3%E5%92%8C%E6%A9%9F%E5%99%A8)
> - [被百萬人崇拜](/docs/part1/Chapter8#is-idolized-by-millions-%E8%A2%AB%E7%99%BE%E8%90%AC%E4%BA%BA%E5%B4%87%E6%8B%9C)
> - [解決謎團](/docs/part1/Chapter8#solves-mysteries-%E8%A7%A3%E6%B1%BA%E8%AC%8E%E5%9C%98)
> - [與機器對話](/docs/part1/Chapter8#talks-to-machines-%E8%88%87%E6%A9%9F%E5%99%A8%E5%B0%8D%E8%A9%B1)
> - [利用系統](/docs/part1/Chapter8#works-the-system-%E5%88%A9%E7%94%A8%E7%B3%BB%E7%B5%B1)
 
#### 能力選擇指南
**第一階層：** 選擇一個低階能力，使角色能夠學到足夠重要的東西，以便能夠選擇明智的行動方案（或者利用這些知識來說服或恐嚇）。角色如何學習這些訊息，因焦點的具體內容而異。角色可能會做實驗來學習答案，另個角色可能會與其他人打開心靈感應的連結來秘密而快速地交換訊息，角色也可能只是接受互動任務的訓練。

有時，額外的低強度能力是合適的，這取決於焦點。通常，這是種在相關知識領域授予技能訓練的能力。

**第二階層：** 選擇低階能力，以提高角色應用影響力的能力。這可能會在焦點的基本主題上開闢額外戰線，或者只是進一步加強已經提供的基本能力。例如，這種第二階層能力可以使與影響有關的任務簡化數級，使心靈感應者能夠讀懂別人的心思，而這些人有他們本來不會透露的秘密，或者賦予對實物的影響（改善或了解）。以此類推。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

如果可能的話，選項應該提供一種與焦點的特定種類的影響有關的攻擊或防禦性能力。例如，發明家可能創造一種血清，使能力增強（可用於進攻或防禦），心靈感應者可能有用精神能量炸毀敵人的方法，而只有辯論的基本技能和通過名聲施加影響的人可能不得不依靠武器訓練或他們的隨從。

另一個中階選項應該在焦點主題中提供額外的影響能力，或者進一步提高之前授予的基本影響能力。這個選項不是直接的進攻或防禦性能力，而是提供與基本能力相關的全新能力，或者增加先前解鎖的基本能力的強度、範圍或其他一些擴展。例如，心靈感應者可能有一個精神暗示的能力。

**第四階層：** 選擇一個中階能力，選擇在上一層沒有選擇的進攻或防禦性能力。

或者，這個能力可以授予一種與焦點所提供的影響有關的額外能力。

**第五階層：** 選擇一個低階能力的高階版本。

或者，選擇一個以前沒有在低階獲得的能力，一個能在特定影響能力上開闢新局面的能力。例如，如果焦點賦予的影響力是心靈感應，那麼第五階層的能力可能允許一個角色看到未來，以獲得對付敵人（和盟友）的資產。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個選項應該提供一種進攻或防禦性能力，並且第四階層提供的能力相反（儘管是高階而不是中階的）。

另一個選項應該是進一步探索焦點所提供的基本影響能力的使用。如果第五階層的選擇是低階能力的高階版本，那麼這可能是與所使用的影響力有關的更好的終極能力，或者是一種使用該能力的不同方式，以解鎖該能力的一個尚未探索的方面。

### 不規則
大多數焦點都有基本主題，「角色故事」，從邏輯上導致了一系列相關的能力。然而，某些焦點的主題非常廣泛，除了自己以外，不適合其他類別。

不規則焦點提供了一籃子不相干的能力。通常，因為首要主題是需要變化性以及不同主題的能力。通常情況下，這些焦點特定於某些體裁並建議對規則進行調整以進一步利用，例如超級英雄體裁中的力量切換和奇幻體裁中的施法。然而，其他不規則的焦點也是可能的。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 焦點主題所需的任何物品。例如，以超級英雄為主題的焦點可能會授予一件超級英雄的服裝。

**建議弱效影響：** 目標也會暈眩一輪，在此期間所有任務都會受阻。

**建議強效影響：** 目標震懾並失去下一輪。

> 焦點範例
> - [引導神性祝福](/docs/part1/Chapter8#channels-divine-blessings-%E5%BC%95%E5%B0%8E%E7%A5%9E%E6%80%A7%E7%A5%9D%E7%A6%8F) 
> - [源於貴族](/docs/part1/Chapter8#descends-from-nobility-%E6%BA%90%E6%96%BC%E8%B2%B4%E6%97%8F)
> - [湧自方尖碑](/docs/part1/Chapter8#emerged-from-the-obelisk-%E6%B9%A7%E8%87%AA%E6%96%B9%E5%B0%96%E7%A2%91)
> - [飛得比子彈快](/docs/part1/Chapter8#flies-faster-than-a-bullet-%E9%A3%9B%E5%BE%97%E6%AF%94%E5%AD%90%E5%BD%88%E5%BF%AB)
> - [法術大師](/docs/part1/Chapter8#masters-spells-%E6%B3%95%E8%A1%93%E5%A4%A7%E5%B8%AB)
> - [大地言談](/docs/part1/Chapter8#speaks-for-the-land-%E5%A4%A7%E5%9C%B0%E8%A8%80%E8%AB%87)

#### 能力選擇指南
**第一階層：** 選擇一個低階能力，賦予焦點主題所承諾的好處之一，一個第一階層角色該有的能力。

有時，一個額外的低強度能力是合適的，這取決於焦點。通常情況下，這是一個在相關知識領域或相關技能方面給予技能訓練的能力。或者，也可以給一個屬性池提供 2-3 點的簡單獎勵。

**第二階層：** 選擇一個低階能力，賦予焦點主題所承諾的好處之一，一個與第一級提供的好處沒有直接關係的能力。也就是說，如果在第一階層沒有提供防禦，第二階層是一個好機會。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該提供焦點主題所承諾的好處之一，這個好處可能與早期層級所提供的好處沒有直接關係。

另一個選項應該包括一種攻擊方法，如果以前沒有授予過的話。另外，如果低階能力不能使角色達到他們需要的程度，這個選項可以進一步提高在低階能力中解鎖的能力。

**第四階層：** 選擇一個中階能力，賦予焦點主題所承諾的好處之一，這個好處可能與早期層級所提供的好處沒有直接關係。

**第五階層：** 選擇一個高階能力，賦予焦點主題所承諾的好處之一，這個好處可能與早期層級所提供的好處沒有直接關係。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

一個選項應該給予焦點主題所承諾的好處之一，這個好處可能與早期層級所提供的好處沒有直接關係。然而，如果中階或低階的選擇不夠充分，這種能力也可以提供低階能力的終極版本。

另個選項應該提供另一種方法來完善這個角色，而不是像第一個六階層的選項。例如，如果第一個選項提供了某種攻擊，這個選項可能是一種互動、訊息收集或治療能力，這取決於焦點的總體主題。

### 移動專業
為了在戰鬥中出類拔萃，在大多數人無法逃脫的情況下逃脫，為了偷竊或逃跑而隱秘地移動，或移動到通常無法進入的地方，優先考慮新的移動形式的焦點屬於移動專業類別。這些焦點通常都有通過移動給予進攻或防禦的方法，儘管可能同時提供一些方法。

典型的移動專業是依靠速度來進行更多攻擊和避免被擊中，儘管一般的敏捷性也可能提供同樣的好處。這一類中的其他焦點可能屬於這一主題，例如賦予角色成為非物質的能力，將形態改變為水或空氣之類的東西，或通過傳送瞬間移動。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何為實現巨大的速度、改變狀態或以其他方式獲得焦點的好處所必需的物品都應作為額外的裝備授予。這一類的一些焦點不需要任何東西來獲得或維持利益。

**建議弱效影響：** 目標也會暈眩一輪，在此期間所有任務都會受阻。

**建議強效影響：** 目標震懾並失去下一輪。


> 焦點範例
> - [存在相位離散](/docs/part1/Chapter8#exists-partially-out-of-phase-%E5%AD%98%E5%9C%A8%E7%9B%B8%E4%BD%8D%E9%9B%A2%E6%95%A3) 
> - [輕盈如貓](/docs/part1/Chapter8#moves-like-a-cat-%E8%BC%95%E7%9B%88%E5%A6%82%E8%B2%93)
> - [像風一樣](/docs/part1/Chapter8#moves-like-the-wind-%E5%83%8F%E9%A2%A8%E4%B8%80%E6%A8%A3)
> - [逃走](/docs/part1/Chapter8#runs-away-%E9%80%83%E8%B5%B0)
> - [撕碎世界之壁](/docs/part1/Chapter8#shreds-the-walls-of-the-world-%E6%92%95%E7%A2%8E%E4%B8%96%E7%95%8C%E4%B9%8B%E5%A3%81)
> - [穿越時間](/docs/part1/Chapter8#travels-through-time-%E7%A9%BF%E8%B6%8A%E6%99%82%E9%96%93)
> - [暗地做事](/docs/part1/Chapter8#works-the-back-alleys-%E6%9A%97%E5%9C%B0%E5%81%9A%E4%BA%8B)
 
#### 能力選擇指南
**第一階層：** 選擇一個低階能力，賦予特定移動風格的基本好處，無論是增強速度、敏捷性、非物質性，等等。

有時一個額外的低強度能力是合適的，這取決於焦點。如果移動的基本好處需要某種額外的理解或訓練，這個能力就可以是這樣。或者，如果所提供的移動看起來也應該解鎖一個基本的進攻或防禦的好處（依賴於最初基本能力的使用），也可以附加它。

**第二階層：** 選擇一個低階能力，提供一個與焦點主題有關的新的進攻或防禦能力。

或者，這個能力可以提供一些與移動形式有關的額外能力，為角色提供有用的訊息，而這些訊息對於沒有焦點的人來說通常是無法獲得的。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該提供與焦點的主題有關的額外移動能力或進一步提高基本的移動能力。這不是直接的攻擊或防禦性能力，而是為角色提供一種新的能力水平，或一種與基本移動能力有關的全新能力。

另一個選項應該提供與焦點提供的特定移動形式有關的進攻或防禦能力。

**第四階層：** 選擇一個中階能力，進一步增強焦點的移動增強範式所提供的優勢。這能提供一種新的或更好的防禦形式（直接的，或間接的，如果移動到一個沒有危險威脅的地點或時間），或一種新的或更好的進攻形式。

**第五階層：** 選擇一個高階的與移動有關的高階版低階能力。例如，如果焦點提供的移動是時間性的，這個能力可能允許實際的（如果是短暫的）時間旅行的短途旅行。如果焦點是增強速度，這個能力可能允許角色用一個動作移動到很遠的距離。以此類推。

或者，解鎖一個尚未探索的相關能力，這是能從焦點所提供的基本移動能力中衍生出來。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個選項應該提供進攻或防禦性的能力，與第四階層提供的能力相反（儘管是高階而不是中階）。

另一個選項應該是進一步探索基本移動能力的使用。如果第五階層的選擇是高階版低階能力，這可能是一種與移動有關的更好的終極能力。


### 前鋒戰鬥
前鋒戰鬥焦點優先考慮在戰鬥中造成傷害而不是其他問題。此類別的焦點也提供防禦，但更強調的是將傷害提升到其他焦點通常無法達到的高度的能力。

為了達到這個目的，前鋒戰鬥焦點可能會提供對某一特定武術戰鬥風格的掌握，這可能是對某一特定武器或武術的訓練，或對某一獨特工具（甚至是一種能量）的使用。一種風格可以是單一的，如最擅長與某類敵人作戰，也可以是更廣泛的，如採用一種特別兇惡或不講武德的風格。前鋒戰鬥可能會使用火、力或磁力作為他們的首選方法來增加傷害。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 從事特定戰鬥方式所需的武器、工具或其他特殊物品或物質（如果有的話）。例如，[骯髒戰鬥](/docs/part1/Chapter8#fights-dirty-%E9%AA%AF%E9%AB%92%E6%88%B0%E9%AC%A5)或[謀殺犯](/docs/part1/Chapter8#murders-%E8%AC%80%E6%AE%BA%E7%8A%AF)需要一劑5級毒藥，[與機器人作戰](/docs/part1/Chapter8#battles-robots-%E8%88%87%E6%A9%9F%E5%99%A8%E4%BA%BA%E4%BD%9C%E6%88%B0)需要以前被打敗的敵人的戰利品，[華麗戰鬥](/docs/part1/Chapter8#fights-with-panache-%E8%8F%AF%E9%BA%97%E6%88%B0%E9%AC%A5)需要一件時尚衣服。

**建議弱效影響：** 目標目眩一輪，所有任務受阻。

**建議強效影響：** 在你的回合中，使用焦點提供的攻擊立即進行額外攻擊。


> 焦點範例
> - [與機器人作戰](/docs/part1/Chapter8#battles-robots-%E8%88%87%E6%A9%9F%E5%99%A8%E4%BA%BA%E4%BD%9C%E6%88%B0) 
> - [骯髒戰鬥](/docs/part1/Chapter8#fights-dirty-%E9%AA%AF%E9%AB%92%E6%88%B0%E9%AC%A5)
> - [華麗戰鬥](/docs/part1/Chapter8#fights-with-panache-%E8%8F%AF%E9%BA%97%E6%88%B0%E9%AC%A5)
> - [狩獵](/docs/part1/Chapter8#hunts-%E7%8B%A9%E7%8D%B5)
> - [授權攜帶](/docs/part1/Chapter8#is-licensed-to-carry-%E6%8E%88%E6%AC%8A%E6%94%9C%E5%B8%B6)
> - [尋找麻煩](/docs/part1/Chapter8#looks-for-trouble-%E5%B0%8B%E6%89%BE%E9%BA%BB%E7%85%A9)
> - [武器大師](/docs/part1/Chapter8#masters-weaponry-%E6%AD%A6%E5%99%A8%E5%A4%A7%E5%B8%AB)
> - [謀殺犯](/docs/part1/Chapter8#murders-%E8%AC%80%E6%AE%BA%E7%8A%AF)
> - [無需武器](/docs/part1/Chapter8#needs-no-weapon-%E7%84%A1%E9%9C%80%E6%AD%A6%E5%99%A8)
> - [展現力量壯舉](/docs/part1/Chapter8#performs-feats-of-strength-%E5%B1%95%E7%8F%BE%E5%8A%9B%E9%87%8F%E5%A3%AF%E8%88%89)
> - [陷入狂怒](/docs/part1/Chapter8#rages-%E9%99%B7%E5%85%A5%E7%8B%82%E6%80%92) 
> - [殺戮怪物](/docs/part1/Chapter8#slays-monsters-%E6%AE%BA%E6%88%AE%E6%80%AA%E7%89%A9) 
> - [致命精準投擲](/docs/part1/Chapter8#throws-with-deadly-accuracy-%E8%87%B4%E5%91%BD%E7%B2%BE%E6%BA%96%E6%8A%95%E6%93%B2)
> - [同時揮舞兩件武器](/docs/part1/Chapter8#wields-two-weapons-at-once-%E5%90%8C%E6%99%82%E6%8F%AE%E8%88%9E%E5%85%A9%E4%BB%B6%E6%AD%A6%E5%99%A8)

#### 能力選擇指南
**第一階層：** 選擇一個低階能力，當角色使用焦點的特殊戰鬥風格、能量或態度進行攻擊時，或對選定的敵人使用時，能造成額外傷害。

有時，額外的低強度能力是合適的，這取決於焦點。例如，一個能使人熟練掌握某種特殊武器的焦點可能會提供與該武器相關的製作訓練。如果焦點是提高對某種特定敵人的傷害，可能會提供識別、定位的技能訓練，或者只是對該敵人有一般的了解。一種涉及惡毒或骯髒戰鬥方式的戰鬥風格可能提供恐嚇方面的訓練。以此類推。

如果專注於與一個特定的敵人作戰，那麼額外的次要力量（比可能提供的更多）可能是合適的。這些力量可能是進一步提高對所選敵人的效力，或是提供更廣泛但相關的能力，使接受焦點的角色在不與敵人作戰時也能發揮一些功能。

**第二階層：** 選擇一個低階能力，利用武器、武器風格或選擇的能量提供某種形式的防禦。如果武器風格特別擅長與某種敵人作戰，那麼這個能力應該是對這種敵人的防禦。另外，焦點可能提供另一種方法來增加所選範式內的傷害。

有時，一個額外的低強度能力在第二階層是合適的。如果是這樣，請選擇在第一階層沒有獲得的任何低強度能力。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該在使用焦點的戰鬥風格、能量或態度時，或在對所選擇的敵人使用時造成額外的傷害。這可以是提供額外攻擊的簡單能力，。

另一個選項應該提供一種方法，通過使用焦點的戰鬥風格、能量或態度，或在對選定的敵人使用時，解除他們的武裝，使他們眩暈或震懾，使他們變慢或停止，或以其他方式使他們混亂。

**第四階層：** 選擇一種中階能力，進一步增強焦點範式所提供的優勢。通常情況下，這包括訓練某種特定的攻擊方式。或者，該能力可能會增加實現某種戰鬥狀態所提供的優勢，比如獲得突襲。

**第五階層：** 選擇一個能造成傷害的高階能力。另外，如果專注於與某一特定類型的敵人作戰，這個能力可能會給角色一個機會，使 3 級以下的單一目標（或更高，如果專注於一個單一的敵人）完全失效、毀滅、失明或死亡。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個選項應該使用焦點範式來造成特殊傷害。

另一個選項能是一種不同的造成傷害的方式，可能使用焦點範式，或只是在一般情況下造成大量的傷害（並依靠先前的焦點能力來改善目標定位）。如果第一個選項是針對單個目標，這可能是針對多個目標，直接殺死或制伏一個目標（從 4 級開始，但有應用努力提高目標等級的指導），或選擇另一個敵人，進行另一次攻擊，或離開以便擇日再戰。


### 支援
允許角色幫助他人、保護他人、治療他人的焦點是支援焦點。當然，大多數焦點的能力都是用來幫助別人的，但支援焦點（如[虹吸力量](/docs/part1/Chapter8#siphons-power-%E8%99%B9%E5%90%B8%E5%8A%9B%E9%87%8F)）優先考慮幫助、治療和幫助他人。

支援焦點依靠各種方法來提供幫助，包括用於防禦的武術訓練、提供治療的超自然或科技手段，或者只是通過娛樂來減輕他人憂慮。

**連結：** 從焦點連結列表中選擇4項相關連結。 

**額外裝備：** 任何提供支持的必要物品。例如，一個用娛樂來幫助別人的人，需要儀器或類似的物體來幫助他們的工作。這個類別中的一些焦點不需要任何東西來獲得或維持利益。

**建議弱效影響：** 在下一輪結束之前的任何時候，你都可以在不需要使用動作的情況下發動攻擊。

**建議強效影響：** 你可以採取額外的動作來幫助盟友。

> 焦點範例
> - [保護弱者](/docs/part1/Chapter8#defends-the-weak-%E4%BF%9D%E8%AD%B7%E5%BC%B1%E8%80%85)
> - [娛樂](/docs/part1/Chapter8#entertains-%E5%A8%9B%E6%A8%82)
> - [幫助他們的朋友](/docs/part1/Chapter8#helps-their-friends-%E5%B9%AB%E5%8A%A9%E4%BB%96%E5%80%91%E7%9A%84%E6%9C%8B%E5%8F%8B)
> - [伸張正義](/docs/part1/Chapter8#metes-out-justice-%E4%BC%B8%E5%BC%B5%E6%AD%A3%E7%BE%A9)
> - [社區牧羊人](/docs/part1/Chapter8#shepherds-the-community-%E7%A4%BE%E5%8D%80%E7%89%A7%E7%BE%8A%E4%BA%BA)
> - [虹吸力量](/docs/part1/Chapter8#siphons-power-%E8%99%B9%E5%90%B8%E5%8A%9B%E9%87%8F)
> - [運用神跡](/docs/part1/Chapter8#works-miracles-%E9%81%8B%E7%94%A8%E7%A5%9E%E8%B7%A1)

#### 能力選擇指南
**第一階層：** 選擇一種低階能力，提供某種形式的防禦、幫助或娛樂、有利於恢復或治療、或保護。這種防禦或保護可以是針對個人的，而不是針對盟友的，因為一個人如果不首先能夠保護自己，就無法保護另一個人（有時保護自己就是全部的重點）。

有時，一個額外低強度能力是合適的，這取決於焦點。通常，這是一個在相關知識領域或相關技能方面給予技能訓練的能力，但可能是與最初的能力一起使用的東西，而且本身並沒有什麼作用。

**第二階層：** 選擇一個低階能力，並跟進上一層所開啟的支援風格。如果上一層的能力只提供擁有者保護手段，那麼第二層的能力應該專門為他人提供幫助。如果上一層專門為另一個人提供了援助，那麼第二層能力可以是自保或提供一種攻擊能力，如果可能的話，以焦點的主題為基礎。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

一個選項應該是在焦點的主題範圍內，幫助、治療、保護或以其他方式幫助他人。

另一個選項應該是對角色有利的東西，可以是攻擊或防禦性的能力，也能是以某種方式擴大專長的東西。另外，也可以是另一種幫助別人的不同方法。

**第四階層：** 選擇一個能給盟友帶來直接好處的中階能力，或為角色提供幫助他人的方法。也可以是一個傷害敵人或使敵人失效的能力，因為消除敵人當然有助於盟友。

**第五階層：** 如果還沒有提供的話，請選擇一個能夠為角色提供進攻或防守選擇的高階能力。如果這個需求之前已經解決了，或者被認為是不必要的，那麼就選擇一個能提供某種形式的防禦、援助或娛樂、有利於恢復或治療、或保護他人的高階能力。例如，一個五階層能力可能會給予盟友一個額外的免費動作，或允許他們重複一個失敗的動作。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

其中一個選項應該在焦點主題中提供幫助他人的終極方法。

另一個選項能提供另一種幫助他人的終極方法；這個類別中的許多焦點都是如此。然而，一個提供高級進攻或防禦的選項也是完全合理的。

### 坦克戰鬥
優先考慮承受大量懲罰和吸收敵人多餘傷害的焦點屬於坦克作戰類別。這些焦點也擁有攻擊性能力，以及與實現改進保護的特定方法有關的額外能力，但防禦能力是最明顯的。

有些坦克戰鬥焦點涉及到能提供額外保護的物理變化，而其他則依賴於專門訓練，使用盾牌或重甲等工具，或提供難以置信的快速治療能力。坦克焦點所提供的物理變化差別很大。一個焦點可能會把角色的皮膚變成石頭，用金屬加固身體，或把人變成巨大怪物，變得更難傷害他們，等等。

**連結：** 從焦點連結列表中選擇4項相關連結。

**額外裝備：** 任何維持身體轉變所需的物體（比如修理工具用於生化人，或是盾牌，或其他熟練的防禦工具，也可能是某種護身符或血清）。一些坦克戰鬥的焦點不需要任何東西來獲得或維持利益。

**建議弱效影響：** +2 護甲幾輪。

**建議強效影響：** 氣力池恢復 2 點。

> 焦點範例
> - [石成肉身](/docs/part1/Chapter8#abides-in-stone-%E7%9F%B3%E6%88%90%E8%82%89%E8%BA%AB) 
> - [揮舞奇異盾牌](/docs/part1/Chapter8#brandishes-an-exotic-shield-%E6%8F%AE%E8%88%9E%E5%A5%87%E7%95%B0%E7%9B%BE%E7%89%8C)
> - [守門](/docs/part1/Chapter8#defends-the-gate-%E5%AE%88%E9%96%80)
> - [鋼鐵肉軀](/docs/part1/Chapter8#fuses-flesh-and-steel-%E9%8B%BC%E9%90%B5%E8%82%89%E8%BB%80)
> - [成長到高聳的高度](/docs/part1/Chapter8#grows-to-towering-heights-%E6%88%90%E9%95%B7%E5%88%B0%E9%AB%98%E8%81%B3%E7%9A%84%E9%AB%98%E5%BA%A6)
> - [對月亮嚎叫](/docs/part1/Chapter8#howls-at-the-moon-%E5%B0%8D%E6%9C%88%E4%BA%AE%E5%9A%8E%E5%8F%AB)
> - [生活在荒野中](/docs/part1/Chapter8#lives-in-the-wilderness-%E7%94%9F%E6%B4%BB%E5%9C%A8%E8%8D%92%E9%87%8E%E4%B8%AD)
> - [防禦大師](/docs/part1/Chapter8#masters-defense-%E9%98%B2%E7%A6%A6%E5%A4%A7%E5%B8%AB)
> - [從不言敗](/docs/part1/Chapter8#never-says-die-%E5%BE%9E%E4%B8%8D%E8%A8%80%E6%95%97)
> - [像堡壘般佇立](/docs/part1/Chapter8#stands-like-a-bastion-%E5%83%8F%E5%A0%A1%E5%A3%98%E8%88%AC%E4%BD%87%E7%AB%8B)

#### 能力選擇指南
**第一階層：** 選擇一個低階能力，在焦點的主題內提供防禦。如果主題是簡單的密集訓練或使用防禦工具，這個能力可能是簡單的護甲獎勵。如果保護來自於身體的轉變，那麼這個能力就會提供基本的形式效果、好處，在某些情況下還有轉變的缺點。一個低階的強化治療能力在第一層也是合適的。

有時，一個額外的低強度能力是合適的，這取決於焦點。如果角色變身，這個能力可能會提供一個連鎖效應，儘管在某些變身的情況下，可能是描述一個擁有異常相貌的人如何能夠完全癒合。其他時候，次要能力可能僅僅是相關技能的訓練，或者它可能解鎖使用特定盔甲或盾牌的能力而不受懲罰。

**第二階層：** 如果焦點的主題不是身體轉化，選擇一個低階能力，提供額外防禦、治療傷害或避免攻擊的方法。

如果焦點的主題是身體轉化，選擇一個低階能力來解鎖與角色形態相關的新能力。這可能意味著獲得對變形的更好控制，解鎖機器人界面，或以其他方式更充分地解鎖該形態。這種能力不一定是防禦性的，儘管它可能是。

**第三階層：** 選擇2個中階能力。將這些能力作為焦點選項；讓 PC 選擇其中一個。

其中一個選項應該提供一種額外的保護形式，以符合焦點的主題，例如從變身中解鎖的更多防禦能力（可能還伴隨著額外的攻擊能力），或者如果防禦是由技能或強化治療獲得的，則是簡單的物理增強。

另一個選項應該提供一種進攻能力，特別是如果創建一個還沒有進攻性好處的非轉化焦點。這種能力可以是一種增強的攻擊，或者提供一些在戰鬥中有用的其他好處，比如快速躲避或變得不可移動。

**第四階層：** 選擇一種中階能力，進一步增強焦點的傷害浸透範式所提供的優勢。通常，這包括訓練一種特殊的防禦。或者，它可能會增加先前解鎖的防禦能力所提供的優勢，無論這是否意味著獲得對變身的更大控制，獲得額外的機會來避免傷害或重試與增強決心有關的任務，等等。如果焦點仍缺乏進攻性選擇，此時是個好機會。

**第五階層：** 選擇一個能提供保護的高階能力，可能是以擺脫衰弱狀態（包括死亡）的形式。如果焦點提供了一個身體上的轉變，這個能力可能會進一步解鎖一個額外的相關能力，無論是進攻、防禦性的，還是與探索或互動有關的能力（比如形態是有翼的，就可以飛行，如果形態是可怕的，就可以恐嚇，等等）。

**第六階層：** 選擇兩個高階能力。將這兩個能力作為焦點選項；PC 選擇其中一個。

一個選項應該是使用焦點範式來增加防禦、保護或甩掉傷害的能力。

另一個選項可能是以不同的方式進行防守。在某些情況下，最好的防禦是良好的進攻，所以這個選項可以提供一個高階的進攻能力，以符合焦點主題，無論是直接提升攻擊的傷害還是更好地控制不穩定的身體轉化。


## 自訂焦點
有時，焦點的所有內容都不適合角色概念，或者 GM 需要額外準則來創建新焦點。無論哪種情況，解決方案都是回頭去看為基本的能力選項。

在任何階層中，玩家都能選擇以下能力之一來代替該階層所賦予的能力。這些替代能力，在高階層時可能涉及身體改造、與高科技設備的整合、學習強大的魔法咒語、揭開禁忌的秘密，或類似的適合於該類型的能力。

第一階層
- [戰鬥英勇](/docs/part1/Chapter9#combat-prowess-%E6%88%B0%E9%AC%A5%E8%8B%B1%E5%8B%87)
- [強化潛力](/docs/part1/Chapter9#enhanced-potential-%E5%BC%B7%E5%8C%96%E6%BD%9B%E5%8A%9B)

第二階層
- 低階能力：選擇第 1 階層的能力替換任意一項
- [防禦技能](/docs/part1/Chapter9#skill-with-defense-%E9%98%B2%E7%A6%A6%E6%8A%80%E8%83%BD) 
- [熟練所有武器](/docs/part1/Chapter9#practiced-with-all-weapons-%E7%86%9F%E7%B7%B4%E6%89%80%E6%9C%89%E6%AD%A6%E5%99%A8)
- [攻擊技能](/docs/part1/Chapter9#skill-with-attacks-%E6%94%BB%E6%93%8A%E6%8A%80%E8%83%BD)

第三階層
- 低階能力：選擇第 1-2 階層的能力替換任意一項 
- [荒誕健康](/docs/part1/Chapter9#incredible-health-%E8%8D%92%E8%AA%95%E5%81%A5%E5%BA%B7) 
- [融合裝甲](/docs/part1/Chapter9#fusion-armor-%E8%9E%8D%E5%90%88%E8%A3%9D%E7%94%B2)


第四階層
- 低階能力：選擇第 1-3 階層的能力替換任意一項 
- [毒素抗性](/docs/part1/Chapter9#poison-resistance-%E6%AF%92%E7%B4%A0%E6%8A%97%E6%80%A7) 
- [內置武裝](/docs/part1/Chapter9#built-in-weaponry-%E5%85%A7%E7%BD%AE%E6%AD%A6%E8%A3%9D)

第五階層
- 低階能力：選擇第 1-4 階層的能力替換任意一項 
- [環境適應性](/docs/part1/Chapter9#adaptation-%E7%92%B0%E5%A2%83%E9%81%A9%E6%87%89%E6%80%A7)
- [防禦領域](/docs/part1/Chapter9#defensive-field-%E9%98%B2%E7%A6%A6%E9%A0%98%E5%9F%9F)

第六階層
- 低階能力：選擇第 1-5 階層的能力替換任意一項 
- [反應領域](/docs/part1/Chapter9#reactive-field-%E5%8F%8D%E6%87%89%E9%A0%98%E5%9F%9F)
