---
sidebar_position: 2
---

# 第3章：如何遊玩密鑰系統

密鑰系統的規則及其核心是非常直接的，所有玩法都基於同個概念。本章簡單介紹如何玩並且學習這個遊戲。一旦理解了基本概念，你可能會想要參考[第11章：遊戲規則](/docs/part2/Chapter11)，以獲得更深入的理解。

本規則使用一顆二十面骰（1d20）來確定大多數的結果。無論何時需要擲骰，如果沒有指定擲骰方式，則擲出 1d20。

主持人為給定的狀況設置難度。難度有 1-10 級。

每個難度都有相應的目標值。目標值總是任務難度的3倍，因此難度 1 的任務的目標值是 3，而難度 4 則是 12。要成功完成任務，擲骰結果必須等於或高於目標值。請參閱任務難度表，以了解其工作原理。

### 任務難度表

|  任務難度   | 描述  | 目標值 | 指導 |
|  ----        | ----  | ----  | ----  |
| 0  | 常規   | 0 | 基本上每個人都可以做到這一點。 |
| 1  | 簡單   | 3 | 大多數人都有能力這樣做。|
| 2  | 標準   | 6 | 標準的任務需要集中注意力，但大多數人通常都能做到這一點。|
| 3  | 苛刻   | 9 | 需要全神貫注；大多數人有50/50的成功機會。|
| 4  | 困難   | 12|受過訓練的人有50/50的成功機會。 |
| 5  | 挑戰   | 15| 即使是受過訓練的人也常常失敗。|
| 6  | 驚人   | 18| 普通人幾乎永遠不會成功。|
| 7  | 艱鉅   | 21| 沒有技能或努力就不可能。|
| 8  | 英雄   | 24| 一個值得你多年後仍舊重提的任務。|
| 9  | 不朽   | 27| 一個傳奇的任務，值得你談論一生。|
| 10 | 不可能 | 30| 普通人不會考慮的一項任務（但沒有違反物理定律）。|

角色技能、有利環境或優良裝備能降低任務難度。例如，如果一個角色在攀爬有`受訓`，則能夠將難度 6 的攀爬降低為難度 5。 這稱為難度簡化 1 級（或僅稱`簡化`）。 如果有對攀爬有專精，則能夠將難度 6 級的攀爬變為 4 級。 因為`專精`能夠將難度簡化 2 級。 

降低任務的難度也能稱為任務簡化。 有些情況會增加或阻礙任務的難度。 如果一項任務`受阻`，難度就會增加一級。

技能是與一項任務有關的知識、能力或活動的範疇，如攀爬、地理位置或說服。 有技能的角色比沒有技能的更擅長完成相關任務。 角色的技能等級可能是受訓（熟練）或專精（非常熟練）。

如果你對某項任務有受訓則難度簡化 1 級，專精的話則是簡化 2 級。技能無法將任務難度降低超過 2 級。

任何降低難度的事物（來自盟友的幫助、特定裝備或其他優勢）都被稱為`資產（asset）`。 資產也無法將任務難度降低超過 2 級。

你還能通過`努力（Effort）`來降低任務難度。 (在第11章有更詳細的描述。)

總而言之，有三件事可以降低任務難度：**技能**、**資產**和**努力**。

如果你能輕鬆完成一項任務，並將其難度降低到 0，你就會自動成功，不需要擲骰。

## 何時擲骰?
每當你的角色嘗試一項任務，GM 就會給該任務分配難度，然後你對目標值擲出 1d20 進行比較。

當你從燃燒的車輛上跳下來，向變異的野獸揮舞斧頭，游過洶湧的河流，識別奇怪裝置，說服商人給更好的優惠，製造物品，使用某種力量控制敵人的思想，或者使用雷射槍在牆上打個洞，都是做一個 1d20 的擲骰。

然而，如果你嘗試的事物的難度為 0，則不需要擲骰。你會自動成功。 許多動作的難度為 0。 例如，通過房間並把門打開，使用某種特殊能力抵消重力，使你能夠飛行，使用能力來保護你的朋友免受輻射，或者啟動設備（你已經理解的）來創造屏障。 這些都是一般操作，無需擲骰。

使用技能、資產和努力，你能將任務難度降低到 0，並避免擲骰的需求。 對大多數人來說，在狹窄的木樑上行走很困難，但對有經驗的體操員來說，這是家常便飯。你甚至能將敵人的攻擊難度降低到 0，使你不需要擲骰就能成功閃避。

不擲骰，就不會失敗。 不過，也錯過了獲得顯著成功的機會（在密鑰系統中，通常意味著骰出 19 或 20，這被稱為特殊骰數;第11章也討論了特殊骰數）。

## 戰鬥
在戰鬥中進行攻擊和其他擲骰一樣：GM 給任務分配難度，然後你對目標值擲出 1d20 進行比較。

你挑戰的難度多少取決於對手有多強大。 任務難度從 1-10 級，而生物的等級也有 1-10 級。 大多數情況下，你的攻擊難度會與生物等級相同。例如，假設你攻擊 2 級強盜，這就是 2 級任務，所以你的目標值是 6。

值得注意的是，所有擲骰都是由玩家進行。 角色攻擊生物時，玩家進行攻擊擲骰。 生物攻擊角色時，玩家進行防禦擲骰。

攻擊造成的傷害不是由擲骰決定 — 而是基於所使用的武器或攻擊的固定數字 。舉例來說，一支長矛通常會造成 4 點傷害。

你的`護甲（Armor）`會減少你受到的傷害。你能透過穿物理防具（比如現代遊戲中的皮夾克或奇幻場景中的鏈甲）或特殊能力來獲得護甲。 和武器傷害一樣，護甲是個固定數字，而不依靠擲骰。 如果你被攻擊命中，從所受的傷害中減去護甲值。 例如，一件皮夾克給你 +1 護甲，這意味著你從攻擊中減少 1 點傷害。如果搶匪在你穿著皮夾克的時候用刀砍你，並造成 2 點傷害，你就只會受到 1 點傷害。 如果護甲將攻擊造成的傷害降低到 0，則你不會受到任何傷害。

典型的物理武器分為三類：輕型、中型和重型：
- ***輕型武器*** 造成 2 點傷害，但因為快速和容易使用所以攻擊任務有`簡化`。 輕型武器有拳頭、腳踢、棒、刀、手斧、刺劍、小手槍等等。 特別小的武器是輕型武器。
- ***中型武器*** 造成 4 點傷害。 中型武器包括劍、戰斧、大砍刀、弩、長矛、手槍、爆破槍等。 大多數武器是中型的。 任何可以單手使用的東西（即使通常是雙手使用，如長棍或矛）都是中型武器。
- ***重型武器*** 造成 6 點傷害，你必須用雙手攻擊。 重型武器是巨劍，巨錘，巨斧，戟，重弩，爆破步槍，等等。 任何必須雙手使用的東西都是重型武器。

## 特殊骰數
當你擲出自然 19（1d20 出現 19 的結果）並且成功時，你也獲得`弱效影響（minor effect）`。 在戰鬥中，弱效影響使你的攻擊 +3 傷害，或者，如果你更喜歡特殊效果，則能決定將敵人擊退，轉移注意力，或者類似的事情。 當不在戰鬥中，弱效影響可能意味著你以特別優雅的姿態執行動作。例如，當你從懸崖上跳下來的時候，你平穩地用腳著地，或者當你試圖說服別人的時候，你會讓他們相信你比自己更聰明。換句話說，你不僅成功了，而且做得更好。

當你擲出自然 20（1d20 出現 20 的結果）並且成功時，你也獲得`強效影響（major effect）`。 這類似弱效影響，但結果更加顯著。 在戰鬥中，強效影響使你的攻擊 +4 傷害，並且同樣，你能夠選擇引入戲劇性的事件，例如將敵人擊倒在地，打暈他們，或者採取一次額外動作。 在戰鬥外，強效影響意味著一些基於環境的有益事情發生。 例如，當你爬上懸崖時，你的速度是別人的兩倍。當擲骰給你強效影響時，如果你願意，你能選擇使用弱效影響。

在戰鬥中（僅在戰鬥中），如果攻擊擲出自然 17 或 18，你的攻擊將會造成 +1 或 +2 傷害。 兩者都沒有任何特殊影響 — 只有額外傷害。

自然 1 總是不好的。 這意味著 GM 在這次擲骰的結果中引入了一個新的突發因素。

對某些人來說，戰鬥會是密鑰系統的重要組成部分。然而，你能自己決定；密鑰系統的遊戲並不一定著重於戰鬥。

## 範圍與速度
距離被簡化為四類：直接距離、短距離、遠距離和極遠距。

- ***直接距離*** 是指伸手可及或幾步之內。 如果角色在小房間裡，房間裡的所有東西都近在咫尺。 最大為 10 英尺（3米）。
- ***短距離*** 是指大於直接距離但小於 50 英尺（15 米）左右的距離。
- ***遠距離*** 是指大於短距離但小於 100 英尺（30 米）左右的距離。
- ***極遠距*** 是指大於遠距離但小於 500 英尺（150 米）左右的距離。 在這個範圍之外，距離總是有規定的 — 1000 英尺(300 米)，一英里（1.5 公里），等等。

這個想法是，沒有必要測量精確的距離。直接距離就是角色身邊。 短距離是附近。遠距離是離的更遠，極遠距則是指真的很遠。

所有武器和特殊能力都使用這些術語。 例如，所有近戰武器都有直接距離 — 因為是近戰武器，你能用來攻擊直接距離內的任何人。 投擲飛刀（和大多數其他投擲武器）的射程都是短距離。 弓的射程是遠距離。行家的猛攻能力是短距離。

角色能以一個動作的同個部分，移動直接距離。也就是說，他們可以走幾步到控制面板，並啟動一個開關。 躍過小房間去攻擊敵人。 打開一扇門，走進去。

角色能以一個動作，移動短距離。也能嘗試移動到遠距離，但可能需要擲骰來確定角色是否會因為移動得太遠太快而發生意外。

例如，假設 PCs 正在和一群邪教份子戰鬥，任何角色都有能在混戰中攻擊任何邪教分子 — 因為都在直接距離內。所以確切位置並不重要。戰鬥中的生物總是在移動、切換位置和衝撞。然而，如果有個邪教份子退後開槍，角色可能必須用整個動作才能移動到攻擊敵人所需要的短距離位置。無論是 20 英尺（6米）還是 40 英尺（12米），都是短距離。這是很重要，因為如果邪教超過 50 英尺（15 米），那這就會是一個遠距離或是極遠距的移動。

此系統中的許多規則避免了繁瑣的精確規範。鬼魂離你13英尺或18英尺真的重要嗎？可能不。並且這種不必要的具體化會讓事情變慢，將玩家的重點從故事本身拉離，而不是故事本身。

## 經驗點
經驗點（XP）是當 GM 影響故事（這稱為 GM 入侵）並帶來新的和意想不到的挑戰時給予玩家的獎勵。 例如，在戰鬥中，GM 可能會告知玩家武器突然掉了。 然而，如果要以這種方式影響，GM 必須獎勵玩家 2 XP。 作為獎勵，玩家得將其中 1 XP 給另一名玩家，並說明為何給他（也許是因為另一名玩家有個好主意、講了有趣的笑話、做了拯救生命的行為，等等)。

或者，玩家能避免 GM 入侵。如果這麼做，就不會從獲得 2 XP ，並且還必須花費 1 XP 才能避免。 如果玩家沒有 XP，則無法拒絕影響。

GM 也能在團務之間給予玩家經驗點作為在冒險中發現的獎勵。 發現有趣的事實，奇妙的秘密，強大的神器，謎團的答案，或問題的解決方案（如綁架者把他們的受害者放在哪裡或 PCs 如何修理太空船）。你不會因為殺死敵人或在遊戲過程中克服標準挑戰而獲得經驗值。發現是密鑰系統的靈魂。

經驗點主要用於角色提升（詳情請參見[第4章：創建角色](/docs/part1/Chapter4)），但玩家也能花費 1 XP 重新擲骰任何骰子，並取其中較優的結果。

## 秘具
秘具是有單一用途的能力。在許多戰役中，秘具不是物理物體 — 可能是法術，來自神祇的祝福，或者只是給予暫時優勢的命運巧合。在某些戰役中，秘具是角色能攜帶的物理物體。 無論是否是物理物體，都是角色的一部分（比如裝備或特殊能力），是角色在遊戲中能使用的東西。 物理秘具的形式取決於環境。 在奇幻世界中，它們可能是魔杖或魔藥，但在科幻遊戲中，可能是外星水晶或原型設備。

角色會在遊戲過程中頻繁地發現秘具，所以玩家應該毫不猶豫地使用秘具。 因為秘具總是不同，角色能獲得新能力來進行嘗試。

## 其他骰子
除了 1d20，你還需要 1d6（六面骰）。 很少需要擲骰 1-100 之間的數字（通常稱為 d100 或 d% 骰），你能簡單通過擲骰兩次 d20 來實現這一點，使用第一次擲骰的尾數作為「十位數」，第二次擲骰的尾數為「個位數」。 例如，擲 17 和 9 得 79，擲 3 和 18 得38，擲 20 和 10 得 00（也稱為 100）。 如果你有 1d10（十面骰），你也能用來代替 1d20 擲骰 1-100 之間的數字。